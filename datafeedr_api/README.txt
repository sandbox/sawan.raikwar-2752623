=== Datafeedr API ===

Contributors: datafeedr.com, cognizant.com
Tags: ubercart, datafeedr, affiliate products, dfrapi, adrecord, adtraction, affiliate window, affiliate4you, affilinet, amazon local, avangate, avantlink, belboon, betty mills, cj, clickbank, clixgalore, commission factory, commission junction, daisycon, dgm, flipkart, impact radius, linkconnector, linkshare, onenetworkdirect, paid on results, partner-ads, pepperjam, mycommerce, revresponse, shareasale, superclix, tradedoubler, tradetracker, webgains, zanox
Requires at least: 7.x

Connect to the Datafeedr API.

== Description ==

**NOTE:** The *Datafeedr API* module requires that you have Datafeedr API keys. API keys can be accessed here: [https://v4.datafeedr.com/pricing](https://v4.datafeedr.com/pricing?p=1&utm_campaign=dfrapiplugin&utm_medium=referral&utm_source=wporg)

The Datafeedr API provides access to our database of affiliate products. We have aggregated over 250,000,000 products from over 10,000 merchants and 32 affiliate networks. We have indexed and normalized the product data making it easy for you to search for and find products you want to promote on your website.

The *Datafeedr API* module currently integrates with the following modules:

* [Datafeedr Product Sets]
* [Datafeedr Ubercart Importer]

The *Datafeedr API* module provides the interface to connect to the Datafeedr API and perform the following tasks:

* Configure your API connection settings.
* Select affiliate networks you are a member of.
* Select merchants who have approved you.
* Add your affiliate network affiliate IDs.
* Import/export your selection of affiliate networks and/or merchants.
* View your API account usage.

The *Datafeedr API* module was built to be extended. The *Datafeedr API* module contains its own functions that third party developers can use to connect to the Datafeedr API, make search requests or display an 'advanced search' form. We encourage other developers to build on top of the *Datafeedr API* module.

Additionally, we have written modules that integrate the *Datafeedr API* module with Ubercart. More extensions are on the way...


== Requirements ==

* PHP's `CURL` support must be enabled.


== Installation ==

This section describes how to install and configure the module:

* Install the module as usual, for further information please visit http://drupal.org/node/895232


== Configuration ==

* Enter your Datafeedr API keys here: Drupal Admin Area > Datafeedr Configuration > Datafeedr API Configuration
* Select the affiliate networks you are a member of here: Drupal Admin Area > Datafeedr Configuration > Datafeedr API Configuration > Networks
* Select the merchants who have approved you here: Drupal Admin Area > Datafeedr Configuration > Datafeedr API Configuration > Merchants

== Frequently Asked Questions ==

= Where can I get help?  =

== Contact ==

Special thanks to Eric Busch (Co-founder of Datafeedr) for Datafeedr WordPress plugins.

* Eric Busch (Co-founder)
  www.datafeedr.com
  help@datafeedr.com

Current mentor:
* Michael Julius (mykill) - https://www.drupal.org/user/3007587

Current maintainers:
* Sawan Raikwar (sawan.raikwar) - http://drupal.org/user/2664283
* Chandra Prakash Maurya (chandrap) - http://drupal.org/user/2877119
* Brajendra Singh (brajendrasingh) - http://drupal.org/user/769014
* Swathi G (swathireddy6) - http://drupal.org/user/1979130
* Kiran Kumar Kollampally (kirankumar_k) - http://drupal.org/user/658778

Major rewrite for Drupal 7 by Sawan Raikwar (sawan.raikwar) & Chandra Prakash Maurya (chandrap).
