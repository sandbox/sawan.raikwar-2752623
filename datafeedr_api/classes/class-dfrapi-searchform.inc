<?php

/**
 * @file
 * This file implements all serach form functions.
 */

/**
 * Data feedr API search class.
 */
class Dfrapi_SearchForm {

  /**
   * Search field.
   */
  public function fields() {
    $opFulltext = array(
      'contain' => t('contains'),
      'not_contain' => t("doesn't contain"),
      'start' => t('starts with'),
      'end' => t('ends with'),
      'match' => t('matches'),
    );
    $opFulltextExact = array_merge($opFulltext, array(
      'is' => t('is'),
    ));
    $opRange = array(
      'eq' => t('equal to'),
      'lt' => t('less than'),
      'lte' => t('less than or equal to'),
      'gt' => t('greater than'),
      'gte' => t('greater than or equal to'),
      'between' => t('between'),
    );
    $opIs = array(
      'is' => t('is'),
    );
    $opIsIsnt = array(
      'is' => t('is'),
      'is_not' => t("isn't"),
    );
    $opYesNo = array(
      'yes' => t('yes'),
      'no' => t('no'),
    );

    $sortOpts = array(
      '' => t('Relevance'),
      '+price' => t('Price Ascending'),
      '-price' => t('Price Decending'),
      '+saleprice' => t('Sale Price Ascending'),
      '-saleprice' => t('Sale Price Decending'),
      '+finalprice' => t('Final Price Ascending'),
      '-finalprice' => t('Final Price Decending'),
      '+salediscount' => t('Discount Ascending'),
      '-salediscount' => t('Discount Decending'),
      '+merchant' => t('Merchant'),
      '+time_created' => t('Date Created Ascending'),
      '-time_created' => t('Date Created Decending'),
      '+time_updated' => t('Last Updated Ascending'),
      '-time_updated' => t('Last Updated Decending'),
    );

    return array(
      array(
        'title' => t('Any Field'),
        'name' => 'any',
        'input' => 'text',
        'operator' => $opFulltext,
        'help' => $this->help('any'),
      ),
      array(
        'title' => t('Product Name'),
        'name' => 'name',
        'input' => 'text',
        'operator' => $opFulltextExact,
        'help' => $this->help('name'),
      ),
      array(
        'title' => t('Brand'),
        'name' => 'brand',
        'input' => 'text',
        'operator' => $opFulltextExact,
        'help' => $this->help('brand'),
      ),
      array(
        'title' => t('Description'),
        'name' => 'description',
        'input' => 'text',
        'operator' => $opFulltext,
        'help' => $this->help('description'),
      ),
      array(
        'title' => t('Tags'),
        'name' => 'tags',
        'input' => 'text',
        'operator' => array(
          'in' => t('contain'),
          'not_in' => t("don't contain"),
        ),
        'help' => $this->help('tags'),
      ),
      array(
        'title' => t('Category'),
        'name' => 'category',
        'input' => 'text',
        'operator' => $opFulltext,
        'help' => $this->help('category'),
      ),
      array(
        'title' => t('Product Type'),
        'name' => 'type',
        'input' => 'select',
        'options' => array(
          'products' => t('Product'),
          'coupons' => t('Coupon'),
        ),
        'operator' => $opIs,
        'help' => $this->help('type'),
      ),
      array(
        'title' => t('Currency'),
        'name' => 'currency',
        'input' => 'select',
        'options' => array(
          'AUD' => 'AUD',
          'CAD' => 'CAD',
          'CHF' => 'CHF',
          'DKK' => 'DKK',
          'EUR' => 'EUR',
          'GBP' => 'GBP',
          'NOK' => 'NOK',
          'NZD' => 'NZD',
          'SEK' => 'SEK',
          'TRY' => 'TRY',
          'USD' => 'USD',
        ),
        'operator' => $opIsIsnt,
        'help' => $this->help('currency'),
      ),
      array(
        'title' => t('Price'),
        'name' => 'price',
        'input' => 'range',
        'operator' => $opRange,
        'help' => $this->help('price'),
      ),
      array(
        'title' => t('Sale Price'),
        'name' => 'saleprice',
        'input' => 'range',
        'operator' => $opRange,
        'help' => $this->help('saleprice'),
      ),
      array(
        'title' => t('Final Price'),
        'name' => 'finalprice',
        'input' => 'range',
        'operator' => $opRange,
        'help' => $this->help('finalprice'),
      ),
      array(
        'title' => t('Network'),
        'name' => 'source_id',
        'input' => 'network',
        'operator' => $opIsIsnt,
        'help' => $this->help('source_id'),
      ),
      array(
        'title' => t('Merchant'),
        'name' => 'merchant_id',
        'input' => 'merchant',
        'operator' => $opIsIsnt,
        'help' => $this->help('merchant_id'),
      ),
      array(
        'title' => t('On Sale'),
        'name' => 'onsale',
        'input' => 'none',
        'operator' => $opYesNo,
        'help' => $this->help('onsale'),
      ),
      array(
        'title' => t('Has Direct URL'),
        'name' => 'direct_url',
        'input' => 'none',
        'operator' => $opYesNo,
        'help' => $this->help('direct_url'),
      ),
      array(
        'title' => t('Discount'),
        'name' => 'salediscount',
        'input' => 'range',
        'operator' => $opRange,
        'help' => $this->help('salediscount'),
      ),
      array(
        'title' => t('Has Image'),
        'name' => 'image',
        'input' => 'none',
        'operator' => $opYesNo,
        'help' => $this->help('image'),
      ),
      array(
        'title' => t('Last Updated'),
        'name' => 'time_updated',
        'input' => 'range',
        'operator' => array(
          'lt' => 'before',
          'gt' => 'after',
          'between' => 'between',
        ),
        'help' => $this->help('time_updated'),
      ),
      array(
        'title' => t('Limit'),
        'name' => 'limit',
        'input' => 'text',
        'operator' => array('is' => 'is'),
        'help' => $this->help('limit'),
      ),
      array(
        'title' => t('Sort By'),
        'name' => 'sort',
        'input' => 'none',
        'operator' => $sortOpts,
        'help' => $this->help('sort'),
      ),
      array(
        'title' => t('Exclude Duplicates'),
        'name' => 'duplicates',
        'input' => 'text',
        'operator' => array(
          'is' => t('matching these fields'),
        ),
        'help' => $this->help('duplicates'),
      ),
    );
  }

  /**
   * Provide default values.
   */
  public function defaults() {
    return array(
      'any' => array('operator' => 'contain', 'value' => ''),
      'name' => array('operator' => 'contain', 'value' => ''),
      'type' => array('value' => 'product'),
      'currency' => array('value' => 'USD'),
      'price' => array(
        'operator' => 'between',
        'value' => '0',
        'value2' => '999999',
      ),
      'saleprice' => array(
        'operator' => 'between',
        'value' => '0',
        'value2' => '999999',
      ),
      'source_id' => array('value' => array()),
      'merchant_id' => array('value' => array()),
      'onsale' => array('value' => '1'),
      'direct_url' => array('value' => '1'),
      'image' => array('value' => '1'),
      'thumbnail' => array('value' => '1'),
      'time_updated' => array('operator' => 'lt', 'value' => 'today'),
      'limit' => array('value' => 1000),
    );
  }

  public $useSelected;
  public $prefix;

  /**
   * Get values.
   */
  public function get($ary, $key, $default = "") {
    return is_array($ary) && isset($ary[$key]) ? $ary[$key] : $default;
  }

  /**
   * Add prefix.
   */
  public function inputPrefix($index) {
    return sprintf('%s[%s]', $this->prefix ? $this->prefix : "query", $index);
  }

  /**
   * Name concat.
   */
  public function byName($a, $b) {
    return strcasecmp($a['name'], $b['name']);
  }

  /**
   * String to array.
   */
  public function ary($s) {
    if (!is_array($s)) {
      $s = explode(',', trim($s));
    }
    return array_filter($s, 'strlen');
  }

  /**
   * Return Id array.
   */
  public function ids($lst) {
    $ids = array();
    foreach ($lst as $obj) {
      $ids[] = $obj['_id'];
    }

    return $ids;
  }

  /**
   * Set options.
   */
  public function selectOpts($opts, $selectedValue = NULL) {
    $html = "";
    foreach ($opts as $value => $title) {
      $sel = $selectedValue == $value ? "selected='selected'" : "";
      $title = htmlspecialchars($title);
      $value = htmlspecialchars($value);
      $html .= "<option value=\"$value\" $sel>$title</option>";
    }
    return $html;
  }

  /**
   * List of all networks.
   */
  public function allNetworks() {
    // Getting API settings data.
    $dfrapi_configuration = variable_get('dfrapi_configuration', array());

    // Get all networks.
    if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
      $lst = datafeedr_api_sample_network_merchant('network');
    }
    else {
      $lst = dfrapi_api_get_all_networks();
    }
    usort($lst, array($this, 'byName'));
    return $lst;
  }

  /**
   * Get selected newwork details.
   */
  public function selectedNetworks() {
    $selected = $this->get(variable_get('dfrapi_networks', array()), 'ids');

    if (empty($selected)) {
      return array();
    }

    $lst = array();
    foreach ($this->allNetworks() as $net) {
      if (isset($selected[$net['_id']])) {
        $lst[] = $net;
      }
    }
    return $lst;
  }

  /**
   * Group class.
   */
  public function groupClass($nid) {
    if (empty($nid)) {
      return '';
    }
    $networks = $this->allNetworks();
    foreach ($networks as $network) {
      if ($network['_id'] == $nid) {
        $name = str_replace(array(" ", "-", "."), "", $network['group']);
        $type = ($network['type'] == 'coupons') ? '_coupons' : '';
        return 'network_logo_16x16_' . strtolower($name . $type);
      }
    }
  }

  /**
   * Provide selected merchant details.
   */
  public function selectedMerchants() {
    $selected = $this->get(variable_get('dfrapi_merchants', array()), 'ids');
    if (empty($selected)) {
      return array();
    }

    $lst = array();
    foreach (dfrapi_api_get_merchants_by_id($selected) as $merchant) {
      $lst[] = $merchant;
    }
    usort($lst, array($this, 'byName'));
    return $lst;
  }

  /**
   * Show network merchant popups.
   */
  public function networksMerchantsPopup($kind, $value) {
    $title = array(
      'network' => t('Select Networks'),
      'merchant' => t('Select Merchants'),
    );
    $clear = t('Clear search');
    $ok = t('OK');
    $cells = "";

    $all = ($kind == 'network') ?
        ($this->useSelected ? $this->selectedNetworks() : $this->allNetworks()) :
        $this->selectedMerchants();

    foreach ($all as $obj) {
      $id = $obj['_id'];
      $nid = ($kind == 'network') ? $obj['_id'] : $obj['source_id'];
      $group_class = $this->groupClass($nid);

      $name = $obj['name'];
      $checked = in_array($id, $value) ? "checked='checked'" : "";

      $cells .= "
				<div class='inline_frame_element nid_{$nid}'>
					<label>
						<input type='checkbox' value='{$id}' $checked />
						<span class='element_name {$group_class}'>{$name}</span>
					</label>
				</div>
			";
    }

    return "
            <h1>{$title[$kind]}</h1>
            <div class='filter_action'>
                Search: <input type='text' />
                <a class='reset_search button' title='{$clear}'>&times;</a>
            </div>
            <div class='inline_frame'>
				<div>
					{$cells}
					<div class='clearfix'></div>
				</div>
			</div>
			<div><a class='button submit'>$ok</a></div>
		";
  }

  /**
   * Get network merchants name.
   */
  public function networksMerchantsNames($kind, $value, $maxNames = 5) {
    $all = ($kind == 'network') ? $this->allNetworks() : dfrapi_api_get_merchants_by_id($value);
    $names = array();
    foreach ($all as $obj) {
      if (in_array($obj['_id'], $value)) {
        $names[] = "<span>{$obj['name']}</span>";
        if (count($names) >= $maxNames) {
          break;
        }
      }
    }
    $html = implode(', ', $names);
    if (count($value) - count($names)) {
      $html .= ' ' . sprintf(t('and %s more'), count($value) - count($names));
    }
    return $html;
  }

  /**
   * Create choose box.
   */
  public function chooseBox($kind, $field, $index, $value) {
    $pfx = $this->inputPrefix($index);
    $value = implode(',', $this->ary($value));
    $choose = t('choose');
    return "
            <div class='dfrapi_choose_box' rel='{$kind}'>
                <span class='names'></span>
                <a class='button choose_{$kind}'>{$choose}</a>
                <input name='{$pfx}[value]' type='hidden' value=\"$value\"/>
            </div>
        ";
  }

  /**
   * Ajax handler.
   */
  public function ajaxHandler() {
    $command = $this->get($_POST, 'command');
    $value = $this->ary($this->get($_POST, 'value'));
    $this->useSelected = intval($this->get($_POST, 'useSelected', 1));

    switch ($command) {
      case "choose_network":
        return $this->networksMerchantsPopup('network', $value);

      case "choose_merchant":
        return $this->networksMerchantsPopup('merchant', $value);

      case "names_network":
        return $this->networksMerchantsNames('network', $value);

      case "names_merchant":
        return $this->networksMerchantsNames('merchant', $value);

    }
    return "";
  }

  /**
   * Render field.
   */
  public function renderField($field, $index, $params) {
    $pfx = $this->inputPrefix($index);
    $value = $this->get($params, 'value');
    $operator = $this->get($params, 'operator');
    $input = "";
    $percent_sign = (isset($field['name']) && $field['name'] == 'salediscount') ? '%' : '';

    switch ($field['input']) {
      case 'text':
        $value = htmlspecialchars($value);
        $input = "<input class='long form-text' name='{$pfx}[value]' type='text' value=\"$value\"/>";
        break;

      case 'select':
        $opts = $this->selectOpts($field['options'], $value);
        $input = "<select name='{$pfx}[value]'class='form-select'>{$opts}</select>";
        break;

      case 'range':
        $value = htmlspecialchars($value);
        $value2 = htmlspecialchars($this->get($params, 'value2'));
        $and = t('and');
        $input = "
        			<input class='short form-text' name='{$pfx}[value]' type='text' value=\"$value\"/>{$percent_sign}
		        	<span class='value2' style='display:none'>
			            $and
			            <input class='short form-text' name='{$pfx}[value2]' type='text' value=\"$value2\" />
                    </span>
		        ";
        break;

      case 'network':
        $input = $this->chooseBox('network', $field, $index, $value);
        break;

      case 'merchant':
        $input = $this->chooseBox('merchant', $field, $index, $value);
        break;

      case 'none':
        $input = "";
        break;
    }

    if (count($field['operator']) == 1) {
      $key = key($field['operator']);
      $val = current($field['operator']);
      $operator = "
                <input type='hidden' name='{$pfx}[operator]' value=\"{$key}\" />
                <span>{$val}</span>
            ";
    }
    else {
      $opts = $this->selectOpts($field['operator'], $operator);
      $operator = "<select class='form-select' name='{$pfx}[operator]'>{$opts}</select>";
    }
    return array($operator, $input);
  }

  /**
   * Render rows.
   */
  public function renderRow($field, $index, $params, $show) {
    $pfx = $this->inputPrefix($index);
    list($operator, $input) = $this->renderField($field, $index, $params);
    $fieldOpts = '';
    foreach ($this->fields() as $f) {
      if (!$this->useSelected && $f['name'] == 'merchant_id') {
        continue;
      }
      $sel = $field['name'] == $f['name'] ? "selected='selected'" : "";
      $fieldOpts .= "<option value=\"{$f['name']}\" $sel>{$f['title']}</option>";
    }
    $style = $show ? "" : "style='display:none'";
    return "
            <div class='filter filter_{$field['name']}' {$style}>
                <div class='valuewrapper'>
                    <div class='value'>{$input}</div>
                </div>
                <div class='plusminus'><a class='minus'></a> </div>
                <div class='field'><select class='form-select' name='{$pfx}[field]' style='width: 140px'>{$fieldOpts}</select><a href='#' class='dfrapi_search_help'>?</a></div>
                <div class='operator'>{$operator}</div>
                <div class='clearfix'></div>
                <div class='help' style='display:none;'><a href='#' class='dfrapi_search_help'><span class='dashicons dashicons-no'> </span></a>{$field['help']}</div>
            </div>
        ";
  }

  /**
   * Render function.
   */
  public function render($prefix, $query, $useSelected = TRUE) {
    $this->prefix = $prefix;
    $this->useSelected = intval($useSelected);
    if (!$query) {
      $query = array(
        array('field' => 'any', 'value' => ''),
      );
    }
    $defaults = $this->defaults();

    $fieldMap = array();
    $fieldUsed = array();
    foreach ($this->fields() as $f) {
      if (!$this->useSelected && $f['name'] == 'merchant_id') {
        continue;
      }
      $fieldMap[$f['name']] = $f;
    }

    $form = "";
    $index = 0;
    foreach ($query as $params) {
      $field = $this->get($fieldMap, $this->get($params, 'field'));
      if (!$field) {
        continue;
      }
      $fieldUsed[$field['name']] = 1;
      $form .= $this->renderRow($field, $index++, $params, TRUE);
    }
    // If none shown, show the first.
    $show = ($index == 0);
    foreach ($fieldMap as $field) {
      $name = $field['name'];
      if (!isset($fieldUsed[$name])) {
        $form .= $this->renderRow($field, $index++, isset($defaults[$name]) ? $defaults[$name] : NULL, $show);
        $show = FALSE;
      }
    }

    $loading = t('Loading, please wait');
    $add = t('add filter');
    return "
            <div id='dfrapi_search_form'>
                <input type='hidden' id='dfrapi_useSelected' value='{$this->useSelected}' />
                <div id='dfprs_loading_content' style='display:none'>
                    <div class='dfrapi_loading'></div>
                    <h3>{$loading}</h3>
                </div>
                {$form}
                <div class='clearfix'></div>
                <div id='dfrapi_search_form_filter'><a href='javascript:void(0);'><span class='dashicons dashicons-plus'> </span> $add</a></div>
            </div>
        ";
  }

  /**
   * Combine lists.
   */
  public function combineLists($lst, $func) {
    if (!count($lst)) {
      return array();
    }
    if (count($lst) == 1) {
      return current($lst);
    }
    $lst = call_user_func_array($func, $lst);
    return count($lst) ? $lst : NULL;
  }

  /**
   * Idfilter.
   */
  public function idFilter($inList, $exList) {
    $inList = $this->combineLists($inList, 'array_intersect');
    $exList = $this->combineLists($exList, 'array_merge');

    if (is_null($inList) || is_null($exList)) {
      return NULL;
    }
    if (count($inList)) {
      if (count($exList)) {
        $inList = array_diff($inList, $exList);
        if (!count($inList)) {
          return NULL;
        }
      }
      return array("IN", $inList);
    }
    if (count($exList)) {
      return array("!IN", $exList);
    }
    return array(NULL, NULL);
  }

  /**
   * Full text filter.
   */
  public function fulltextFilter($operator, $value) {
    if ($operator == 'match') {
      return array("LIKE", $value);
    }
    $value = trim(preg_replace('~[!\\[\\]"^$]+~', " ", $value));
    if (strlen($value)) {
      switch ($operator) {
        case 'is':
          return array('LIKE', "^$value\$");

        case 'contain':
          return array('LIKE', $value);

        case 'not_contain':
          return array('!LIKE', $value);

        case 'start':
          return array('LIKE', "^$value");

        case 'end':
          return array('LIKE', "$value\$");

      }
    }
    return array(NULL, NULL);
  }

  /**
   * Make filters.
   */
  public function makeFilters($query, $useSelected = TRUE) {

    $filters = array();

    $selected = array(
      'in:source_id' => array(),
      'ex:source_id' => array(),
      'in:merchant_id' => array(),
      'ex:merchant_id' => array(),
    );

    $allNetworks = $this->ids($this->allNetworks());

    if ($useSelected) {
      $selected['in:source_id'][] = $this->ids($this->selectedNetworks());
      $selected['in:merchant_id'][] = $this->ids($this->selectedMerchants());
    }
    else {
      $selected['in:source_id'][] = $allNetworks;
    }

    foreach ($query as $params) {
      $fname = @$params['field'];
      $value = $this->get($params, 'value');
      $value2 = $this->get($params, 'value2');
      $operator = strtolower($this->get($params, 'operator'));

      switch ($fname) {
        case 'any':
        case 'name':
        case 'brand':
        case 'description':
        case 'category':
          $s = $this->fulltextFilter($operator, $value);
          if (!is_null($s[0])) {
            $filters[] = "{$fname} {$s[0]} {$s[1]}";
          }
          break;

        case 'tags':
          $operator = ($operator == 'in') ? "LIKE" : "!LIKE";
          $filters[] = "{$fname} {$operator} {$value}";
          break;

        case 'currency':
          $op = ($operator == 'is') ? '=' : '!=';
          $filters[] = "{$fname} $op {$value}";
          break;

        case 'id':
          $op = ($operator == 'is') ? 'IN' : '!IN';
          $filters[] = "{$fname} $op {$value}";
          break;

        case 'price':
        case 'finalprice':
        case 'saleprice':
        case 'salediscount':
          $conv = $fname == 'salediscount' ? 'intval' : 'dfrapi_price_to_int';
          $value = $conv($value);
          switch ($operator) {
            case 'between':
              $value2 = $conv($value2);
              $filters[] = "{$fname} > {$value}";
              $filters[] = "{$fname} < {$value2}";
              break;

            case 'eq':
              $filters[] = "{$fname} = {$value}";
              break;

            case 'lt':
              $filters[] = "{$fname} < {$value}";
              break;

            case 'lte':
              $filters[] = "{$fname} <= {$value}";
              break;

            case 'gt':
              $filters[] = "{$fname} > {$value}";
              break;

            case 'gte':
              $filters[] = "{$fname} >= {$value}";
              break;
          }
          break;

        case 'source_id':
        case 'merchant_id':
          $key = ($operator == 'is') ? 'in' : 'ex';
          $selected["$key:$fname"][] = $this->ary($value);
          break;

        case 'type':
          $ids = array();
          foreach ($this->allNetworks() as $net) {
            if ($net['type'] == $value) {
              $ids[] = $net['_id'];
            }
          }
          $selected["in:source_id"][] = $ids;
          break;

        case 'onsale':
          $value = ($operator == 'yes') ? '1' : '0';
          $filters[] = "{$fname} = {$value}";
          break;

        case 'direct_url':
          $operator = ($operator == 'yes') ? '!EMPTY' : 'EMPTY';
          $filters[] = "{$fname} {$operator}";
          break;

        case 'image':
        case 'thumbnail':
          $operator = ($operator == 'yes') ? '!EMPTY' : 'EMPTY';
          $filters[] = "image {$operator}";
          break;

        case 'time_updated':
          $value = @date('Y-m-d H:i:s', strtotime($value));
          switch ($operator) {
            case 'between':
              $value2 = @date('Y-m-d H:i:s', strtotime($value2));
              $filters[] = "{$fname} > {$value}";
              $filters[] = "{$fname} < {$value2}";
              break;

            case 'lt':
              $filters[] = "{$fname} < {$value}";
              break;

            case 'gt':
              $filters[] = "{$fname} > {$value}";
              break;

          }
          break;
      }
    }

    $s = $this->idFilter($selected['in:source_id'], $selected['ex:source_id']);
    if (is_null($s)) {
      return array('error' => 'No networks selected');
    }
    if (!is_null($s[0]) && count($s[1]) < count($allNetworks)) {
      $filters[] = "source_id {$s[0]} " . implode(',', $s[1]);
    }

    $s = $this->idFilter($selected['in:merchant_id'], $selected['ex:merchant_id']);
    if (is_null($s)) {
      return array('error' => 'No merchants selected');
    }
    if (!is_null($s[0])) {
      $m = implode(',', $s[1]);
      if ($m) {
        $filters[] = "merchant_id {$s[0]} $m";
      }
    }

    return $filters;
  }

  /**
   * Provide help tip.
   */
  public function helpTip($tip) {
    return '
    	<div class="dfrapi_search_tip">
    		<span class="dashicons dashicons-lightbulb"></span>
    		<p>
    			<strong>' . t('TIP:') . '</strong> ' .
        $tip . '
    		</p>
    	</div>
    	';
  }

  /**
   * Provide help links.
   */
  public function helpOperators() {
    $learn_more_link = t('!learn_more about advanced uses of this field.', array(
      '!learn_more' => l(t('Learn more'), 'https://v4.datafeedr.com/node/620', array(
        'attributes' => array(
          'target' => '_blank',
        ),
        'external' => TRUE,
      )
      ),
    )
    );
    return '
    		<p>
    			<span class="dashicons dashicons-info"></span> 
    			<em>' . $learn_more_link . '</em>
    		</p>
    	';
  }

  /**
   * Function for help content.
   */
  public function help($field) {
    $help = array();

    // Any.
    $help['any'] = '<h3>' . t('Any Field') . '</h3>';
    $help['any'] .= '<p>' . t('Search all indexed text fields at once to return a broad set of results.') . '</p>';
    $help['any'] .= '<p>' . t('The fields listed below are indexed as text fields and searchable using Any Field. Image, currency, and price fields are not searched.') . '</p>';
    $help['any'] .= '<p><em>' . t('*Note that not all merchants and networks provide every field.') . ' </em></p>';
    $help['any'] .= '<p>' . t('Fields searched using Any Field:') . '</p>';
    $help['any'] .= '<table width="100%" border="0" style="margin-bottom:20px"><tr><td width="33%" valign="top">accommodationtype<br />address<br />artist<br />author<br />bestsellers<br />brand<br />category<br />city<br />color<br />commissiontype<br />condition<br />country<br />county<br />description<br />destination<br />discount</td><td width="33%" valign="top">discounttype<br />ean<br />fabric<br />featured<br />flavour<br />gender<br />genre<br />isbn<br />language<br />location<br />manufacturer<br />manufacturerid<br />material<br />model<br />mpn<br />name</td><td width="33%" valign="top">offercode<br />offertype<br />platform<br />productnumber<br />promo<br />publisher<br />rating<br />region<br />size<br />sku<br />stars<br />state<br />subcategory<br />tags<br />upc<br />weight</td></tr></table>';
    $help['any'] .= $this->helpOperators();

    // Name.
    $help['name'] = '<h3>' . t('Product Name') . '</h3>';
    $help['name'] .= '<p>' . t('Search by product name to narrow your results.') . '</p>';
    $help['name'] .= $this->helpTip(t('Some merchants include color, size, gender, product codes, sale information and promotions in the product name field.'));
    $help['name'] .= $this->helpOperators();

    // Brand.
    $help['brand'] = '<h3>' . t('Brand') . '</h3>';
    $help['brand'] .= '<p>' . t('Search by brand name to get specific results. Not every item has a brand name.') . '</p>';
    $help['brand'] .= $this->helpTip(t('Enter the shorter version of a brand name in this field. Omit words such as "Incorporated", "Limited" and their abbreviations like Inc., Ltd. and so on.'));
    $help['brand'] .= $this->helpOperators();

    // Description.
    $help['description'] = '<h3>' . t('Description') . '</h3>';
    $help['description'] .= '<p>' . t('Search the description field for product attributes such as size, color, material, or usage.') . '</p>';
    $help['description'] .= $this->helpTip(t('Most merchants provide a product description. Some product details may only appear in the description and not in any other field. However, some merchants duplicate the product name or product code in the description field without supplying additional details or leave the product description blank.'));
    $help['description'] .= $this->helpOperators();

    // Tags.
    $help['tags'] = '<h3>' . t('Tags') . '</h3>';
    $help['tags'] .= '<p>' . t('Limit search results based on product tag and keyword information.') . '</p>';
    $help['tags'] .= $this->helpTip(t('This field contains data from various keyword-related fields provided by the merchant. This field does not always exist.'));

    // Category.
    $help['category'] = '<h3>' . t('Category') . '</h3>';
    $help['category'] .= '<p>' . t('Limit search results based on category information.') . '</p>';
    $help['category'] .= $this->helpTip(t('This field contains data from various category-related fields provided by the merchant. This field does not always exist.'));
    $help['category'] .= $this->helpOperators();

    // Type.
    $help['type'] = '<h3>' . t('Product Type') . '</h3>';
    $help['type'] .= '<p>' . t('Limit your search results to one type of item, either Product or Coupon.') . '</p>';
    $help['type'] .= $this->helpTip(t('In order to use this filter, you must have already selected merchants that provide that type of item. For example, if you choose "Product type: Coupon" but have not selected any merchants that offer coupons, your search will return an error.'));

    // Currency.
    $help['currency'] = '<h3>' . t('Currency') . '</h3>';
    $help['currency'] .= '<p>' . t('Limit your search results to items with a specific currency code.') . '</p>';
    $help['currency'] .= $this->helpTip(t('Selecting a currency code is one way to limit your search results to items from a specific country. However, not every item has been given a currency code value by the merchant. Items without a currency code will be excluded from your search results.'));

    // Price.
    $help['price'] = '<h3>' . t('Price') . '</h3>';
    $help['price'] .= '<p>' . t('Filter your search results based on price. Return products less than, greater than, or within a price range that you set.') . '</p>';
    $help['price'] .= $this->helpTip(t('This field does not search on sale price. If you set a price range to less than 30, you will exclude an item with a regular price of 40 that is on sale for less than 30.'));

    // Sale Price.
    $help['saleprice'] = '<h3>' . t('Sale Price') . '</h3>';
    $help['saleprice'] .= '<p>' . t('Filter your search results based on sale price. Return items less than, greater than, or within a sale price range that you set.') . '</p>';
    $help['saleprice'] .= $this->helpTip(t("This field does not search on regular price.  If you set a sale price range to between 50 and 100, an item with a sale price of 40 will be excluded even if it's regular price matches the range you set."));

    // Final Price.
    $help['finalprice'] = '<h3>' . t('Final Price') . '</h3>';
    $help['finalprice'] .= '<p>' . t('Filter your search results based on the final price. Return items less than, greater than, or within a sale price range that you set.') . '</p>';
    $help['finalprice'] .= $this->helpTip(t('The final price is the lower price when comparing the regular price and sale price field.'));

    // Network.
    $help['source_id'] = '<h3>' . t('Network') . '</h3>';
    $help['source_id'] .= '<p>' . t('Limit your search results to items from one or more affiliate networks.') . '</p>';
    $help['source_id'] .= $this->helpTip(t('Affiliate networks are generally country-specific. Using the Network filter is one way to limit your search results by country.'));

    // Merchant.
    $help['merchant_id'] = '<h3>' . t('Merchant') . '</h3>';
    $help['merchant_id'] .= '<p>' . t('Limit your search results to items from one or more merchants.') . '</p>';
    $help['merchant_id'] .= $this->helpTip(t('Using the Merchant filter is a quick way to exclude unrelated products and reduce the number of API requests made when building a Product Set. For example, if you are creating a Product Set related to cat products, exclude all merchants that only sell dog products.'));

    // On Sale.
    $help['onsale'] = '<h3>' . t('On Sale') . '</h3>';
    $help['onsale'] .= '<p>' . t('Set this field to "<strong>yes</strong>" to return only items which are on sale. To exclude products which are on sale, set this field to "<strong>no</strong>".') . '</p>';

    // Has Direct URL.
    $help['direct_url'] = '<h3>' . t('Has Direct URL') . '</h3>';
    $help['direct_url'] .= '<p>' . t("Limit your search results to items which have a direct URL (or which don't).") . '</p>';
    $help['direct_url'] .= $this->helpTip(t("A 'Direct URL' is a URL directly to the product page on the merchant's website. By default, the 'Direct URL' is never used as the URL in your 'Buy' links. However, if you need the products in your store to contain a 'Direct URL' to the product page on the merchants' websites, then you should use this filter. This is most useful if you are using Skimlinks (or similar service) to generate your affiliate links instead of the affiliate networks."));

    // Discount.
    $help['salediscount'] = '<h3>' . t('Discount') . '</h3>';
    $help['salediscount'] .= '<p>' . t('Limit your search results to items with a specified discount. Enter the number in terms of percentage (1 - 100) to indicate a discount less than, greater than, or between a given range. You do not need to enter the percentage sign.') . '</p>';
    $help['salediscount'] .= $this->helpTip(t('To display, for example, only products that are on sale for a discount of 20% or more, choose the "greater than" operator and type "19".'));

    // Has Image.
    $help['image'] = '<h3>' . t('Has Image') . '</h3>';
    $help['image'] .= '<p>' . t("Limit your search results to items which have an image (or which don't).") . '</p>';
    $help['image'] .= $this->helpTip(t("Sometimes the image URL in the merchant's data feed is broken. Items with broken images will still return in search results even though there appears to be no image."));

    // Last Updated.
    $help['time_updated'] = '<h3>' . t('Last Updated') . '</h3>';
    $help['time_updated'] .= '<p>';
    $help['time_updated'] .= t("Filter products by the last time they were updated by the merchant. Enter an English textual datetime description using PHP's !link function.", array(
      '!link' => l(t('strtotime()'), "http://www.php.net/strtotime", array(
        'attributes' => array(
          'target' => '_blank',
        ),
        'external' => TRUE,
      )
      ),
    )
    );
    $help['time_updated'] .= '</p>';
    $help['time_updated'] .= '<h3>' . t('Examples') . '</h3>';
    $help['time_updated'] .= '<p>';
    $help['time_updated'] .= '<tt>' . t('last week') . '</tt><br />';
    $help['time_updated'] .= '<tt>' . t('2 days ago') . '</tt><br />';
    $help['time_updated'] .= '<tt>' . t('1 month ago') . '</tt>';
    $help['time_updated'] .= '</p>';
    $help['time_updated'] .= '<p></p>';
    $help['time_updated'] .= $this->helpTip(t("An item's Last Updated date will not change if the merchant does not change any product information during a data feed update."));

    // Limit.
    $help['limit'] = '<h3>' . t('Limit') . '</h3>';
    $help['limit'] .= '<p>' . t('Limit the number of items returned in your search results. The maximum number of products that can be returned is 10,000.') . '</p>';
    $help['limit'] .= $this->helpTip(t('Limiting the number of products returned helps reduce the number of API requests made during searching and updating Product Sets.'));

    // Sort By.
    $help['sort'] = '<h3>' . t('Sort By') . '</h3>';
    $help['sort'] .= '<p>' . t('Change the sort criteria by which items are displayed in your search results.') . '</p>';
    $help['sort'] .= $this->helpTip(t('By default, search results are sorted by relevance. You can change that to sort by price, sale price, discount, or last updated date in ascending order (lowest to highest) or descending order (highest to lowest). You can also sort by merchant, which lists items alphabetically by merchant name.'));

    // Exclude Duplicates.
    $help['duplicates'] = '<h3>' . t('Exclude Duplicates') . '</h3>';
    $help['duplicates'] .= '<p>' . t('Exclude items that contain identical product names, image URLs, etc. Enter one or more terms from the list below. Separate terms by a space (meaning AND) or | (pipe symbol, meaning OR).') . '<br />';
    $help['duplicates'] .= '<p>' . t('Enter one of these terms to exclude duplicates matching these fields:') . '<br />';
    $help['duplicates'] .= '<tt>' . t('name') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('brand') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('currency') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('price') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('saleprice') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('source_id') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('merchant_id') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('direct_url') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('onsale') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('image') . '</tt><br />';
    $help['duplicates'] .= '<tt>' . t('thumbnail') . '</tt>';
    $help['duplicates'] .= '</p>';
    $help['duplicates'] .= '<h3>' . t('Examples') . '</h3>';
    $help['duplicates'] .= '<p>';
    $help['duplicates'] .= '<tt>' . t('image!ttc - Exclude items which have the same image URL.', array('!ttc' => '</tt>')) . '<br />';
    $help['duplicates'] .= '<tt>' . t('name image!ttc - Exclude items with the same name AND the same image URL.', array('!ttc' => '</tt>')) . '<br />';
    $help['duplicates'] .= '<tt>' . t('name|image!ttc - Exclude items with the same name OR the same image URL.', array('!ttc' => '</tt>')) . '<br />';
    $help['duplicates'] .= '<tt>' . t('merchant_id name|image!ttc - Exclude items which have the same merchant id AND (product name OR image URL).', array('!ttc' => '</tt>')) . '<br />';
    $help['duplicates'] .= '</p>';
    $help['duplicates'] .= $this->helpTip(t('By excluding duplicates, you will eliminate all but one item. For example, if 20 products have identical image URLs and you exclude duplicates matching the <strong>image</strong> field, <em>one</em> item will be returned and 19 items will be excluded.'));

    if (isset($help[$field])) {
      return $help[$field];
    }
    else {
      return '';
    }
  }

}
