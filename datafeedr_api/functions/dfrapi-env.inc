<?php

/**
 * @file
 * This file is used to check the environment errors.
 */

/**
 * Check for API key configuration.
 */
function check_environment() {

  // Cascading Errors.
  if (!api_keys_exist()) {

    dfrapi_admin_messages('missing_api_keys');
  }
  elseif (!network_is_selected()) {

    dfrapi_admin_messages('missing_network_ids');
  }
  elseif (!merchant_is_selected()) {

    dfrapi_admin_messages('missing_merchant_ids');
  }

  // Non-cascading Errors.
  if (usage_over_90_percent()) {
    dfrapi_admin_messages('usage_over_90_percent');
  }

  if (missing_affiliate_ids()) {
    dfrapi_admin_messages('missing_affiliate_ids');
  }

  if (check_gmt_time()) {
    dfrapi_admin_messages('database_rotation');
  }

  if (unapproved_zanox_merchants_exist()) {
    dfrapi_admin_messages('unapproved_zanox_merchants');
  }
}

/**
 * Check if API key either exist or not.
 *
 * @return bool
 *   Check for the api key.
 */
function api_keys_exist() {

  $configuration = variable_get('dfrapi_configuration', array());
  $access_id = FALSE;
  $secret_key = FALSE;

  if (isset($configuration['access_id']) && ($configuration['access_id'] != '')) {
    $access_id = $configuration['access_id'];
  }

  if (isset($configuration['secret_key']) && ($configuration['secret_key'] != '')) {
    $secret_key = $configuration['secret_key'];
  }

  if ($access_id && $secret_key) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Provide selected network ID.
 *
 * @return bool
 *   Check for the network id.
 */
function network_is_selected() {
  $networks = variable_get('dfrapi_networks', array());
  if (!empty($networks['ids'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Provide selected merchant ID.
 *
 * @return bool
 *   Check for the merchant ID.
 */
function merchant_is_selected() {
  $merchants = variable_get('dfrapi_merchants', array());
  if (!empty($merchants['ids'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Provide API consumption on site.
 *
 * @return bool
 *   Check for the usages percentage greater than 90 percentage.
 */
function usage_over_90_percent() {
  $percentage = dfrapi_get_api_usage_percentage();
  if ($percentage >= 90) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Check whether Affiliate ID is available or not.
 *
 * @return bool
 *   Check for the missing affiliate ID.
 */
function missing_affiliate_ids() {
  $networks = variable_get('dfrapi_networks', array());
  if (!empty($networks)) {
    foreach ($networks['ids'] as $network) {
      if (empty($network['aid'])) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Check GMT time.
 *
 * @return bool
 *   GMT time check.
 */
function check_gmt_time() {
  $gmt_time = gmdate('Gis');
  if ($gmt_time > 80000 && $gmt_time < 82000) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Check zanox merchant existence.
 *
 * @return bool
 *   Zanox merchant exit then TRUE else FALSE
 */
function unapproved_zanox_merchants_exist() {
  $results = array();
  $results = db_select('cache', 'c')
    ->fields('c', array('cid'))
    ->condition('c.cid', db_like('zmid_') . '%', 'LIKE')
    ->condition('c.data', db_like('dfrapi_unapproved_zanox_merchant'), 'LIKE')
    ->execute()
    ->fetchAssoc();
  if (!empty($results['cid'])) {
    return TRUE;
  }
  return FALSE;
}
