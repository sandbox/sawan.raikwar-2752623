<?php

/**
 * @file
 * This file is used to define the admin helper functions.
 */

/**
 * This stores messages to be displayed using the 'admin_notices' action.
 */
function dfrapi_admin_messages($key = FALSE) {
  $basepath = base_path();
  $zanox_merchants_sel_link = l(t('Zanox merchant selection'), "admin.php", array(
    'attributes' => array(
      'target' => '_blank',
    ),
    'query' => array(
      'page' => 'dfrapi_merchants',
    ),
    'external' => TRUE,
  )
    );
  $link_here = l(t('here'), "admin.php", array(
    'attributes' => array(
      'target' => '_blank',
    ),
    'query' => array(
      'page' => 'dfrapi_merchants',
    ),
    'external' => TRUE,
  )
    );
  $zanox_merchants_msg = t('!tag_open Unapproved Zanox Merchant(s): !tag_closed It appears you are not approved by one or more Zanox merchants you have selected. Please remove unapproved Zanox merchants from your !zanox_merchants_sel_link then delete your cached API data !link_here.', array(
    '!tag_open' => '<strong>',
    '!tag_closed' => '</strong>',
    '!zanox_merchants_sel_link' => $zanox_merchants_sel_link,
    '!link_here' => $link_here,
  )
      );
  $messages = array(
    // User is missing 1 or more of their API Keys.
    'missing_api_keys' => array(
      'class' => 'update-nag',
      'message' => t('Your Datafeedr API Keys are missing.&nbsp;'),
      'url' => $basepath . 'admin/config/datafeedr/settings',
      'button_text' => t('Add Your API Keys'),
    ),
    // User is missing a network.
    'missing_network_ids' => array(
      'class' => 'update-nag',
      'message' => t("You haven't selected any affiliate networks yet.&nbsp;"),
      'url' => $basepath . 'admin/config/datafeedr/settings/networks',
      'button_text' => t('Select Networks'),
    ),
    // User is missing a merchant.
    'missing_merchant_ids' => array(
      'class' => 'update-nag',
      'message' => t("You haven't selected any merchants yet.&nbsp;"),
      'url' => $basepath . 'admin/config/datafeedr/settings/merchants',
      'button_text' => t('Select Merchants'),
    ),
    // Display message that user has used 90%+ of API requests.
    'usage_over_90_percent' => array(
      'class' => 'update-nag',
      'message' => t('You have used !percentage % of your total Datafeedr API requests this month.', array('!percentage' => dfrapi_get_api_usage_percentage())),
      'url' => $basepath . 'admin/config/datafeedr/settings/account',
      'button_text' => t('View Account'),
    ),
    // Missing affiliate IDs message.
    'missing_affiliate_ids' => array(
      'class' => 'update-nag',
      'message' => t('You are missing affiliate IDs.&nbsp;'),
      'url' => $basepath . 'admin/config/datafeedr/settings/networks',
      'button_text' => t('Enter your Affiliate IDs'),
    ),
    // Database rotation message.
    'database_rotation' => array(
      'class' => 'update-nag',
      'message' => t('!so Datafeedr API Message: !sc We are currently refreshing our database of 270 million products. This process starts daily at 8:00am GMT and runs for about 20 minutes. During this time you may be unable to query our database.', array('!so' => '<strong>', '!sc' => '</strong>')),
      'url' => '',
      'button_text' => '',
    ),
    // Unapproved Zanox merchant(s) message.
    'unapproved_zanox_merchants' => array(
      'class' => 'update-nag',
      'message' => $zanox_merchants_msg,
      'url' => '',
      'button_text' => '',
    ),
  );
  if (isset($messages[$key])) {
    dfrapi_admin_notices($key, $messages);
  }
}

/**
 * This gets any notices set by the plugin.
 */
function dfrapi_admin_notices($key, $messages) {
  $notices = variable_get('dfrapi_admin_notices');
  $notices[$key] = $messages[$key];
  variable_set('dfrapi_admin_notices', $notices);
}

/**
 * Button text for admin notices.
 */
function dfrapi_fix_button($url, $button_text = FALSE) {
  if (!$button_text) {
    $button_text = t('Fix This Now');
  }
  if (substr($url, 0, 4) === "http") {
    return l($button_text, $url, array('attributes' => array('target' => '_blank')));
  }
  else {
    return l($button_text, $url, array('attributes' => array('target' => '_blank')));
  }
}

/**
 * Return plan IDs & names.
 */
function dfrapi_get_membership_plans() {
  return array(
    10200 => 'Free',
    101000 => 'Starter',
    1025000 => 'Basic',
    1125000 => 'Beta Tester',
    10100000 => 'Professional',
    10250000 => 'Enterprise',
  );
}

/**
 * Convert network group names to a css class name.
 */
function dfrapi_group_name_to_css($group) {
  if (is_string($group)) {
    return strtolower(
        str_replace(
            array(" ", "-", "."), "", $group
        )
    );
  }
  elseif (is_array($group)) {
    $name = str_replace(array(" ", "-", "."), "", $group['group']);
    $type = ($group['type'] == 'coupons') ? '_coupons' : '';
    return strtolower($name . $type);
  }
}

/**
 * Set admin notices message.
 */
function admin_notices() {
  check_environment();
  if ($notices = variable_get('dfrapi_admin_notices')) {
    foreach ($notices as $key => $message) {
      $button = ($message['url'] != '') ? dfrapi_fix_button($message['url'], $message['button_text']) : '';
      $upgrade_account = ($key == 'usage_over_90_percent') ? ' | <a href="' . dfrapi_user_pages('change') . '?utm_source=plugin&utm_medium=link&utm_campaign=upgradenag">' . t('Upgrade') . '</a>' : '';
      drupal_set_message($message['message'] . $button . $upgrade_account, 'warning', FALSE);
    }
    variable_set('dfrapi_admin_notices', '');
  }
}
