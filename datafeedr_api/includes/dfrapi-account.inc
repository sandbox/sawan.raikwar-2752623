<?php

/**
 * @file
 * This file used to display the account summary page of Datafeedr API module.
 */

/**
 * Page callback for account.
 */
function datafeedr_account() {
  $account = variable_get('dfrapi_account', array());

  $status = array();
  if (isset($account[0]) && empty($account[0])) {
    $account = $status = dfrapi_api_get_status();
    if (!array_key_exists('dfrapi_api_error', $status)) {
      variable_set('dfrapi_account', $account);
    }
  }

  $plans = dfrapi_get_membership_plans();
  $plan_name = '';
  if (isset($account['plan_id']) && $account['plan_id'] > 0) {
    $plan_name .= $plans[$account['plan_id']];
    if ($account['plan_id'] != 10250000) {
      $plan_name .= ' (';
      $plan_name .= l(t('Upgrade'), dfrapi_user_pages('change'), array(
        'attributes' => array(
          'target' => '_blank',
          'title' => t('Upgrade'),
          'class' => array('dfrapi_plan_link'),
        ),
        'external' => TRUE,
        'query' => array(
          'utm_source' => 'plugin',
          'utm_medium' => 'link',
          'utm_campaign' => 'dfrapiconfigpage',
        ),
      )
      );
      $plan_name .= ')';
    }
  }
  else {
    $plan_name .= '<em>';
    $plan_name .= t('None');
    $plan_name .= '</em> (';
    $plan_name .= l(t('Reactivate your subscription'), dfrapi_user_pages('signup'),
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Reactivate your subscription'),
            'class' => array('dfrapi_plan_link'),
          ),
          'external' => TRUE,
          'query' => array(
            'utm_source' => 'plugin',
            'utm_medium' => 'link',
            'utm_campaign' => 'dfrapiconfigpage',
          ),
        )
      );
    $plan_name .= ')';
  }

  // Percent of API Requests Used.
  $percent_api_requests_users = '';
  if (isset($account['max_requests']) && $account['max_requests'] > 0) {
    $percent_api_requests_users .= floor((intval($account['request_count']) / intval($account['max_requests']) * 100));
  }
  else {
    $percent_api_requests_users .= 0;
  }
  $percent_api_requests_users .= '%';

  // Your API requests will reset on.
  $reset_date = '';
  $today = date('j');
  $num_days = date('t');
  $bill_day = 0;
  if (isset($account['bill_day'])) {
    if ($account['bill_day'] > $num_days) {
      $bill_day = $num_days;
    }
    else {
      $bill_day = $account['bill_day'];
    }
  }
  if ($bill_day == 0) {
    $reset_date .= '<em>' . t('Never') . '</em>';
  }
  elseif ($today >= $bill_day) {
    $reset_date .= date('F', strtotime('+1 month')) . ' ' . $bill_day . ', ' . date('Y', strtotime('+1 month'));
  }
  else {
    $reset_date .= date('F') . ' ' . $bill_day . ', ' . date('Y');
  }

  $max_requests = 0;
  if (isset($account['max_requests'])) {
    $max_requests = $account['max_requests'];
  }

  $max_length = 0;
  if (isset($account['max_length'])) {
    $max_length = $account['max_length'];
  }

  $request_count = 0;
  if (isset($account['request_count'])) {
    $request_count = $account['request_count'];
  }
  $rows = array(
    array(
      'data' => array(
        array(
          'data' => t('Plan Name'),
          'width' => '30%',
        ),
        $plan_name,
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => t('Request Per Month(RPM)'),
          'width' => '30%',
        ),
        number_format($max_requests),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => t('Products Per Request(PPR)'),
          'width' => '30%',
        ),
        number_format($max_length),
      ),
    ),
  );
  $output = theme('table', array('header' => array(), 'rows' => $rows));
  if (!array_key_exists('dfrapi_api_error', $status)) {
    $output .= '<h3>';
    $output .= t('Current Usage');
    $output .= '</h3>';
    $rows2 = array(
      array(
        'data' => array(
          array(
            'data' => t('API Requests Used This Period'),
            'width' => '30%',
          ),
          number_format($request_count),
        ),
      ),
      array(
        'data' => array(
          array(
            'data' => t('API Requests Remaining This Period'),
            'width' => '30%',
          ),
          number_format($max_requests - $request_count),
        ),
      ),
      array(
        'data' => array(
          array(
            'data' => t('Percent of API Requests Used This Period'),
            'width' => '30%',
          ),
          $percent_api_requests_users,
        ),
      ),
      array(
        'data' => array(
          array(
            'data' => t('Your API Requests Reset On'),
            'width' => '30%',
          ),
          $reset_date,
        ),
      ),
    );
    $output .= theme('table', array('header' => array(), 'rows' => $rows2));
  }
  return $output;
}
