<?php

/**
 * @file
 * This file is used to implement the hooks for Datafeedr Network page.
 */

/**
 * Network configuration form.
 *
 * Returns list of networks details.Also we are comparing two
 *   networks and enabling the check boxes which are chosen by admin.
 */
function datafeedr_networks_form($form, &$form_state) {
  global $base_path;

  // Getting API settings data.
  $dfrapi_configuration = variable_get('dfrapi_configuration', array());

  // Get all networks.
  if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
    $networks_list = datafeedr_api_sample_network_merchant('network');
  }
  else {
    $networks_list = dfrapi_api_get_all_networks();
  }

  $ntlist = array();
  foreach ($networks_list as $val) {
    $groupname = $val[key($val)];
    $ntlist[$groupname][] = $val;
  }

  $form = array();
  $form['network_page'] = array(
    '#markup' => '<h3 class="page-title">' . t('Networks &#8212; Datafeedr API') . '</h3>',
  );
  $form['network_section'] = array(
    '#markup' => '<h4>' . t('Select Networks') . '</h4>',
  );
  $form['network_section_desc'] = array(
    '#markup' => '<p>' . t('Select the affiliate networks you belongs to, enter your affiliate id for each then click !bo[Save Changes]!bc.', array('!bo' => '<b>', '!bc' => '</b>')) . '</p>',
  );

  // Get the selected networks list from the database.
  $dfrapi_networks = variable_get('dfrapi_networks', array());
  if (isset($dfrapi_networks['ids'])) {
    $dbNetworksList = $dfrapi_networks['ids'];
  }

  $networks_list1 = $ntlist;

  foreach ($networks_list1 as $mykey => $myval) {
    $key_network = $mykey;
    $form[$key_network] = array(
      '#title' => network_details($mykey, $networks_list),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#required' => FALSE,
      '#tree' => TRUE,
      '#prefix' => '<div id="dfrapi_networks">',
      '#suffix' => '</div>',
    );
    $group_name = $key_network;
    $dfr_network_link = map_link($group_name);
    $dfr_network_link = ($dfr_network_link != '') ? $dfr_network_link : '#';
    $hint_img = '<img src="' . $base_path . drupal_get_path('module', 'datafeedr_api') . '/images/icons/help.png" alt="';
    $hint_img .= t('more info');
    $hint_img .= '" style="vertical-align: middle" />';
    $affiliate_hint = l($hint_img, $dfr_network_link, array(
      'attributes' => array(
        'target' => '_blank',
        'title' => t('Learn how to find your affiliate ID from !group_name opens in new window.', array('!group_name' => $group_name)),
      ),
      'external' => TRUE,
      'html' => TRUE,
    )
    );
    $tracking_hint = l($hint_img, DATAFEEDR_API_HOME_URL . '/node/1113', array(
      'attributes' => array(
        'target' => '_blank',
        'title' => t('Learn more about this field (opens in new window).'),
      ),
      'external' => TRUE,
      'html' => TRUE,
    )
    );
    $form[$key_network]['network_markup'] = array(
      '#type' => 'markup',
      '#markup' => '<table cellspacing=0 celladding=0>
      <tr>
      <th>#</th>
      <th>' . t('Network') . '</th>
      <th>' . t('Type') . '</th>
      <th>' . t('Affiliate') . ' ' . $affiliate_hint . ' <small style="font-weight:normal;color:#a00;">(' . t('required') . ')</small></th>
      <th>' . t('Tracking ID') . ' ' . $tracking_hint . ' <small style="font-weight:normal;color:#999;">(' . t('optional') . ')</small>
      </th>',
    );

    $dfnetwork_id = array();

    $check_true = FALSE;
    $affiliate_id = '';
    $tracking_id = '';
    for ($l = 0; $l < count($myval); $l++) {
      if (!empty($dbNetworksList)) {
        foreach ($dbNetworksList as $values) {
          $dfnetwork_id[] = $values['nid'];
          if (in_array($myval[$l]['_id'], $dfnetwork_id)) {
            $check_true = TRUE;
            if ($myval[$l]['_id'] == $values['nid']) {
              $affiliate_id = isset($values['aid']) ? $values['aid'] : '';
              $tracking_id = isset($values['tid']) ? $values['tid'] : '';
            }
          }
          else {
            $check_true = FALSE;
            $affiliate_id = '';
            $tracking_id = '';
          }
        }
      }

      $class = ($l % 2 == 1) ? 'even' : 'odd';
      $form[$key_network][$l]['networkid'] = array(
        '#type' => 'hidden',
        '#value' => $myval[$l]['_id'],
        '#prefix' => '<tr class="' . $class . '">',
      );
      $form[$key_network][$l]['network_checkbox'] = array(
        '#type' => 'checkbox',
        '#title' => '',
        '#size' => 10,
        '#validate' => '',
        '#default_value' => $check_true,
        '#prefix' => '<td>',
        '#suffix' => '</td>',
      );
      $myval_name = $myval[$l]['name'];
      $form[$key_network][$l]['nid'] = array(
        '#markup' => '<h6>' . $myval_name . '</h6>',
        '#prefix' => '<td style="width:200px;">',
        '#suffix' => '</td>',
      );
      $myval_type = $myval[$l]['type'];
      $form[$key_network][$l]['type'] = array(
        '#markup' => '<h6>' . $myval_type . '</h6>',
        '#prefix' => '<td>',
        '#suffix' => '</td>',
      );
      $form[$key_network][$l]['affiliates'] = array(
        '#type' => 'textfield',
        '#title' => '',
        '#size' => 50,
        '#default_value' => $affiliate_id,
        '#prefix' => '<td>',
        '#suffix' => '</td>',
      );
      $form[$key_network][$l]['trackingid'] = array(
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => $tracking_id,
        '#title' => '',
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>',
      );
    }
    $form[$key_network]['nework_markup_wrap'] = array(
      '#type' => 'markup',
      '#markup' => '</table>',
    );
  }

  $form['submit'] = array(
    '#title' => t('Submit'),
    '#type' => 'submit',
    '#value' => 'Save Changes',
  );
  return $form;
}

/**
 * Method should handle the submit values for Datafeedr configuration page.
 *
 * 1. Get the existing data from DB and keep the networkIds in Array
 * 2. Getting the submitting data in an Array
 * 3. Compare the NetworkIds Available IN Submit data,
 *   if the networkIds not in Submitted data remove those Networks from DB
 *   if the neworkIds available, then you need to Update the existing Data
 *   And additional data need to be inserted.
 */
function datafeedr_networks_form_submit($form, &$form_state) {

  $networkdata = $form_state['values'];

  if (!empty($networkdata)) {
    foreach ($networkdata as $networks) {
      if (is_array($networks)) {
        foreach ($networks as $n) {
          if ($n['network_checkbox'] == 1) {
            $selected_networks[$n['networkid']] = array(
              'nid' => $n['networkid'],
              'aid' => $n['affiliates'],
              'tid' => $n['trackingid'],
            );
          }
        }
      }
    }
  }
  $dfrapi_networks['ids'] = !empty($selected_networks) ? $selected_networks : array();

  variable_set('dfrapi_networks', $dfrapi_networks);
  drupal_set_message(t('Network data updated successfully.'));
}

/**
 * Return the number of missing affiliate ids in network group.
 */
function num_missing_affiliate_ids_in_group($group_name, $all_networks) {
  $count = 0;
  $dfrapi_networks = variable_get('dfrapi_networks', array());
  foreach ($all_networks as $network) {
    if ($network['group'] == $group_name) {
      if (isset($dfrapi_networks['ids'])) {
        if (array_key_exists($network['_id'], (array) $dfrapi_networks['ids'])) {
          if (trim($dfrapi_networks['ids'][$network['_id']]['aid']) == '') {
            $count++;
          }
        }
      }
    }
  }
  if ($count > 0) {
    return '<span class="num_missing">' . sprintf(format_plural($count, '%s missing affiliate ID', '%s missing affiliate IDs'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
}

/**
 * Return the number of selected networks in a network group.
 */
function num_networks_checked_in_group($group_name, $all_networks) {
  $count = 0;
  $dfrapi_networks = variable_get('dfrapi_networks', array());
  foreach ($all_networks as $network) {
    if (isset($network['group']) && $network['group'] == $group_name) {
      if (isset($dfrapi_networks['ids'])) {
        if (array_key_exists($network['_id'], (array) $dfrapi_networks['ids'])) {
          $count++;
        }
      }
    }
  }
  if ($count > 0) {
    return '<span class="num_checked">' . sprintf(format_plural($count, '%s network selected', '%s networks selected'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
}

/**
 * Return the number of networks in a network group.
 */
function num_networks_in_group($group_name, $all_networks) {
  $count = 0;
  foreach ($all_networks as $network) {
    if ($network['group'] == $group_name) {
      $count++;
    }
  }
  if ($count > 0) {
    return '<span class="num_networks">' . sprintf(format_plural($count, '%s network', '%s networks'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
}

/**
 * Return the number of merchant in network group.
 */
function num_merchants_in_group($group_name, $all_networks) {
  $count = 0;
  foreach ($all_networks as $network) {
    if ($network['group'] == $group_name) {
      $count += $network['merchant_count'];
    }
  }
  if ($count > 0) {
    return '<span class="num_merchants">' . sprintf(format_plural($count, '%s merchant', '%s merchants'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
}

/**
 * Return the number of products in network group.
 */
function num_products_in_group($group_name, $all_networks) {
  $count = 0;
  foreach ($all_networks as $network) {
    if ($network['group'] == $group_name) {
      $count += $network['product_count'];
    }
  }
  if ($count > 0) {
    return '<span class="num_products">' . sprintf(format_plural($count, '%s product', '%s products'), number_format($count)) . '</span>';
  }
}

/**
 * Function provide the network details.
 *
 * @param string $mykey
 *   Network key.
 * @param string $networks_list
 *   Network html list.
 */
function network_details($mykey, $networks_list) {
  $output = '';

  $num_networks_checked_in_group = num_networks_checked_in_group($mykey, $networks_list);
  $active = ($num_networks_checked_in_group != '') ? 'active' : '';
  $output = '<div class="group network_logo_30x30_' . dfrapi_group_name_to_css($mykey) . ' ' . $active . '" id="group_' . dfrapi_group_name_to_css($mykey) . '">
					<div class="meta">
						<span class="name">' . $mykey . '</span>
						<span class="status">
							' . num_missing_affiliate_ids_in_group($mykey, $networks_list) . '
							' . $num_networks_checked_in_group . '
							' . num_networks_in_group($mykey, $networks_list) . '
							' . num_merchants_in_group($mykey, $networks_list) . '
							' . num_products_in_group($mykey, $networks_list) . ' 
						</span>
					</div>
				</div>';
  return $output;
}

/**
 * Function provide the url for defined source.
 *
 * @param string $name
 *   Merchant name.
 */
function map_link($name) {
  $links = array(
    'Adrecord' => 'http://www.datafeedr.com/docs/item/252',
    'Adtraction' => 'http://www.datafeedr.com/docs/item/261',
    'Affiliate4You' => 'http://www.datafeedr.com/docs/item/265',
    'AffiliateWindow' => 'http://www.datafeedr.com/docs/item/101',
    'Affiliator' => 'http://www.datafeedr.com/docs/item/270',
    'Affilinet' => 'http://www.datafeedr.com/docs/item/104',
    'Amazon Local' => 'http://www.datafeedr.com/docs/item/275',
    'Avangate' => 'http://www.datafeedr.com/docs/item/188',
    'AvantLink' => 'http://www.datafeedr.com/docs/item/190',
    'Belboon' => 'http://www.datafeedr.com/docs/item/201',
    'BettyMills' => 'http://www.datafeedr.com/docs/item/264',
    'bol.com' => 'http://www.datafeedr.com/docs/item/253',
    'ClickBank' => 'http://www.datafeedr.com/docs/item/106',
    'ClixGalore' => 'http://www.datafeedr.com/docs/item/200',
    'Commission Factory' => 'http://www.datafeedr.com/docs/item/193',
    'Commission Junction' => 'http://www.datafeedr.com/docs/item/65',
    'Commission Monster' => 'http://www.datafeedr.com/docs/item/109',
    'Daisycon' => 'http://www.datafeedr.com/docs/item/196',
    'DGM' => 'http://www.datafeedr.com/docs/item/263',
    'Double.net' => 'http://www.datafeedr.com/docs/item/271',
    'FlipKart' => 'http://www.datafeedr.com/docs/item/273',
    'Impact Radius' => 'http://www.datafeedr.com/docs/item/268',
    'LinkConnector' => 'http://www.datafeedr.com/docs/item/105',
    'LinkShare' => 'http://www.datafeedr.com/docs/item/60',
    'M4N' => 'http://www.datafeedr.com/docs/item/198',
    'MyCommerce' => 'http://www.datafeedr.com/docs/item/111',
    'OneNetworkDirect' => 'http://www.datafeedr.com/docs/item/112',
    'Paid on Results' => 'http://www.datafeedr.com/docs/item/189',
    'Partner-ads' => 'http://www.datafeedr.com/docs/item/262',
    'PepperJam' => 'http://www.datafeedr.com/docs/item/103',
    'RegNow' => 'http://www.datafeedr.com/docs/item/111',
    'RevResponse' => 'http://www.datafeedr.com/docs/item/197',
    'ShareASale' => 'http://www.datafeedr.com/docs/item/108',
    'SuperClix' => 'http://www.datafeedr.com/docs/item/256',
    'TradeDoubler' => 'http://www.datafeedr.com/docs/item/113',
    'TradeTracker' => 'http://www.datafeedr.com/docs/item/195',
    'Webgains' => 'http://www.datafeedr.com/docs/item/114',
    'Zanox' => 'http://www.datafeedr.com/docs/item/269',
  );
  return (array_key_exists($name, $links)) ? $links[$name] : '';
}
