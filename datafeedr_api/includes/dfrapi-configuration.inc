<?php

/**
 * @file
 * This file implements Datafeedr API configurations form.
 */

/**
 * Method should return a form for Datafeedr configuration page.
 */
function datafeedr_configuration_form($form, &$form_state) {
  // Getting API settings data.
  $dfrapi_configuration = variable_get('dfrapi_configuration', array());

  $form['api_settings'] = array(
    '#markup' => '<h1>' . t('Datafeedr API Settings') . '</h1>',
  );
  $dfr_api_key = l(t('Datafeed API Keys'), DATAFEEDR_API_KEYS_URL, array(
    'attributes' => array(
      'target' => '_blank',
      'title' => t('Get your Datafeedr API Keys'),
    ),
    'external' => TRUE,
    'query' => array(
      'utm_source' => 'plugin',
      'utm_medium' => 'link',
      'utm_campaign' => 'dfrapiconfigpage',
    ),
  )
  );
  $form['api_settings_desc'] = array(
    '#markup' => '<p>' . t('Add your !dfr_api_key', array('!dfr_api_key' => $dfr_api_key)) . '</p>',
  );
  $form['access_id'] = array(
    '#type' => 'textfield',
    '#title' => t('API Access ID'),
    '#required' => 'true',
    '#default_value' => isset($dfrapi_configuration['access_id']) ? $dfrapi_configuration['access_id'] : '',
  );
  $form['secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret Key'),
    '#required' => 'true',
    '#default_value' => isset($dfrapi_configuration['secret_key']) ? $dfrapi_configuration['secret_key'] : '',
    '#size' => 80,
  );
  $form['transport_method'] = array(
    '#type' => 'select',
    '#title' => t('Transport Method'),
    '#required' => 'true',
    '#options' => array(
      'curl' => t('CURL'),
      'file' => t('File'),
      'socket' => t('Socket'),
    ),
    '#default_value' => isset($dfrapi_configuration['transport_method']) ? $dfrapi_configuration['transport_method'] : 'curl',
    '#description' => '<p><i>' . t("If you're not sure, use CURL.") . '</i></p>',
  );

  $form['zanox_api_settings'] = array(
    '#markup' => '<h1>' . t('Zanox Settings') . '</h1>',
  );
  $zanox_key = l(t('Zanox Keys'), "http://publisher.zanox.com/ws_gettingstarted/ws.gettingstarted.html", array(
    'attributes' => array(
      'target' => '_blank',
      'title' => t('Get your Zanox Keys'),
    ),
    'external' => TRUE,
  )
  );
  $form['zanox_api_settings_desc'] = array(
    '#markup' => '<p>' . t('If you want to use Zanox affiliate network, add your !zanox_key', array('!zanox_key' => $zanox_key)) . '</p>',
  );
  $form['zanox_connection_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Connection Key'),
    '#default_value' => isset($dfrapi_configuration['zanox_connection_key']) ? $dfrapi_configuration['zanox_connection_key'] : '',
  );
  $form['zanox_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => isset($dfrapi_configuration['zanox_secret_key']) ? $dfrapi_configuration['zanox_secret_key'] : '',
  );

  // Amazon API settings Fields.
  if (defined('DATAFEEDR_API_VERSION') && DATAFEEDR_API_VERSION == 1) {
    $form['amazon_api_settings'] = array(
      '#markup' => '<h1>' . t('Amazon Settings') . '</h1>',
    );
    $amazon_api_settings_desc = '<p>';
    $amazon_api_settings_desc .= t('Add your !amazon_key', array(
      '!amazon_key' => l(t('Amazon Product Advertising API keys'), "https://affiliate-program.amazon.com/gp/advertising/api/detail/your-account.html", array(
        'attributes' => array(
          'target' => '_blank',
          'title' => t('Get Amazon Product Advertising API keys'),
        ),
        'external' => TRUE,
      )
      ),
    )
    );
    $amazon_api_settings_desc .= '<br /><span style="color:red">';
    $amazon_api_settings_desc .= t('These are only compatible with the Datafeedr comparison sets plugin.');
    $amazon_api_settings_desc .= '</span>';
    $amazon_api_settings_desc .= '</p>';
    $form['amazon_api_settings_desc'] = array(
      '#markup' => $amazon_api_settings_desc,
    );
    $form['amazon_access_key_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon Access Key ID'),
      '#default_value' => isset($dfrapi_configuration['amazon_access_key_id']) ? $dfrapi_configuration['amazon_access_key_id'] : '',
    );
    $form['amazon_secret_access_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon Secret Access Key'),
      '#default_value' => isset($dfrapi_configuration['amazon_secret_access_key']) ? $dfrapi_configuration['amazon_secret_access_key'] : '',
    );
    $form['amazon_tracking_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon Tracking ID'),
      '#default_value' => isset($dfrapi_configuration['amazon_tracking_id']) ? $dfrapi_configuration['amazon_tracking_id'] : '',
      '#description' => t('This is the same as your Associates Tag usually ending in "-20" or "-21". Can be found !here', array(
        '!here' => l(
            t('here'), "https://affiliate-program.amazon.com/gp/associates/network/main.html", array(
              'attributes' => array(
                'target' => '_blank',
              ),
              'external' => TRUE,
            )
        ),
      )
      ),
    );

    $options_al = array(
      'br' => t('Brazil'),
      'ca' => t('Canada'),
      'cn' => t('China'),
      'fr' => t('France'),
      'de' => t('Germany'),
      'in' => t('India'),
      'it' => t('Italy'),
      'jp' => t('Japan'),
      'mx' => t('Mexico'),
      'es' => t('Spain'),
      'uk' => t('United Kingdom'),
      'us' => t('United States'),
    );
    $form['amazon_locale'] = array(
      '#type' => 'select',
      '#title' => t('Amazon Locale'),
      '#required' => 'true',
      '#options' => $options_al,
      '#default_value' => isset($dfrapi_configuration['amazon_locale']) ? $dfrapi_configuration['amazon_locale'] : 'us',
      '#description' => '<p>'
      . t('!aws_com regarding Amazon Locales.', array(
        '!aws_com' => l(
            t('More information'), "http://docs.aws.amazon.com/AWSECommerceService/latest/DG/AssociateIDs.html", array(
              'attributes' => array(
                'target' => '_blank',
              ),
              'external' => TRUE,
            )
        ),
      )
      ) . '</p>',
    );
  }

  // Check if sample data is used.
  $form['dfrapi_sample_data'] = array(
    '#type' => 'checkbox',
    '#title' => '<b>' . t('Use Sample Data') . '</b>',
    '#default_value' => isset($dfrapi_configuration['dfrapi_sample_data']) ? $dfrapi_configuration['dfrapi_sample_data'] : 1,
    '#description' => t('Check the check-box to use the sample networks, merchants and products data.<br />
    <ul>
      <li><strong>On check:</strong> Import the sample data and sample database table. It will take some time.</li>
      <li><strong>On un-check:</strong> Delete the sample data and sample database table.</li>
    </ul>'),
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );
  return $form;
}

/**
 * Method should handle the submit values for Datafeedr configuration page.
 */
function datafeedr_configuration_form_submit($form, &$form_state) {
  // Insert or delete dummy tables and data as per user selection.
  datafeedr_dump_remove_dummy_data($form_state['values']['dfrapi_sample_data']);
  $dfrapi_configuration = array(
    'access_id' => $form_state['values']['access_id'],
    'secret_key' => $form_state['values']['secret_key'],
    'transport_method' => isset($form_state['values']['transport_method']) ? $form_state['values']['transport_method'] : 'curl',
    'zanox_connection_key' => $form_state['values']['zanox_connection_key'],
    'zanox_secret_key' => $form_state['values']['zanox_secret_key'],
    'amazon_access_key_id' => '',
    'amazon_secret_access_key' => '',
    'amazon_tracking_id' => '',
    'amazon_locale' => isset($form_state['values']['amazon_locale']) ? $form_state['values']['amazon_locale'] : 'us',
    'dfrapi_sample_data' => ($form_state['values']['dfrapi_sample_data'] == 1) ? 1 : 0,
  );
  variable_set('dfrapi_configuration', $dfrapi_configuration);
  drupal_set_message(t("Datafeedr API Settings successfully saved."));
}

/**
 * Function to Insert or delete dummy tables and data as per user selection.
 *
 * @param mixed $mode
 *   Param $mode will be 0 or 1.
 *   Here 0 means drop dummy data tables.
 *   Here 1 Means create and insert dummy tables and data respectively.
 */
function datafeedr_dump_remove_dummy_data($mode) {
  // Drop tables is user don't choose dummy mode.
  if ($mode == 0) {
    db_drop_table('dfrapi_sample_merchants');
    db_drop_table('dfrapi_sample_networks');
    db_drop_table('dfrapi_sample_products');
    db_drop_table('dfrapi_sample_products_data_01');
    return;
  }
  // Check for dummy data insertion.
  if (db_table_exists('dfrapi_sample_merchants')) {
    return;
  }
  // Start transcation.
  $txn = db_transaction();
  set_time_limit(0);
  try {
    $module_root_path = realpath(__DIR__ . '/../..');
    $file_path = $module_root_path . '\dummy_data\dfrapi_sample_data.sql';

    // Temporary variable, used to store current query.
    $templine = '';
    // Read in entire file.
    $lines = file($file_path);
    global $databases;
    $con = mysqli_connect(
        $databases['default']['default']['host'],
        $databases['default']['default']['username'],
        $databases['default']['default']['password'],
        $databases['default']['default']['database']
    );
    // Loop through each line.
    foreach ($lines as $line) {
      // Skip it if it's a comment.
      if (substr($line, 0, 2) == '--' || $line == '') {
        continue;
      }

      // Add this line to the current segment.
      $templine .= $line;
      // If it has a semicolon at the end, it's the end of the query.
      if (substr(trim($line), -1, 1) == ';') {
        // Perform the query.
        mysqli_query($con, $templine);
        // Reset temp variable to empty.
        $templine = '';
      }
    }
    watchdog('message', 'Dummy data imported successfully.');
  }
  catch (Exception $e) {
    // Something went wrong somewhere, so roll back now.
    $txn->rollback();
    // Log the exception to watchdog.
    watchdog_exception('type', $e);
  }
}
