<?php

/**
 * @file
 * This file is used to display the export network and merchant list.
 */

/**
 * Method should return a form for Datafeedr Exporting page.
 */
function datafeedr_export_form($form, &$form_state) {

  // Networks Exporting Data.
  $dbNetworksList = variable_get('dfrapi_networks', array());
  $serialize_networkinfo = serialize($dbNetworksList);
  // Merchants Exporting Data.
  $dbMerchantsList = variable_get('dfrapi_merchants', array());
  $serialize_merchantinfo = serialize($dbMerchantsList);

  $form['export_data'] = array(
    '#markup' => t('To use the same selection of network on another website export this stores network data by copying the code below and paste the code into the import page of your other site.'),
  );
  $form['network_exportdata'] = array(
    '#type' => 'textarea',
    '#title' => t('Network'),
    '#value' => $serialize_networkinfo,
    '#description' => t('Click within the box to select all your networks and affiliate IDs.'),
    '#required' => TRUE,
  );
  $form['merchant_data'] = array(
    '#markup' => t('To use the same selection of merchant on another website export this stores merchant data by copying the code below and paste the code into the import page of your other site.'),
  );
  $form['merchant_exportdata'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant'),
    '#value' => $serialize_merchantinfo,
    '#description' => t('Click within the box to select all your merchant IDs.'),
    '#required' => TRUE,
  );
  return $form;
}
