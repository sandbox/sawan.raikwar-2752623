<?php

/**
 * @file
 * This file is used to implement the hooks for Datafeedr Merchant page.
 */

/**
 * Method should return a form for Datafeedr Merchant page.
 */
function datafeedr_merchants_form() {
  // Getting API settings data.
  $dfrapi_configuration = variable_get('dfrapi_configuration', array());

  // Get all networks.
  if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
    $all_networks = datafeedr_api_sample_network_merchant('network');
  }
  else {
    $all_networks = dfrapi_api_get_all_networks();
  }
  $output = '';
  $form = array();
  // Check the api error.
  if ($errors = api_errors($all_networks)) {
    $output = dfrapi_html_output_api_error($errors);
  }
  else {
    // User selected network.
    $users_networks = set_users_networks();
    // Get saved merchants.
    $dfrapi_merchants = variable_get('dfrapi_merchants', array());

    $form['merchant_page'] = array(
      '#markup' => '<h3 class="page-title">' . t('Merchants &#8212; Datafeedr API') . '</h3>',
    );
    $form['merchant_section'] = array(
      '#markup' => '<h4>' . t('Select Merchants') . '</h4>',
    );
    $form['merchant_section_desc'] = array(
      '#markup' => '<p>' . t('Select merchants from your affiliate networks then click !bo[Save Changes]!bc.', array('!bo' => '<b>', '!bc' => '</b>')) . '</p>',
    );
    $output .= '<div class="wrap" id="dfrapi_merchants">';
    if (isset($dfrapi_merchants['ids']) && is_array($dfrapi_merchants['ids']) && !empty($dfrapi_merchants['ids'])) {
      $ids = htmlspecialchars(implode(',', $dfrapi_merchants['ids']));
    }
    else {
      $ids = '';
    }
    $output .= '<input type="hidden" id="ids" name="dfrapi_merchants[ids]" value="' . $ids . '" />';
    foreach ($users_networks as $user_network) {
      $network = get_network_info($all_networks, $user_network);
      if (empty($network)) {
        $output .= t('No networks have been selected.');
        break;
      }
      $num_networks_checked_in_network = num_networks_checked_in_network($network['_id'], $dfrapi_merchants);
      $active = (preg_match("/num_checked_none/", $num_networks_checked_in_network)) ? '' : 'active';
      $output .= '
			<div class="network network_logo_30x30_' . dfrapi_group_name_to_css($network) . ' ' . $active . '" id="network_' . $network['_id'] . '">
				<div class="meta">
					<span class="name">' . $network['name'] . '</span>
					<span class="status">
						' . $num_networks_checked_in_network . '
						' . num_merchants_in_network($network) . '
						' . num_products_in_network($network) . ' 
					</span>
				</div>
				' . list_merchants($network['_id'], $dfrapi_merchants) . '
			</div>
			';
    }

    $output .= '</div>';
  }

  $form['merchant_list'] = array(
    '#markup' => $output,
  );

  $form['search_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );

  return $form;
}

/**
 * Function to get merchant formatted data.
 *
 * @param array $merchant
 *   Merchant Array.
 *
 * @return html
 *   Formatted html for merchant.
 */
function format_merchant($merchant, $selected) {
  $no_products = ($merchant['product_count'] < 1) ? 'no_products hidden' : '';
  $button = '<span class="merchant_hint_remove">';
  $button .= t('Click to remove');
  $button .= "</span>";
  $button .= '<span class="merchant_hint_add">';
  $button .= t('Click to add');
  $button .= "</span>";
  return '
		<div class="merchant ' . $no_products . '" id="merchant_id_' . $merchant['_id'] . '">
			<div class="merchant_hint">
				' . $button . '
			</div>
			<div class="merchant_name">
				' . $merchant['name'] . '
			</div>
			<div class="merchant_info">
				' . num_products_in_network($merchant) . '
			</div>
		</div>
	';
}

/**
 * Function to get merchant list.
 *
 * @param string $network_id
 *   Network ID.
 * @param array $dfrapi_merchants
 *   Merchants array.
 */
function list_merchants($network_id, $dfrapi_merchants) {
  // Getting API settings data.
  $dfrapi_configuration = variable_get('dfrapi_configuration', array());

  // Get all merchants.
  $merchants = array();
  if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
    $merchants_list = datafeedr_api_sample_network_merchant('merchant');
    if (!empty($merchants_list)) {
      foreach ($merchants_list as $merchant_list) {
        if (!empty($merchant_list)) {
          foreach ($merchant_list as $k => $v) {
            if ($k == 'source_id' && $v == $network_id) {
              $merchants[] = $merchant_list;
            }
          }
        }
      }
    }
  }
  else {
    $merchants = dfrapi_api_get_all_merchants($network_id);
  }

  if (array_key_exists('dfrapi_api_error', $merchants)) {
    return dfrapi_html_output_api_error($merchants);
  }

  $html = '<div style="display:none;" class="merchants" id="merchants_for_nid_' . $network_id . '">'
      . '<div class="merchant_actions"><span class="filter_action">';
  $html .= t('Search');
  $html .= ': <input type="text">';
  $html .= '<a class="reset_search button" title="';
  $html .= t('Clear search');
  $html .= '">';
  $html .= t('&times');
  $html .= '</a>';
  $html .= '</span><span class="sep">|</span><span class="hide_action">'
      . '<a style="display:none" class="hide_empty_merchants button">';
  $html .= t('Hide Empty Merchants');
  $html .= '</a><a class="show_empty_merchants button">';
  $html .= t('Show Empty Merchants');
  $html .= '</a></span></div>';

  $left = $right = '';
  foreach ($merchants as $merchant) {
    $selected = FALSE;
    if (isset($dfrapi_merchants['ids'])) {
      $selected = in_array($merchant['_id'], $dfrapi_merchants['ids']);
    }
    $row = format_merchant($merchant, $selected);

    if ($selected) {
      $right .= $row;
    }
    else {
      $left .= $row;
    }
  }

  $html .= '
		<div class="dfrapi_panes">
			<div class="dfrapi_pane_left">
				<div class="dfrapi_pane_title">
					<span>';
  $html .= t('Available Merchants');
  $html .= '</span>
					<a class="add_all button">';
  $html .= t('Add All');
  $html .= '</a>
				</span>

				</div>
				<div class="dfrapi_pane_content">
					' . $left . '
				</div>
			</div>
			<div class="dfrapi_pane_right">
				<div class="dfrapi_pane_title">
					<span>';
  $html .= t('Selected Merchants');
  $html .= '</span>
					<a class="remove_all button">';
  $html .= t('Remove All');
  $html .= '</a>
				</div>
				<div class="dfrapi_pane_content">
					' . $right . '
				</div>
			</div>
		</div>
	';

  $html .= "</div>";

  return $html;
}

/**
 * Function to get network details.
 *
 * @param array $all_networks
 *   Array containing all networks.
 * @param string $network_id
 *   Network ID.
 *
 * @return arraynetwork
 *   Network array.
 */
function get_network_info($all_networks, $network_id) {
  foreach ($all_networks as $network) {
    if ($network['_id'] == $network_id) {
      return $network;
    }
  }
}

/**
 * Get network details as per configured by user.
 *
 * @return arraynewtwork
 *   Network array.
 */
function set_users_networks() {
  $network_ids = array();
  $networks = variable_get('dfrapi_networks', array());
  if (!empty($networks) && isset($networks['ids'])) {
    foreach ($networks['ids'] as $network) {
      if (isset($network['nid']) && !empty($network['nid'])) {
        $network_ids[] = $network['nid'];
      }
    }
  }
  return $network_ids;
}

/**
 * Function to get selected network.
 *
 * @param string $network_id
 *   Network ID.
 * @param array $dfrapi_merchants
 *   API merchants.
 *
 * @return string
 *   Selected newtworks and merchants.
 */
function num_networks_checked_in_network($network_id, $dfrapi_merchants) {
  // Getting API settings data.
  $dfrapi_configuration = variable_get('dfrapi_configuration', array());

  // Get all merchants.
  if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
    $merchants_list = datafeedr_api_sample_network_merchant('merchant');
    $merchants = $merchants_list;
  }
  else {
    $merchants = dfrapi_api_get_all_merchants($network_id);
  }

  $count = 0;
  foreach ($merchants as $merchant) {
    if ($merchant['source_id'] == $network_id) {
      if (isset($dfrapi_merchants['ids'])) {
        if (in_array($merchant['_id'], $dfrapi_merchants['ids'])) {
          $count++;
        }
      }
    }
  }

  if ($count > 0) {
    return '<span class="num_checked_some">' . sprintf(format_plural($count, '%s merchant selected', '%s merchants selected'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
  else {
    return '<span class="num_checked_none">' . sprintf(format_plural($count, '%s merchant selected', '%s merchants selected'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
}

/**
 * Function to get available merchants in newtwork.
 *
 * @param array $network
 *   Network details.
 */
function num_merchants_in_network($network) {
  $count = ($network['merchant_count'] > 0) ? $network['merchant_count'] : 0;
  if ($count > 0) {
    return '<span class="num_merchants">' . sprintf(format_plural($count, '%s merchant', '%s merchants'), number_format($count)) . '</span> <span class="sep">/</span> ';
  }
}

/**
 * Function to get available products in newtwork.
 *
 * @param array $network
 *   Network details.
 */
function num_products_in_network($network) {
  $count = ($network['product_count'] > 0) ? $network['product_count'] : 0;
  return '<span class="num_products">' . sprintf(format_plural($count, '%s product', '%s products'), number_format($count)) . '</span>';
}

/**
 * Submit function.
 */
function datafeedr_merchants_form_submit($form, &$form_state) {
  $dfrapi_merchants['ids'] = ($_REQUEST['dfrapi_merchants']['ids']) ? explode(',', $_REQUEST['dfrapi_merchants']['ids']) : array();
  variable_set('dfrapi_merchants', $dfrapi_merchants);
  drupal_set_message(t('Merchant data updated successfully.'));
}
