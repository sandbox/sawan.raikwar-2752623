<?php

/**
 * @file
 * This file is used to display the import network and merchant list.
 */

/**
 * Form to import merchant as well as network data from another sources.
 */
function datafeedr_import_form($form, &$form_state) {
  $markup = '<b>';
  $markup .= t('Import Network Data');
  $markup .= '</b>';
  $markup .= '<br />';
  $markup .= t('Import data from another website when you want to use the same selection of networks as you have already configure on another website. You can import network data at the same time.');
  $markup .= '<br /><p style="color:red;"><b>';
  $markup .= t('Warning');
  $markup .= '</b>: ';
  $markup .= t('Importing will overwrite all of your current network settings.');
  $markup .= '</p>';
  $form['import_networkdata'] = array(
    '#markup' => $markup,
  );
  $form['network_importdata'] = array(
    '#type' => 'textarea',
    '#title' => t('Network to Import'),
    '#description' => t('Click within the box to select all your networks and affiliate IDs.'),
  );
  $import_merchantdata_markup = '<div><b>';
  $import_merchantdata_markup .= t('Import Merchant Data');
  $import_merchantdata_markup .= '</b><br>';
  $import_merchantdata_markup .= t('Import data from another website when you want to use the same selection of merchants as you have already configure on another website. You can import merchant data at the same time.');
  $import_merchantdata_markup .= '<br><p style="color:red;"><b>';
  $import_merchantdata_markup .= t('Warning');
  $import_merchantdata_markup .= '</b>: ';
  $import_merchantdata_markup .= t('Importing will overwrite all of your current merchant settings.');
  $import_merchantdata_markup .= '</p></div>';
  $form['import_merchantdata'] = array(
    '#markup' => $import_merchantdata_markup,
  );
  $form['merchant_importdata'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant to Import'),
    '#description' => t('Click within the box to select all your merchants and affiliate IDs.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Data'),
  );

  return $form;
}

/**
 * Submit callback.
 */
function datafeedr_import_form_submit($form, &$form_state) {

  $networkdata = $form_state['values']['network_importdata'];
  $merchantdata = $form_state['values']['merchant_importdata'];

  if (!empty($networkdata) && !empty($merchantdata)) {
    if ((is_serialized($networkdata) == 1) && (is_serialized($merchantdata) == 1)) {
      network_import($networkdata);
      merchant_import($merchantdata);
    }
    else {
      drupal_set_message(t('Please enter valid data.'), 'error');
    }

    if (!empty($networkdata)) {
      if (is_serialized($networkdata) == 1) {
        network_import($networkdata);
      }
      else {
        drupal_set_message(t('Please enter valid data.'), 'error');
      }
    }

    if (!empty($merchantdata)) {
      if (is_serialized($merchantdata) == 1) {
        merchant_import($merchantdata);
      }
      else {
        drupal_set_message(t('Please enter valid data.'), 'error');
      }
    }
  }
  else {
    drupal_set_message(t('Please enter network/merchant data.'), 'error');
  }
}

/**
 * Import network data.
 */
function network_import($network_data) {
  $unserialize_networkdata = (array) unserialize($network_data);
  variable_set('dfrapi_networks', $unserialize_networkdata);
  drupal_set_message(t('Network data imported successfully.'));
}

/**
 * Import merchant data.
 */
function merchant_import($merchant_data) {
  $unserialize_merchantdata = (array) unserialize($merchant_data);
  variable_set('dfrapi_merchants', $unserialize_merchantdata);
  drupal_set_message(t('Merchant data imported successfully.'));
}

/**
 * Function to checking the user entered data is serialize data or not.
 *
 * @param string $value
 *   String which is entered by Admin.
 */
function is_serialized($value, &$result = NULL) {
  // Bit of a give away this one.
  if (!is_string($value)) {
    return FALSE;
  }
  // Serialized false, return true. unserialize() returns false on an
  // invalid string or it could return false if the string is serialized
  // false, eliminate that possibility.
  if ($value === 'b:0;') {
    $result = FALSE;
    return TRUE;
  }
  $length = strlen($value);
  $end = '';
  switch ($value[0]) {
    case 's':
      if ($value[$length - 2] !== '"') {
        return FALSE;
      }
    case 'b':
    case 'i':
    case 'd':
      // This looks odd but it is quicker than isset()ing
      $end .= ';';
    case 'a':
    case 'O':
      $end .= '}';
      if ($value[1] !== ':') {
        return FALSE;
      }
      switch ($value[2]) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:

          break;

        default:
          return FALSE;
      }
    case 'N':
      $end .= ';';
      if ($value[$length - 1] !== $end[0]) {
        return FALSE;
      }
      break;

    default:
      return FALSE;
  }
  if (($result = @unserialize($value)) === FALSE) {
    $result = NULL;
    return FALSE;
  }
  return TRUE;
}
