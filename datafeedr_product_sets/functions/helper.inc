<?php

/**
 * @file
 * This file contains all the helper functions.
 */

/**
 * Gets the next update time for a PS.
 */
function dfrps_get_next_update_time() {

  $dfrps_configuration = variable_get('dfrps_configuration', array());
  if ($dfrps_configuration['update_interval'] == -1) {
    $time = time();
  }
  else {
    $time = (($dfrps_configuration['update_interval'] * (24 * 60 * 60)) + time());
  }
  return $time;
}

/**
 * Generate the product set pagination.
 */
function dfrps_pagination($data, $context) {

  // Initialize $html variable.
  $html = '';

  // Return nothing if there are no products.
  if (empty($data['products'])) {
    return $html;
  }

  // Begin pagination class.
  $html .= '<div class="dfrps_pagination">';

  $current_page = $data['page'];
  $limit = $data['limit'];
  $offset = $data['offset'];
  $found_count = $data['found_count'];
  $query = (isset($data['query'])) ? $data['query'] : array();
  $hard_limit = dfrapi_get_query_param($query, 'limit');

  // Limit Found Count to hard limit if hard limit exists.
  if ($hard_limit) {
    if ($found_count > $hard_limit['value']) {
      $found_count = $hard_limit['value'];
    }
  }

  // Maximum number of products.
  $max_num_products = (($offset + count($data['products'])) > $found_count) ? $found_count : ($offset + count($data['products']));

  // Set total possible page.
  $total_possible_pages = ceil($found_count / $limit);

  // Maximum number of pages.
  $max_total = 10000;
  $max_possible_pages = ceil($max_total / $limit);

  // Set total pages (if more pages that max_total value allows, adjust total).
  $total_pages = ($max_possible_pages < $total_possible_pages) ? $max_possible_pages : $total_possible_pages;

  // Number of relevant products.
  $relevant_results = ($found_count > 10000) ? 10000 : $found_count;

  // Showing 1 - 100 of 10,000 total relevant products found.
  $html .= '<div class="dfrps_pager_info">';
  $html .= t('Showing');
  $html .= ' <span class="dfrps_pager_start">';
  $html .= number_format((1 + $offset));
  $html .= '</span>';
  $html .= ' - ';
  $html .= '<span class="dfrps_pager_end">';
  $html .= number_format($max_num_products);
  $html .= '</span> ';
  $html .= t('of');
  $html .= ' <span class="dfrps_relevant_results">';
  $html .= number_format($relevant_results);
  $html .= '</span> ';
  $html .= t('total products.');
  $html .= ' <span style="float:right"><a class="dfrps_delete_saved_search" href="#">' . t('Delete Saved Search') . '</a></span>';
  $html .= '</div>';

  // Return nothing if there are less than 2 pages.
  if ($total_pages < 2) {
    $html .= '<div class="clearfix"></div>';
    // dfrps_pagination.
    $html .= '</div>';
    return $html;
  }

  // There is more than 1 page. Start pager classes.
  $html .= '<div class="dfrps_pager_label_wrapper">';
  $html .= '<div class="dfrps_pager_label">' . t('Page') . '</div>';
  // Dfrps_pager_label_wrapper.
  $html .= '</div>';
  $html .= '<div class="dfrps_pager_links">';
  for ($i = 1; $i <= $total_pages; $i++) {
    if ($i == $current_page) {
      $html .= '<span><strong>' . $i . '</strong></span>';
    }
    else {
      $html .= '<span> <a href="#" class="dfrps_pager" page="' . $i . '" context="' . $context . '">' . $i . '</a> </span>';
    }
  }
  // Dfrps_pager_links.
  $html .= '<div class="clearfix"></div></div>';
  // Dfrps_pagination.
  $html .= '</div>';
  return $html;
}

/**
 * Generate the list of product.
 */
function dfrps_format_product_list($data, $context, $new_arg = array()) {

  $html = '';
  $msg = '';

  // Get manually included product IDs. & Get manually blocked product IDs.
  $manually_included_ids = isset($new_arg['manually_added']) ? $new_arg['manually_added'] : array();
  $manually_blocked_ids = isset($new_arg['manually_blocked']) ? $new_arg['manually_blocked'] : array();

  // Get pagination.
  $pagination = dfrps_pagination($data, $context);

  // Message on "Search" tab.
  if (empty($data)) {

    if ($context == 'div_dfrps_tab_search') {
      $msg .= '<div class="messages status"><h2 class="element-invisible">Error message</h2>' . t('Click the [Search] button to view products that match your search.') . '</div>';
    }
  }
  elseif (empty($data['products'])) {

    if ($context == 'div_dfrps_tab_search') {
      $msg .= '<div class="messages status"><h2 class="element-invisible">Error message</h2>' . t('No products matched your search.') . '</div>';
    }
  }

  if (empty($data) || empty($data['products'])) {

    if ($context == 'div_dfrps_tab_saved_search') {
      $msg .= '<div class="messages status"><h2 class="element-invisible">Error message</h2>' . t('You have not saved a search.') . '</div>';
    }
    elseif ($context == 'div_dfrps_tab_included') {
      $msg .= '<div class="messages status"><h2 class="element-invisible">Error message</h2>' . t('You have not added any individual products to this Product Set.') . '</div>';
    }
    elseif ($context == 'div_dfrps_tab_blocked') {
      $msg .= '<div class="messages status"><h2 class="element-invisible">Error message</h2>' . t('You have not blocked any products from this Product Set.') . '</div>';
    }
  }
  else {

    $args = array(
      'manually_included_ids' => $manually_included_ids,
      'manually_blocked_ids' => $manually_blocked_ids,
      'context' => $context,
    );

    if ($context == 'div_dfrps_tab_search') {
      $msg .= '';
    }
    elseif ($context == 'div_dfrps_tab_saved_search') {
      $msg .= '';
    }
    elseif ($context == 'div_dfrps_tab_included') {
      $msg .= '';
    }
    elseif ($context == 'div_dfrps_tab_blocked') {
      $msg .= '';
    }
  }

  // Loop through products and display them.
  $result['msg'] = $msg;

  // Query info.
  if (isset($data['params']) && !empty($data['params'])) {
    $html .= '<div class="dfrps_api_info" id="dfrps_raw_api_query">
			<div class="dfrps_head">' . t('API Request') . '</div>
			<div class="dfrps_query"><span>' . dfrapi_display_api_request($data['params']) . '</span></div>
		</div>';
  }

  $html .= $pagination;
  $html .= '<div class="product_list">';
  $dfrps_html_product_list = '';
  if (isset($data['products']) && !empty($data['products'])) {
    foreach ($data['products'] as $product) {
      $dfrps_html_product_list .= dfrps_html_product_list($product, $args);
    }
  }
  $html .= $dfrps_html_product_list;
  $html .= '</div>';
  $html .= $pagination;

  $result['html'] = $html;
  return $result;
}

/**
 * Row information.
 */
function dfrps_more_info_rows($product) {

  $html = '';
  $dfr_fields = array(
    '_id',
    'onsale',
    'merchant_id',
    'time_updated',
    'time_created',
    'source_id',
    'feed_id',
    'ref_url',
  );

  if (is_array($product)) {
    ksort($product);
    $f = 1;
    foreach ($product as $k => $v) {
      $class1 = ($f % 2) ? 'even' : 'odd';
      $class2 = (in_array($k, $dfr_fields)) ? ' dfrps_data' : '';
      $html .= '<tr class="' . $class1 . $class2 . '">';
      $html .= '<td class="count">' . $f . '</td>';
      $html .= '<td class="field">' . str_replace(array("<", ">"), array("&lt;", "&gt;"), $k) . '</td>';
      if ($k == 'image' || $k == 'thumbnail') {
        $html .= '
        <td class="value dfrps_force_wrap">
          <a href="' . $v . '" target="_blank" title="Open image in new window.">' . htmlspecialchars($v) . '</a>
          <br />
          <img src="' . $v . '" />
        </td>';
      }
      elseif ($k == '_wc_url') {
        $html .= '
        <td class="value dfrps_force_wrap">
          <a href="' . $v . '" target="_blank" title="Search for product in store.">' . htmlspecialchars($v) . '</a>
        </td>';
      }
      elseif ($k == 'url') {
        $html .= '
        <td class="value dfrps_force_wrap">
          <a href="' . dfrapi_url($product) . '" target="_blank" title="Open affiliate link in new window.">' . htmlspecialchars(dfrapi_url($product)) . '</a>
        </td>';
      }
      elseif ($k == 'ref_url') {
        $html .= '
        <td class="value dfrps_force_wrap">
          <a href="' . dfrapi_url($product) . '" target="_blank" title="Open affiliate link in new window.">' . htmlspecialchars(dfrapi_url($product)) . '</a>
        </td>';
      }
      else {
        $html .= '<td class="value dfrps_force_wrap">' . htmlspecialchars($v) . '</td>';
      }
      $html .= '</tr>';
      $f++;
    }
  }
  return $html;
}

/**
 * This estimates the percentage of completion of a product set.
 */
function dfrps_percent_complete($set_id) {

  $phase = get_product_set_data($set_id, 'dfrps_update_phase');
  $update_phase = intval($phase['dfrps_update_phase']);

  $update_info = get_product_set_data($set_id, 'dfrps_previous_update_info');
  $last_update = unserialize($update_info['dfrps_previous_update_info']);

  if ($update_phase < 1) {
    return FALSE;
  }

  if ($last_update['dfrps_last_update_time_completed'] == 0) {
    // There is no last update info. Return percentage based on update phase.
    $percent = round(($update_phase / 5) * 100);
    return $percent;
  }
  $dfrps_update_iteration = get_product_set_data($set_id, 'dfrps_update_iteration');
  $current_iteration = intval($dfrps_update_iteration['dfrps_update_iteration']);
  $total_iterations = intval($last_update['dfrps_update_iteration']);

  if ($total_iterations > 0) {
    if ($current_iteration <= $total_iterations) {
      $percent = round(($current_iteration / $total_iterations) * 100);
      return $percent;
    }
    else {
      return 101;
    }
  }

  return FALSE;
}

/**
 * Display the process bar.
 */
function dfrps_progress_bar($percent) {

  if (!$percent) {
    return '';
  }

  if ($percent <= 100) {

    return '
		<div id="dfrps_dynamic_progress_bar">
			<div><small>' . $percent . '% ' . t('complete') . '</small></div>
			<div class="dfrps_progress">
				<div class="dfrps_progress-bar dfrps_progress-bar-success" role="progressbar" aria-valuenow="' . $percent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $percent . '%">
					<span class="dfrps_sr-only">' . $percent . '% ' . t('complete') . '</span>
				</div>
			</div>
		</div>
		';
  }
  else {

    return '
		<div id="dfrps_dynamic_progress_bar">
			<div><small><em>' . t('Unknown % complete') . '</em></small></div>
		</div>
		';
  }
}

/**
 * Adds a Product ID to an existing or new field value.
 */
function dfrps_helper_add_id_to_field($product_id, $ps_id, $field_key) {

  try {
    // Get all Product IDs already stored for this $field_key.
    $product_ids = array();
    $result = db_select('datafeedr_product_sets', 'dfrps')
      ->fields('dfrps', array('dfrps_id', $field_key))
      ->condition('dfrps.dfrps_id', $ps_id, '=')
      ->execute()
      ->fetchAssoc();

    if (!empty($result[$field_key])) {
      $product_ids = unserialize($result[$field_key]);
    }

    // Add new $product_id to array of Product IDs.
    if (!empty($product_ids)) {
      array_unshift($product_ids, $product_id);
    }
    else {
      $product_ids = array($product_id);
    }

    // Remove any empty array values.
    $product_ids = array_filter($product_ids);

    // Update product set.
    db_update('datafeedr_product_sets')
      ->fields(array($field_key => serialize($product_ids)))
      ->condition('dfrps_id', $result['dfrps_id'], '=')
      ->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
}

/**
 * Removes a Product ID from an existing field value.
 */
function dfrps_helper_remove_id_from_field($product_id, $ps_id, $field_key) {

  try {
    // Get all Product IDs already stored for this $field_key.
    $product_ids = array();
    $result = db_select('datafeedr_product_sets', 'dfrps')
      ->fields('dfrps', array('dfrps_id', $field_key))
      ->condition('dfrps.dfrps_id', $ps_id, '=')
      ->execute()
      ->fetchAssoc();

    if (!empty($result[$field_key])) {
      $product_ids = unserialize($result[$field_key]);
    }
    else {
      return TRUE;
    }

    // Remove Product ID from $product_ids array.
    $product_ids = array_diff($product_ids, array($product_id));

    // Remove any empty array values.
    $product_ids = array_filter($product_ids);

    // Update product set.
    db_update('datafeedr_product_sets')
      ->fields(array($field_key => serialize($product_ids)))
      ->condition('dfrps_id', $result['dfrps_id'], '=')
      ->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
}

/**
 * This returns the text "Saving..." to JS.
 */
function dfrps_helper_js_text($str) {
  if ($str == 'saving') {
    return t("Saving...");
  }
  elseif ($str == 'searching') {
    return t("Searching...");
  }
  elseif ($str == 'search') {
    return t("Search");
  }
  elseif ($str == 'deleting') {
    return t("Deleting...");
  }
}

/**
 * Display include product.
 */
function dfrps_helper_include_product($pid, $args) {

  // Product has already been included?
  if (in_array($pid, $args['manually_included_ids'])) {

    // What's the context of this page?
    if ($args['context'] == 'div_dfrps_tab_search') {
      // Search page, display "checkmark" icon.
      dfrps_html_included_product_icon();
    }
    elseif ($args['context'] == 'div_dfrps_tab_included') {
      // Included page, display "minus" icon/link.
      dfrps_html_remove_included_product_link($pid);
    }

    // Product has NOT already been included?
  }
  else {
    if ($args['context'] != 'blocked' && $args['context'] != 'saved_search') {
      // Not already included and we're not in the "blocked" context, display,
      // Add icon/link.
      dfrps_html_include_product_link($pid);
    }
  }
}

/**
 * Display blocked or unbloked product.
 */
function dfrps_helper_block_product($pid, $args) {

  // Product has already been blocked?
  if (in_array($pid, $args['manually_blocked_ids'])) {

    // What's the context of this page?
    if ($args['context'] == 'div_dfrps_tab_blocked') {
      // Product is blocked, display "unblock" icon/link.
      dfrps_html_unblock_product_link($pid);
    }

    // Product has NOT already been blocked?
  }
  else {
    if ($args['context'] != 'div_dfrps_tab_included') {
      // Not already blocked, display "block" icon/link.
      dfrps_html_block_product_link($pid);
    }
  }
}

/**
 * Display date.
 */
function dfrps_date_in_two_rows($date) {
  if (is_numeric($date)) {
    $html = '<div>' . date('M j', $date) . ' ' . date('g:ia', $date) . '</div>';
  }
  else {
    $html = '<div>' . date('M j', strtotime($date)) . ' ' . date('g:ia', strtotime($date)) . '</div>';
  }
  return $html;
}
