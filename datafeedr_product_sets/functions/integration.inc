<?php

/**
 * @file
 * This file is used to integration of importer functionality.
 */

/**
 * This gets the category IDs for the current post.
 *
 * (ie. the product that was already imported into the database) and removes
 * the IDs from the post.
 *
 * Why?
 *
 * We need to delete this set's current category IDs from this product so that
 * at the end of the update, if this product isn't re-imported
 * during the update, the post/product's category information (for this set)
 * will no longer be available so that if this post/product was
 * added via another Product Set, only that Product Set's category IDs
 * will be attributed to this post/product.
 */
function dfrps_remove_category_ids_from_post($post_id, $set, $cpt, $taxonomy) {

  // Get the category IDs associated with this Set.
  $this_sets_categories = unserialize($set['dfrps_categories_history'][0]);
  $this_sets_categories = $this_sets_categories['product'];

  if (is_array($this_sets_categories) && !empty($this_sets_categories)) {
    $terms = array();
    foreach ($this_sets_categories as $term) {
      $terms[] = intval($term);
    }

    $node = node_load($post_id);
    $category = array();
    foreach ($node->taxonomy as $cat) {
      if (!in_array($cat['tid'], $terms)) {
        $category[] = $cat;
      }
    }
    $node->taxonomy = $category;
    node_save($node);
  }
}

/**
 * Get all the product id by the product set.
 */
function dfrps_get_all_post_ids_by_set_id($set_id) {

  $set_id = intval($set_id);

  if ($set_id < 1) {
    return array();
  }

  $node_products = db_select('node', 'n');
  $node_products->leftJoin('field_data_dfrps_product_set_id', 'ps', 'ps.entity_id = n.nid');
  $node_products->fields('n', array('nid'))
    ->condition('ps.dfrps_product_set_id_value', $set_id, '=')
    ->execute()
    ->fetchAll();

  if ($node_products == NULL) {
    return array();
  }

  $ids = array();
  foreach ($node_products as $node_product) {
    $ids[] = $node_product['nid'];
  }

  return $ids;
}

/**
 * Returns post ARRAY if post already exists.
 *
 * Returns FALSE if post does not exist.
 */
function dfrps_get_existing_post($product, $set) {

  $import_into = get_product_set_data($set['dfrps_id'], 'dfrps_import_into');
  if (is_array($import_into['dfrps_import_into']) && count($import_into['dfrps_import_into']) > 0) {
    $import_into = implode("','", $import_into['dfrps_import_into']);
  }
  $import_into = 'product';

  $post = db_select('node', 'n');
  $post->leftJoin('field_data_dfrps_product_id', 'ps', 'ps.entity_id = n.nid');
  $post->fields('n');
  $post->condition('ps.dfrps_product_id_value', $product['_id'], '=');
  $post->condition('n.type', $import_into, '=');
  $result = $post->execute();
  $product = $result->fetchAssoc();

  if ($product != NULL) {
    return $product;
  }

  return FALSE;
}

/**
 * Convert int to price.
 */
function dfrps_int_to_price($price) {
  $price = intval($price);
  return ($price / 100);
}
