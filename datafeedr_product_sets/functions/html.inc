<?php

/**
 * @file
 * This file generates the html for single product.
 */

/**
 * This is responsible for displaying a list of products in the admin section.
 *
 * The list of products could be generated from a search query, a list of
 * manually included products or a list of blocked products.
 */
if (!function_exists('dfrps_html_product_list')) {

  /**
   * Function to show the product list.
   */
  function dfrps_html_product_list($product, $args = array()) {

    global $base_path;
    // Image.
    if (@$product['image'] != '') {
      $image = @$product['image'];
    }
    elseif (@$product['thumbnail'] != '') {
      $image = @$product['thumbnail'];
    }
    else {
      $image = $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/no-image.jpg';
    }

    // Currency.
    $currency = isset($product['currency']) ? $product['currency'] : '';

    // Product type.
    $coupon_networks = variable_get('dfrapi_coupon_networks');
    $type = 'product';
    if (!empty($coupon_networks)) {
      $type = (array_key_exists($product['source_id'], $coupon_networks)) ? 'coupon' : 'product';
    }

    // Has this product already been included?
    $already_included = (in_array($product['_id'], $args['manually_included_ids'])) ? TRUE : FALSE;
    $product_list = '';

    $product_list .= '<div id="product_' . $product['_id'] . '_' . $args['context'] . '" class="product_block product_' . $product['_id'] . '">
			<table class="dfrps_product_table type_' . $type . '">
				<tr class="product">
					<td class="image" rowspan="2">
						<a href=" ' . $image . '" title="View image in new browser window." target="_blank">
							<img src="' . $image . '" />
						</a>
					</td>
					<td class="name">
						<div>
							<a href="#" class="more_info" title="View more information about this product.">
								' . $product['name'] . '
							</a>
						</div>
					</td>
					<td class="links">
						<div class="action_links">';

    if ($already_included) :
      $product_list .= '<div class="dfrps_product_already_included" title="This product has already been manually added to this Product Set.">
									<img src="' . $base_path . drupal_get_path("module", "datafeedr_product_sets") . '/images/icons/checkmark.png" />									
								</div>';
    else :
      $product_list .= '<div class="dfrps_add_individual_product">
									<a href="#" product-id="' . $product['_id'] . '" title="Add this product to this Product Set.">
										<img src="' . $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/plus.png" />
									</a>
								</div>';
    endif;

    $product_list .= '<div class="dfrps_remove_individual_product">
								<a href="#" product-id="' . $product['_id'] . '" title="Remove this product from the individually added list for this Product Set.">
									<img src=" ' . $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/minus.png" />
								</a>
							</div>
						
							<div class="dfrps_unblock_individual_product">
								<a href="#" product-id="' . $product['_id'] . '" title="Unblock this product and allow it to show up in product searches for this Product Set.">
									<img src="' . $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/unblock.png" />
								</a>
							</div>

							<div class="dfrps_block_individual_product">
								<a href="#" product-id="' . $product['_id'] . '" title="Block this product from appearing in searches for this Product Set.">
									<img src="' . $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/block.png" />
								</a>
							</div>

						</div>
					</td>
					<td class="product_type" rowspan="3">
						<img src="' . $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/' . $type . '-label.png" />
					</td>
				</tr>
				<tr>
					<td class="info" colspan="2">
				
						<div class="description">';
    if (isset($product['description'])) :
      $product_list .= strip_tags($product['description']);
    endif;
    $product_list .= '</div>
				
						<div class="details">				
							<div class="network" title="Network: ' . htmlspecialchars($product['source']) . '">
								<span class="bullet">&bull;</span>
								<span class="label">' . $product['source'] . '</span>
							</div>
							<div class="merchant" title="Merchant: ' . htmlspecialchars($product['merchant']) . '">
								<span class="bullet">&bull;</span>
								<span class="label">' . substr($product['merchant'], 0, 25) . '</span>
							</div>';
    if (isset($product['brand'])) :
      $product_list .= '<div class="brand" title="Brand: ' . htmlspecialchars($product['brand']) . '">
									<span class="bullet">&bull;</span>
									<span class="label">' . $product['brand'] . '</span>
								</div>';
    endif;
    if (isset($product['price'])) :
      $product_list .= '<div class="price" title="Price: ' . htmlspecialchars(dfrapi_currency_code_to_sign($currency) . dfrapi_int_to_price($product['price']) . ' ' . $currency) . '">
									<span class="bullet">&bull;</span>
									<span class="label">' . dfrapi_currency_code_to_sign($currency) . dfrapi_int_to_price($product['price']) . '</span>
								</div>';
    endif;
    if (isset($product['saleprice'])) :
      $product_list .= '<div class="saleprice" title="Sale Price: ' . htmlspecialchars(dfrapi_currency_code_to_sign($currency) . dfrapi_int_to_price($product['saleprice']) . ' ' . $currency) . '">
									<span class="bullet">&bull;</span>
									<span class="label">' . dfrapi_currency_code_to_sign($currency) . dfrapi_int_to_price($product['saleprice']) . '</span>
								</div>';
    endif;
    $product_list .= '</div>
			
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="more_info_row" style="display: none;">
							<table class="product_values">
								' . dfrps_more_info_rows($product) . '
							</table>
							<div class="more_info_note">* <span>Italicized rows</span> indicate field and value was generated by Datafeedr, not merchant.</div>
						</div>
					</td>
				</tr>
			</table>
		</div>';

    return $product_list;
  }

}
