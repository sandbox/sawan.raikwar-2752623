<?php

/**
 * @file
 * This file implements all cron related hooks.
 */

/**
 * The interval (in seconds) in which the system should check for product sets.
 *
 * Which need to be updated. By default: 1 minute (60 seconds).
 */
function dfrps_default_cron_interval() {
  $dfrps_configuration = variable_get('dfrps_configuration', array());
  if (!empty($dfrps_configuration)) {
    return $dfrps_configuration['cron_interval'];
  }
  else {
    return 60;
  }
}

/**
 * Query product sets and run update if needed.
 */
function dfrps_get_product_set_to_update() {

  // Check if an update is already running.
  $doing_update = variable_get('dfrps_doing_update', 0);
  if ($doing_update !== 0) {
    return;
  }

  // Check that updates are enabled.
  $dfrps_configuration = variable_get('dfrps_configuration', array());
  if ($dfrps_configuration['updates_enabled'] == 0) {
    return;
  }

  // Check that at least 1 network is selected.
  $dfrapi_networks = variable_get('dfrapi_networks', array());
  if (empty($dfrapi_networks['ids'])) {
    return TRUE;
  }

  // Check that at least 1 merchant is selected.
  $dfrapi_merchants = variable_get('dfrapi_merchants', array());
  if (empty($dfrapi_merchants['ids'])) {
    return TRUE;
  }

  $product_set = db_select('datafeedr_product_sets', 'ps')
    ->fields('ps')
    ->orderBy('dfrps_update_phase', 'DESC')
    ->orderBy('dfrps_next_update_time', 'ASC')
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();
  /*
   * First check if post_status is 'trash'. Trashed sets
   * get priority as we need to remove those products
   * from the store ASAP.
   */
  if ($product_set['dfrps_status'] == 0 && $product_set['dfrps_next_update_time'] <= time()) {
    module_load_include('inc', 'datafeedr_product_sets', '/classes/class-dfrps-delete');
    new DatafeedrProductSetsDfrpsDelete($product_set);
    return;
  }

  /*
   * If a Product Set is currently in an update phase
   * or, if a Product Set's next update time is now
   * or, if a Product Set's next update time is 0
   * then, run the update.
   */
  if (
      $product_set['dfrps_status'] == 1 &&
      (
      $product_set['dfrps_update_phase'] > 0 ||
      $product_set['dfrps_next_update_time'] <= time() ||
      $product_set['dfrps_next_update_time'] == 0
      )) {

    if (isset($product_set['dfrps_id']) && ($product_set['dfrps_id'] > 0)) {

      // Update Product Set.
      module_load_include('inc', 'datafeedr_product_sets', '/classes/class-dfrps-update');
      new DatafeedrProductSetsDfrpsUpdate($product_set);
      return;
    }
  }
}
