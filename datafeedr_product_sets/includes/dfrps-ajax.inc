<?php

/**
 * @file
 * This file handles all the Ajax calls.
 */

/**
 * Method is used to display the network and merchant modals.
 */
function datafeedr_product_sets_ajax() {
  $sform = new Dfrapi_SearchForm();
  echo $sform->ajaxHandler();
}

/**
 * This saves the categories checked by the user.
 */
function dfrps_ajax_update_taxonomy() {

  // Set $postid variable.
  $psid = (isset($_REQUEST['psid']) && ($_REQUEST['psid'] > 0)) ? $_REQUEST['psid'] : 0;

  // Set product set title variable.
  $title = isset($_REQUEST['title']) ? $_REQUEST['title'] : FALSE;
  if (!$title) {
    $response = array(
      'status' => 'error',
      'message' => 'Please provide a Product Set title.',
      'data' => array('html' => t('Please provide a Product Set title.')),
    );
    echo json_encode($response);
    die;
  }

  // Set product set query variable.
  $query_raw = (isset($_REQUEST['query']) && $_REQUEST['query'] !== '') ? $_REQUEST['query'] : FALSE;
  if ($query_raw) {
    parse_str($_REQUEST['query'], $query);
    $query = serialize($query['_dfrps_cpt_query']);
  }

  // Get cids.
  $cids = (isset($_REQUEST['cids']) && !empty($_REQUEST['cids'])) ? $_REQUEST['cids'] : array();

  // Set $cpt variable.
  $cpt = (isset($_REQUEST['cpt']) && (!$_REQUEST['cpt'] == '')) ? $_REQUEST['cpt'] : 'product';

  $categories[$cpt] = $cids;

  $product_set_id = NULL;

  if ($psid == 0) {
    try {
      $product_set_id = db_insert('datafeedr_product_sets')
        ->fields(
          array(
            'dfrps_title' => $title,
            'dfrps_query' => ($query) ? $query : '',
            'dfrps_temp_query' => ($query) ? $query : '',
            'dfrps_categories' => (!empty($categories[$cpt])) ? serialize($categories) : '',
            'dfrps_categories_history' => (!empty($categories[$cpt])) ? serialize($categories) : '',
            'dfrps_created' => time(),
            'dfrps_modified' => time(),
            'dfrps_update_phase' => 0,
            'dfrps_next_update_time' => 0,
          )
        )
        ->execute();
    }
    catch (Exception $e) {
      $response = array(
        'status' => 'error',
        'message' => 'Error in product set insert.',
        'data' => array('html' => t('Error in product set insert.')),
      );
      echo json_encode($response);
      die;
    }
  }
  else {

    /* Check the product set exist or not. */
    $result = db_select('datafeedr_product_sets', 'dfrps')
      ->fields('dfrps', array('dfrps_id', 'dfrps_categories_history'))
      ->condition('dfrps.dfrps_id', $psid, '=')
      ->execute()
      ->fetchAssoc();

    if (!empty($result['dfrps_id'])) {

      try {

        $categories_history = unserialize($result['dfrps_categories_history']);

        if (!empty($categories_history)) {
          $categories_history[$cpt] = (is_array($categories_history[$cpt])) ? array_merge($categories_history[$cpt], $cids) : $cids;
        }
        else {
          $categories_history[$cpt] = $cids;
        }

        $dfrps_categories_history[$cpt] = array_unique($categories_history[$cpt]);

        $result_upd = db_update('datafeedr_product_sets')
          ->fields(
            array(
              'dfrps_title' => $title,
              'dfrps_query' => ($query) ? $query : '',
              'dfrps_temp_query' => ($query) ? $query : '',
              'dfrps_categories' => (!empty($categories[$cpt])) ? serialize($categories) : '',
              'dfrps_categories_history' => (!empty($dfrps_categories_history[$cpt])) ? serialize($dfrps_categories_history) : '',
              'dfrps_modified' => time(),
            )
          )
          ->condition('dfrps_id', $result['dfrps_id'], '=');

        $result_upd->execute();
      }
      catch (Exception $e) {
        $response = array(
          'status' => 'error',
          'message' => 'Error in product set update.',
          'data' => array('html' => t('Error in product set update.')),
        );
        echo json_encode($response);
        die;
      }
      $product_set_id = $psid;
    }
  }

  if ($product_set_id != NULL && $product_set_id > 0) {
    $response = array(
      'status' => 'success',
      'message' => 'Product Set has been successfully saved.',
      'data' => array('id' => $product_set_id, 'html' => 'Product Set has been successfully updated.'),
    );
    echo json_encode($response);
    die;
  }
  elseif ($product_set_id == NULL) {
    $response = array(
      'status' => 'error',
      'message' => 'Invalid Product Set ID provided.',
      'data' => array('html' => t('Error in product set update.')),
    );
    echo json_encode($response);
    die;
  }
}

/**
 * This saves a query search when user clicks the save/update search button.
 */
function dfrps_ajax_save_query() {

  // Set $psid variable.
  $psid = (isset($_REQUEST['psid']) && is_numeric($_REQUEST['psid'])) ? (int) $_REQUEST['psid'] : 0;

  // Set product set title variable.
  $title = isset($_REQUEST['title']) ? $_REQUEST['title'] : FALSE;
  if (!$title) {
    $response = array(
      'status' => 'error',
      'message' => 'No Product Set title provided.  A Product Set title is required.',
      'data' => array(
        'html' => t('No Product Set title provided.  A Product Set title is required.'),
      ),
    );
    echo json_encode($response);
    die;
  }

  // Set product set query variable.
  $query = '';
  if (isset($_REQUEST['query']) && $_REQUEST['query'] !== '') {
    parse_str($_REQUEST['query'], $query);
    $query = serialize($query['_dfrps_cpt_query']);
  }

  // Process the product set ID, Title & query.
  $product_set_id = dfrapi_get_product_set_id($psid, $title, $query);

  if ($product_set_id != NULL && $product_set_id > 0) {
    $response = array(
      'status' => 'success',
      'message' => 'Search has been successfully saved.',
      'data' => array('id' => $product_set_id, 'html' => 'Update Saved Search'),
    );
    echo json_encode($response);
    die;
  }
  elseif ($product_set_id == NULL) {
    $response = array(
      'status' => 'error',
      'message' => 'Invalid Product Set title provided. Provided a valid Product Set id.',
      'data' => array(
        'html' => t('Invalid Product Set title provided. Provided a valid Product Set id.'),
      ),
    );
    echo json_encode($response);
    die;
  }
}

/**
 * This function is used to determine how we should return the products.
 */
function dfrps_ajax_get_products() {
  /*
   * Possible $_REQUEST values:
   *
   * query 	- This is the API query (multiple filters)
   * ids		- An array of product IDs.
   * psid 	- This is the post ID of the Product Set.
   * page 	- This is the page number being requested for pagination.
   * context	- This will determine how to output the list of products.
   */
  // Set $ps_id variable.
  $ps_id = (isset($_REQUEST['psid']) && is_numeric($_REQUEST['psid'])) ? (int) $_REQUEST['psid'] : 0;

  // If $ps_id doesn't validate, show error.
  $dfrps_manually_added_ids = $dfrps_manually_blocked_ids = array();
  if ($ps_id > 0) {
    $result = db_select('datafeedr_product_sets', 'dfrps')
      ->fields('dfrps',
        array(
          'dfrps_id',
          'dfrps_query',
          'dfrps_temp_query',
          'dfrps_manually_added_ids',
          'dfrps_manually_blocked_ids',
        )
      )
      ->condition('dfrps.dfrps_id', $ps_id, '=')
      ->execute()
      ->fetchAssoc();
    if (empty($result['dfrps_id'])) {
      $response = array(
        'status' => 'error',
        'flag' => 0,
        'message' => 'Product Set id is not valid.',
        'data' => array('html' => t('Product Set id is not valid.')),
      );
      echo json_encode($response);
      die;
    }
    else {
      $dfrps_id = $result['dfrps_id'];
      $dfrps_query = $result['dfrps_query'];
      $dfrps_temp_query = $result['dfrps_temp_query'];
      $dfrps_manually_added_ids = array_filter((array) unserialize($result['dfrps_manually_added_ids']));
      $dfrps_manually_blocked_ids = array_filter((array) unserialize($result['dfrps_manually_blocked_ids']));
    }
  }

  // Possible contexts.
  $possible_contexts = array(
    'div_dfrps_tab_search',
    'div_dfrps_tab_saved_search',
    'div_dfrps_tab_included',
    'div_dfrps_tab_blocked',
  );

  // Set $context variable.
  $context = (isset($_REQUEST['context']) && in_array($_REQUEST['context'], $possible_contexts)) ? $_REQUEST['context'] : FALSE;

  // If $context doesn't validate, show error.
  if (!$context) {
    $response = array(
      'status' => 'error',
      'flag' => 0,
      'message' => 'No context provided. Context is required.',
      'data' => array('html' => t('No context provided. Context is required.')),
    );
    echo json_encode($response);
    die;
  }

  // Set $page variable.
  $page = (isset($_REQUEST['page'])) ? intval($_REQUEST['page']) : 1;

  // Get "num_products_per_search" value for $limit value.
  $dfrps_configuration = variable_get('dfrps_configuration', array());
  $limit = $dfrps_configuration['num_products_per_search'];

  // Initialize $args array.
  // $args = array();
  // Set offset.
  $offset = ($page > 0) ? (($page - 1) * $limit) : 0;

  // Make sure $limit doesn't go over 10,000.
  if (($offset + $limit) > 10000) {
    $limit = (10000 - $offset);
  }

  // Default $args to pass to any function.
  // $args['limit'] = $limit;
  // $args['offset'] = $offset;
  /*
   * Based on $context, determine what to do next.
   */
  // Context is "div_dfrps_tab_search".
  if ($context == 'div_dfrps_tab_search') {

    /*
     * A search was performed. We need to save the query
     * so that it can be requested on subsequent paginated pages.
     */
    // Isolate the query.
    if (isset($_REQUEST['query']) && $_REQUEST['query'] !== '') {
      parse_str($_REQUEST['query'], $query);
    }
    else {
      $query = array();
      $query['_dfrps_cpt_query'] = array();
    }

    // If query is not empty, store it as the temp query.
    if (!empty($query['_dfrps_cpt_query'])) {

      // Query exists so save it.
      $temp_query = $query['_dfrps_cpt_query'];

      if ($ps_id > 0 && $dfrps_id > 0) {
        db_update('datafeedr_product_sets')
          ->fields(array('dfrps_temp_query' => ($temp_query) ? serialize($temp_query) : ''))
          ->condition('dfrps_id', $result['dfrps_id'], '=')
          ->execute();
      }
    }
    else {

      // No query exists so grab the last stored query.
      if ($ps_id > 0 && $dfrps_id > 0) {
        $temp_query = ($dfrps_temp_query != '') ? unserialize($dfrps_temp_query) : '';
      }
    }

    // Get manually blocked product IDs.
    $manually_blocked = (is_array($dfrps_manually_blocked_ids) && !empty($dfrps_manually_blocked_ids)) ? $dfrps_manually_blocked_ids : array();

    // Get manually added product IDs.
    $manually_added = (is_array($dfrps_manually_added_ids) && !empty($dfrps_manually_added_ids)) ? $dfrps_manually_added_ids : array();

    $arg_new = array(
      'manually_blocked' => $manually_blocked,
      'manually_added' => $manually_added,
    );

    $data = array();
    // Query API if a temp query exists.
    if ($temp_query !== '') {

      // Getting API settings data.
      $dfrapi_configuration = variable_get('dfrapi_configuration', array());

      // Get all products.
      if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
        $products_data = db_select('dfrapi_sample_products_data_01', 'spd')
          ->fields('spd', array('product_data'))
          ->range($offset, $limit)
          ->execute()
          ->fetchAll();
        foreach ($products_data as $product) {
          $product_data = (array) $product;
          $data['products'][] = @unserialize($product_data['product_data']);
        }
        $data['found_count'] = 1000;
      }
      else {
        $data = dfrapi_api_get_products_by_query($temp_query, $limit, $page, $manually_blocked);
      }
    }

    // Print any errors.
    if (is_array($data) && array_key_exists('dfrapi_api_error', $data)) {
      $response = array(
        'status' => 'error',
        'flag' => 1,
        'message' => t('Error in product sets search.'),
        'data' => array('html' => dfrapi_output_api_error($data, TRUE)),
      );
      echo json_encode($response);
      die;
    }

    // Add a few more helpful values to the $data array.
    $data['page'] = $page;
    $data['psid'] = $ps_id;
    $data['limit'] = $limit;
    $data['offset'] = $offset;
    $dfrps_format_product_list = dfrps_format_product_list($data, $context, $arg_new);

    $response = array(
      'status' => 'success',
      'flag' => 0,
      'message' => t('Product sets search has been successfully performed.'),
      'data' => array(
        'html' => $dfrps_format_product_list['html'],
        'html_msg' => $dfrps_format_product_list['msg'],
      ),
    );
    echo json_encode($response);
    die;
  }

  // Context is "div_dfrps_tab_saved_search".
  elseif ($context == 'div_dfrps_tab_saved_search') {

    // Get query.
    $saved_query = ($dfrps_query != '') ? unserialize($dfrps_query) : '';

    // Get manually blocked product IDs.
    $manually_blocked = (is_array($dfrps_manually_blocked_ids) && !empty($dfrps_manually_blocked_ids)) ? $dfrps_manually_blocked_ids : array();

    // Query API if a saved query exists.
    if (!empty($saved_query)) {

      // Getting API settings data.
      $dfrapi_configuration = variable_get('dfrapi_configuration', array());

      // Get all products.
      if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {
        $products_data = db_select('dfrapi_sample_products_data_01', 'spd')
          ->fields('spd', array('product_data'))
          ->range($offset, $limit)
          ->execute()
          ->fetchAll();
        foreach ($products_data as $product) {
          $product_data = (array) $product;
          $data['products'][] = @unserialize($product_data['product_data']);
        }
        $data['found_count'] = 1000;
      }
      else {
        $data = dfrapi_api_get_products_by_query($saved_query, $limit, $page, $manually_blocked);
      }
    }

    // Print any errors.
    if (isset($data) && is_array($data) && array_key_exists('dfrapi_api_error', $data)) {
      $response = array(
        'status' => 'error',
        'flag' => 1,
        'message' => t('Error in product set saved search.'),
        'data' => array('html' => dfrapi_output_api_error($data, TRUE)),
      );
      echo json_encode($response);
      die;
    }

    // Add a few more helpful values to the $data array.
    $data['page'] = $page;
    $data['postid'] = $ps_id;
    $data['limit'] = $limit;
    $data['offset'] = $offset;

    $dfrps_format_product_list = dfrps_format_product_list($data, $context);

    // Update number of products in this saved search.
    $response = array(
      'status' => 'success',
      'flag' => 0,
      'message' => t('Product set saved search has been successfully performed.'),
      'data' => array(
        'html' => $dfrps_format_product_list['html'],
        'html_msg' => $dfrps_format_product_list['msg'],
      ),
    );
    echo json_encode($response);
    die;
  }

  // Context is "div_dfrps_tab_included".
  elseif ($context == 'div_dfrps_tab_included') {

    $ids = array_filter((array) $dfrps_manually_added_ids);

    // Query API if IDs exists.
    if (!empty($ids)) {

      // Getting API settings data.
      $dfrapi_configuration = variable_get('dfrapi_configuration', array());

      // Get all products.
      if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {

        $products_count = db_select('dfrapi_sample_products_data_01', 'spd')
          ->fields('spd', array('id'))
          ->condition('spd.product_id', $ids, 'IN')
          ->execute();
        $num_of_results = $products_count->rowCount();

        $products_data = db_select('dfrapi_sample_products_data_01', 'spd')
          ->fields('spd', array('product_data'))
          ->condition('spd.product_id', $ids, 'IN')
          ->range($offset, $limit)
          ->execute()
          ->fetchAll();

        foreach ($products_data as $product) {
          $product_data = (array) $product;
          $data['products'][] = @unserialize($product_data['product_data']);
        }
        $data['found_count'] = $num_of_results;
      }
      else {
        $data = dfrapi_api_get_products_by_id($ids, $limit, $page);
      }
    }

    // Print any errors.
    if (isset($data) && is_array($data) && array_key_exists('dfrapi_api_error', $data)) {
      $response = array(
        'status' => 'error',
        'flag' => 1,
        'message' => t('Error in product set included product.'),
        'data' => array('html' => dfrapi_output_api_error($data, TRUE)),
      );
      echo json_encode($response);
      die;
    }

    // Add a few more helpful values to the $data array.
    $data['page'] = $page;
    $data['postid'] = $ps_id;
    $data['limit'] = $limit;
    $data['offset'] = $offset;

    $dfrps_format_product_list = dfrps_format_product_list($data, $context);

    $response = array(
      'status' => 'success',
      'flag' => 0,
      'message' => t('Product set included product has been successfully performed.'),
      'data' => array(
        'html' => $dfrps_format_product_list['html'],
        'html_msg' => $dfrps_format_product_list['msg'],
      ),
    );
    echo json_encode($response);
    die;
  }

  // Context is "div_dfrps_tab_blocked".
  elseif ($context == 'div_dfrps_tab_blocked') {

    $ids = array_filter((array) $dfrps_manually_blocked_ids);

    // Query API if IDs exists.
    if (!empty($ids)) {
      // Getting API settings data.
      $dfrapi_configuration = variable_get('dfrapi_configuration', array());

      // Get all products.
      if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {

        $products_count = db_select('dfrapi_sample_products_data_01', 'spd')
          ->fields('spd', array('id'))
          ->condition('spd.product_id', $ids, 'IN')
          ->execute();
        $num_of_results = $products_count->rowCount();

        $products_data = db_select('dfrapi_sample_products_data_01', 'spd')
          ->fields('spd', array('product_data'))
          ->condition('spd.product_id', $ids, 'IN')
          ->range($offset, $limit)
          ->execute()
          ->fetchAll();

        foreach ($products_data as $product) {
          $product_data = (array) $product;
          $data['products'][] = @unserialize($product_data['product_data']);
        }
        $data['found_count'] = $num_of_results;
      }
      else {
        $data = dfrapi_api_get_products_by_id($ids, $limit, $page);
      }
    }

    // Print any errors.
    if (isset($data) && is_array($data) && array_key_exists('dfrapi_api_error', $data)) {
      $response = array(
        'status' => 'error',
        'flag' => 1,
        'message' => t('Error in product set blocked product.'),
        'data' => array('html' => dfrapi_output_api_error($data, TRUE)),
      );
      echo json_encode($response);
      die;
    }

    // Add a few more helpful values to the $data array.
    $data['page'] = $page;
    $data['postid'] = $ps_id;
    $data['limit'] = $limit;
    $data['offset'] = $offset;

    $dfrps_format_product_list = dfrps_format_product_list($data, $context);

    $response = array(
      'status' => 'success',
      'flag' => 0,
      'message' => t('Product set blocked product has been successfully performed.'),
      'data' => array(
        'html' => $dfrps_format_product_list['html'],
        'html_msg' => $dfrps_format_product_list['msg'],
      ),
    );
    echo json_encode($response);
    die;
  }

  echo 'Uh-oh. Something went wrong.';
  die;
}

/**
 * Handle ajax callback for all delete saved search.
 */
function dfrps_ajax_delete_saved_search() {

  // Set $psid variable.
  $psid = (isset($_REQUEST['psid']) && ($_REQUEST['psid'] > 0)) ? $_REQUEST['psid'] : FALSE;

  // If $psid doesn't validate, show error.
  if (!$psid) {
    $response = array(
      'status' => 'error',
      'message' => 'No Product Set ID provided.  A Product Set ID is required.',
      'data' => array(
        'html' => t('No Product Set ID provided.  A Product Set ID is required.'),
      ),
    );
    echo json_encode($response);
    die;
  }

  try {
    db_update('datafeedr_product_sets')
      ->fields(array('dfrps_query' => ''))
      ->condition('dfrps_id', $psid, '=')
      ->execute();

    $response = array(
      'status' => 'success',
      'message' => 'Saved products search has been successfully deleted.',
      'data' => array('html' => t('Saved products search has been successfully deleted.')),
    );
    echo json_encode($response);
    die;
  }
  catch (Exception $e) {
    $response = array(
      'status' => 'error',
      'message' => 'Error in saved products search deleted.',
      'data' => array(
        'html' => t('Error in saved products search deleted.'),
      ),
    );
    echo json_encode($response);
    die;
  }
}

/**
 * Handle ajax callback for all action links product list.
 */
function dfrps_ajax_action_links() {

  switch ($_REQUEST['action']) {
    case 'dfrps_ajax_add_individual_product':
      dfrps_ajax_add_individual_product();
      break;

    case 'dfrps_ajax_block_individual_product':
      dfrps_ajax_block_individual_product();
      break;

    case 'dfrps_ajax_remove_individual_product':
      dfrps_ajax_remove_individual_product();
      break;

    case 'dfrps_ajax_unblock_individual_product':
      dfrps_ajax_unblock_individual_product();
      break;
  }
}

/**
 * Add individual product to Product Set.
 */
function dfrps_ajax_add_individual_product() {
  global $base_path;
  $result = dfrps_helper_add_id_to_field($_REQUEST['pid'], $_REQUEST['psid'], 'dfrps_manually_added_ids');

  $response = array(
    'status' => 'error',
    'message' => 'Product Set has not updated.',
    'data' => array(
      'html' => t('Product Set has not updated.'),
    ),
  );
  if ($_REQUEST['psid'] == 0) {
    $response = array(
      'status' => 'error',
      'message' => 'Please save the Product Set before adding product to it.',
      'data' => array(
        'html' => t('Please save the Product Set before adding product to it.'),
      ),
    );
  }
  elseif ($result !== FALSE) {
    $image_path = $base_path . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/checkmark.png';
    $response = array(
      'status' => 'success',
      'message' => 'Product Set has been updated.',
      'data' => array(
        'html' => '<div class="dfrps_product_already_included" title="Product successfully added to this Product Set."><img src="' . $image_path . '"></div>',
      ),
    );
  }
  echo json_encode($response);
  die;
}

/**
 * Add product ID to blocked_ids array.
 *
 * Remove Product ID from manually included.
 */
function dfrps_ajax_block_individual_product() {

  $result1 = dfrps_helper_add_id_to_field($_REQUEST['pid'], $_REQUEST['psid'], 'dfrps_manually_blocked_ids');
  $result2 = dfrps_helper_remove_id_from_field($_REQUEST['pid'], $_REQUEST['psid'], 'dfrps_manually_added_ids');

  $response = array(
    'status' => 'error',
    'message' => 'Product Set has not updated.',
    'data' => array('html' => t('Product Set has not updated.')),
  );
  if ($_REQUEST['psid'] == 0) {
    $response = array(
      'status' => 'error',
      'message' => 'Please save the Product Set before block any product for it.',
      'data' => array('html' => t('Please save the Product Set before block any product for it.')),
    );
  }
  elseif ($result1 !== FALSE && $result2 !== FALSE) {
    $response = array(
      'status' => 'success',
      'message' => 'Product Set has been updated.',
      'data' => array('html' => t('Product Set has been updated.')),
    );
  }
  echo json_encode($response);
  die;
}

/**
 * Remove individual product that was added manually from Product Set.
 */
function dfrps_ajax_remove_individual_product() {

  $result = dfrps_helper_remove_id_from_field($_REQUEST['pid'], $_REQUEST['psid'], 'dfrps_manually_added_ids');
  if ($result !== FALSE) {
    $response = array('status' => 'success', 'message' => 'Product Set has been updated.');
  }
  else {
    $response = array(
      'status' => 'error',
      'message' => 'Product Set has not updated.',
      'data' => array('html' => t('Product Set has not updated.')),
    );
  }
  echo json_encode($response);
  die;
}

/**
 * Unblock individual product that already blocked from the Product Set.
 */
function dfrps_ajax_unblock_individual_product() {

  $result = dfrps_helper_remove_id_from_field($_REQUEST['pid'], $_REQUEST['psid'], 'dfrps_manually_blocked_ids');
  if ($result !== FALSE) {
    $response = array(
      'status' => 'success',
      'message' => 'Product Set has been updated.',
    );
  }
  else {
    $response = array(
      'status' => 'error',
      'message' => 'Product Set has not updated.',
      'data' => array('html' => t('Product Set has not updated.')),
    );
  }
  echo json_encode($response);
  die;
}

/**
 * Method should check the product set and return the ID.
 */
function dfrapi_get_product_set_id($ps_ID, $title, $query) {

  $product_set_id = NULL;

  if ($ps_ID == 0) {
    try {
      $product_set_id = db_insert('datafeedr_product_sets')
        ->fields(
          array(
            'dfrps_title' => $title,
            'dfrps_query' => ($query) ? $query : '',
            'dfrps_temp_query' => ($query) ? $query : '',
            'dfrps_created' => time(),
            'dfrps_modified' => time(),
            'dfrps_update_phase' => 0,
            'dfrps_next_update_time' => 0,
          )
        )
        ->execute();
    }
    catch (Exception $e) {

    }
    return $product_set_id;
  }
  else {

    /* Check the product set exist or not. */
    $result = db_select('datafeedr_product_sets', 'dfrps')
      ->fields('dfrps', array('dfrps_id'))
      ->condition('dfrps.dfrps_id', $ps_ID, '=')
      ->execute()
      ->fetchAssoc();

    if (!empty($result['dfrps_id'])) {

      try {
        db_update('datafeedr_product_sets')
          ->fields(
            array(
              'dfrps_title' => $title,
              'dfrps_query' => ($query) ? $query : '',
              'dfrps_temp_query' => ($query) ? $query : '',
              'dfrps_modified' => time(),
            )
          )
          ->condition('dfrps_id', $result['dfrps_id'], '=')
          ->execute();
      }
      catch (Exception $e) {

      }
      return $ps_ID;
    }
  }
  return $product_set_id;
}

/**
 * Fix missing image.
 */
function dfrps_ajax_fix_missing_images() {

  /*
   * Find number of product to fix the missing images.
   */
  $query = db_select('field_data_dfrps_product_check_image', 'pci');
  $query->leftJoin('node', 'n', 'n.nid = pci.entity_id');
  $query->leftJoin('field_data_uc_product_image', 'upi', 'upi.entity_id = pci.entity_id');
  $query->leftJoin('field_data_dfrps_is_dfrps_product', 'idp', 'idp.entity_id = pci.entity_id');
  $query->fields('pci', array('entity_id'));
  $query->condition('n.status', 1);
  $query->condition('pci.dfrps_product_check_image_value', 0);
  $query->condition('idp.dfrps_is_dfrps_product_value', 1);
  $query->isNull('upi.entity_id');
  $result = $query->execute();
  $node_count = $result->rowCount();

  if (is_int($node_count)) {

    if ($node_count == 0) {
      echo number_format($node_count) . ' ' . t('product images need fixing at this time.');
    }
    else {
      // Undefined method UpdateQuery::join() in db_update.
      // Please refer https://www.drupal.org/node/1792486.
      db_query("UPDATE field_data_dfrps_product_check_image AS ci LEFT JOIN field_data_uc_product_image AS pi ON pi.entity_id = ci.entity_id LEFT JOIN node AS n ON n.nid = ci.entity_id LEFT JOIN field_data_dfrps_is_dfrps_product AS dp ON dp.entity_id = ci.entity_id SET ci.dfrps_product_check_image_value = '1' WHERE pi.entity_id IS NULL AND ci.dfrps_product_check_image_value = '0' AND n.status = '1' AND dp.dfrps_is_dfrps_product_value = '1'");

      echo sprintf(format_plural($node_count, '%s product image is flagged to be fixed the next time it is displayed on your site.', '%s product images are flagged to be fixed the next they are displayed on your site.'), number_format($node_count));
    }
  }
  else {
    echo t('There was an error with your request.');
  }
  die;
}

/**
 * Bulk import.
 */
function dfrps_ajax_start_batch_image_import() {
  variable_set('dfrps_do_batch_image_import', 1);
  die;
}

/**
 * Bulk import stop.
 */
function dfrps_ajax_stop_batch_image_import() {
  variable_del('dfrps_do_batch_image_import');
  sleep(2);
  die;
}

/**
 * Bulk import start.
 */
function dfrps_ajax_batch_import_images() {

  $do_import = variable_get('dfrps_do_batch_image_import', 0);

  if (!$do_import) {
    // Don't translate this. We use this value on the other side.
    echo 'Stopped';
    die;
  }

  /*
   * Find nid of product missing image.
   */
  $query = db_select('node', 'n');
  $query->leftJoin('field_data_dfrps_product_check_image', 'pci', 'pci.entity_id = n.nid');
  $query->leftJoin('field_data_uc_product_image', 'upi', 'upi.entity_id = n.nid');
  $query->leftJoin('field_data_dfrps_is_dfrps_product', 'idp', 'idp.entity_id = n.nid');
  $query->fields('n', array('nid'));
  $query->condition('n.status', 1);
  $query->condition('pci.dfrps_product_check_image_value', 1);
  $query->condition('idp.dfrps_is_dfrps_product_value', 1);
  $query->isNull('upi.entity_id');
  $query->range(0, 1);

  $result = $query->execute();
  $node_info = $result->fetchField();

  if ($node_info == 0) {
    variable_del('dfrps_do_batch_image_import');
    // Don't translate this. We use this value on the other side.
    echo 'Complete';
    die;
  }

  $node = node_load($node_info);

  if ($node->dfrps_featured_image_url[$node->language][0]['value'] != '') {
    $product_image = $node->dfrps_featured_image_url[$node->language][0]['value'];
    $file_info = system_retrieve_file($product_image, 'public://', TRUE, FILE_EXISTS_REPLACE);
    if ($file_info->fid) {
      $node->uc_product_image[LANGUAGE_NONE]['0']['fid'] = $file_info->fid;
      $node->dfrps_product_check_image[$node->language][0]['value'] = 0;
    }
  }
  node_save($node);
  $title = $node->title;
  echo '<div>Image imported for ' . l($title, 'node/' . $node_info) . '</div>';
  die;
}
