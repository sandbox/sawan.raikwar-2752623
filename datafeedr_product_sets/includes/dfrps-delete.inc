<?php

/**
 * @file
 * This file is used to delete a Datafeedr Product Sets.
 */

/**
 * Method should return a form for Datafeedr Product Sets delete page.
 */
function datafeedr_product_sets_del_form($form, &$form_state) {

  $id = $form_state['build_info']['args'][0];
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
  return confirm_form($form, t('Are you sure you want to delete this item?'), 'admin/config/datafeedr_product_sets', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Method should handle the submit.
 */
function datafeedr_product_sets_del_form_submit($form, &$form_state) {

  $query = db_delete('datafeedr_product_sets')
    ->condition('dfrps_id', $form_state['values']['id']);
  $result = $query->execute();
  if ($result) {
    drupal_set_message(t('Product Set is deleted.'));
  }
  else {
    drupal_set_message(t('Error in Product Set deletion.'), 'error');
  }
  drupal_goto('admin/config/datafeedr_product_sets');
}
