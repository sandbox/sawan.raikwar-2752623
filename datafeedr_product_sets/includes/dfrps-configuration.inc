<?php

/**
 * @file
 * This file is used to display Datafeedr Product Sets configuration page.
 */

/**
 * Method should return a form for Datafeedr Product Sets configuration page.
 */
function datafeedr_product_sets_config_form($form, &$form_state) {

  $dfrps_configuration = variable_get('dfrps_configuration', array());

  $sform = new Dfrapi_SearchForm();
  $default_filters = array();
  if (isset($dfrps_configuration['default_filters'])) {
    $default_filters = $dfrps_configuration['default_filters'];
  }

  $default_cpt = array();
  if (isset($default_filters['dfrps_query'])) {
    $default_cpt = $default_filters['dfrps_query'];
  }

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Configuration - Datafeedr Product Sets'),
  );

  /* Section: Search Settings	 */
  $form['search_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $options_ser = array(
    10 => 10,
    20 => 20,
    30 => 30,
    40 => 40,
    50 => 50,
    60 => 60,
    70 => 70,
    80 => 80,
    90 => 90,
    100 => 100,
  );
  $form['search_settings']['num_products_per_search'] = array(
    '#type' => 'select',
    '#title' => t('Products per Search'),
    '#options' => $options_ser,
    '#default_value' => isset($dfrps_configuration['num_products_per_search']) ? $dfrps_configuration['num_products_per_search'] : 100,
    '#description' => t("Set the number of products to display per page of search results."),
    '#required' => TRUE,
  );
  $form['search_settings']['num_products_per_search1'] = array(
    '#type' => 'item',
    '#title' => t('Default Search Setting'),
    '#markup' => $sform->render('dfrps_configuration[dfrps_query]', $default_cpt),
    '#description' => t("Set the default search parameters for creating new Product Sets."),
  );

  /* Section: General Update Settings.	 */
  $form['general_update_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Update Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $options_cpt = array(0 => t('Ubercart Product'), 1 => t('Drupal Commerce'));
  $form['general_update_settings']['default_cpt'] = array(
    '#type' => 'radios',
    '#title' => t('Default Custom Post Type'),
    '#options' => $options_cpt,
    '#default_value' => isset($dfrps_configuration['default_cpt']) ? $dfrps_configuration['default_cpt'] : 0,
    '#description' => t('Set the custom post type your Product Sets will import into.'),
    '#required' => TRUE,
  );
  $form['general_update_settings']['default_cpt'][1] = array(
    '#disabled' => TRUE,
  );

  $options_del = array(1 => t('Yes'), 0 => t('No'));
  $form['general_update_settings']['delete_missing_products'] = array(
    '#type' => 'radios',
    '#title' => t('Delete Missing Products'),
    '#options' => $options_del,
    '#default_value' => isset($dfrps_configuration['delete_missing_products']) ? $dfrps_configuration['delete_missing_products'] : 1,
    '#description' => t('Delete products which are no longer available in the API.'),
    '#required' => TRUE,
  );

  $options_upd = array(1 => t('Enabled'), 0 => t('Disabled'));
  $form['general_update_settings']['updates_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Updates'),
    '#options' => $options_upd,
    '#default_value' => isset($dfrps_configuration['updates_enabled']) ? $dfrps_configuration['updates_enabled'] : 1,
    '#description' => t('Enable or disable Product Set updates. Disabling updates will not immediately affect your website, however, over time your product data will become outdated.'),
    '#required' => TRUE,
  );

  /* Section: Advanced Update Settings. */
  $desc_update_setting = '<span style="color:red;">';
  $desc_update_setting .= t('WARNING - Modifying the following settings could have severely negative effects on your server. We recommend you do not change the default settings unless you are sure that your server can handle the change.');
  $desc_update_setting .= '</span>';
  $form['advanced_update_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Update Settings'),
    '#description' => $desc_update_setting,
  );

  $options_int_upd = array(
    1 => t('Every Day'),
    2 => t('Every 2 Days'),
    3 => t('Every 3 Days'),
    4 => t('Every 4 Days'),
    5 => t('Every 5 Days'),
    6 => t('Every 6 Days'),
    7 => t('Every 7 Days'),
    10 => t('Every 10 Days'),
    14 => t('Every 14 Days'),
    21 => t('Every 21 Days'),
    30 => t('Every 30 Days'),
    45 => t('Every 45 Days'),
    60 => t('Every 60 Days'),
  );

  $update_interval = '';
  if (isset($dfrps_configuration['update_interval'])) {
    $update_interval = $dfrps_configuration['update_interval'];
  }
  $desc_update_interval = t('How often a Product Set will be updated.');
  $desc_update_interval .= '<br />';
  $desc_update_interval .= t('If updates are causing too much load on your server, update less frequently.');
  $desc_update_interval .= $update_interval;
  $form['advanced_update_settings']['update_interval'] = array(
    '#type' => 'select',
    '#title' => t('Update Interval'),
    '#options' => $options_int_upd,
    '#default_value' => isset($dfrps_configuration['update_interval']) ? $dfrps_configuration['update_interval'] : 7,
    '#description' => $desc_update_interval,
    '#required' => TRUE,
  );

  $options_int_cron = array(
    10 => t('Every 10 seconds'),
    30 => t('Every 30 seconds'),
    60 => t('Every minute'),
    120 => t('Every 2 minutes'),
    300 => t('Every 5 minutes'),
    600 => t('Every 10 minutes'),
    900 => t('Every 15 minutes'),
    1800 => t('Every 30 minutes'),
    3600 => t('Every hour'),
  );
  $desc_cron_interval = t('How often Drupal Cron will check if Product Sets should be updated.');
  $desc_cron_interval .= '<br />';
  $desc_cron_interval .= t('Increase this value if you are experiencing server load or timeout issues.');
  $form['advanced_update_settings']['cron_interval'] = array(
    '#type' => 'select',
    '#title' => t('Cron Interval'),
    '#required' => TRUE,
    '#options' => $options_int_cron,
    '#default_value' => isset($dfrps_configuration['cron_interval']) ? $dfrps_configuration['cron_interval'] : 60,
    '#description' => $desc_cron_interval,
  );

  $options_per_upd = array(
    10 => '10',
    25 => '25',
    50 => '50',
    75 => '75',
    100 => '100',
    150 => '150',
    200 => '200',
    250 => '250',
    300 => '300',
    350 => '350',
    400 => '400',
    450 => '450',
    500 => '500',
  );
  $desc_num_products_per_update = t('The number of products per batch to import into your store.');
  $desc_num_products_per_update .= '<br />';
  $desc_num_products_per_update .= t('This process is server intensive. Reduce this number if you are experiencing server load or timeout issues.');
  $form['advanced_update_settings']['num_products_per_update'] = array(
    '#type' => 'select',
    '#title' => t('Products per Update'),
    '#required' => TRUE,
    '#options' => $options_per_upd,
    '#default_value' => isset($dfrps_configuration['num_products_per_update']) ? $dfrps_configuration['num_products_per_update'] : 100,
    '#description' => $desc_num_products_per_update,
  );

  $options_pre_max = array(
    25 => '25',
    50 => '50',
    75 => '75',
    100 => '100',
    200 => '200',
    300 => '300',
    400 => '400',
    500 => '500',
    600 => '600',
    700 => '700',
    800 => '800',
    900 => '900',
    1000 => '1000',
  );
  $desc_preprocess_maximum = t('The number of products per batch to prepare for updating or deleting.');
  $desc_preprocess_maximum .= '<br />';
  $desc_preprocess_maximum .= t('This process is server intensive. Reduce this number if you are experiencing server load or timeout issues.');
  $form['advanced_update_settings']['preprocess_maximum'] = array(
    '#type' => 'select',
    '#title' => t('Preprocess Maximum'),
    '#required' => TRUE,
    '#options' => $options_pre_max,
    '#default_value' => isset($dfrps_configuration['preprocess_maximum']) ? $dfrps_configuration['preprocess_maximum'] : 100,
    '#description' => $desc_preprocess_maximum,
  );

  $options_post_max = array(
    25 => '25',
    50 => '50',
    75 => '75',
    100 => '100',
    200 => '200',
    300 => '300',
    400 => '400',
    500 => '500',
    600 => '600',
    700 => '700',
    800 => '800',
    900 => '900',
    1000 => '1000',
  );
  $desc_postprocess_maximum = t('The number of products per batch to delete.');
  $desc_postprocess_maximum .= '<br />';
  $desc_postprocess_maximum .= t('This process is server intensive. Reduce this number if you are experiencing server load or timeout issues.');
  $form['advanced_update_settings']['postprocess_maximum'] = array(
    '#type' => 'select',
    '#title' => t('Postprocess Maximum'),
    '#required' => TRUE,
    '#options' => $options_post_max,
    '#default_value' => isset($dfrps_configuration['postprocess_maximum']) ? $dfrps_configuration['postprocess_maximum'] : 100,
    '#description' => $desc_postprocess_maximum,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );
  return $form;
}

/**
 * Handle the configuration form submit.
 */
function datafeedr_product_sets_config_form_submit($form, &$form_state) {

  /* Storing the values. */
  $dfrps_configuration = array(
    'update_interval' => $form_state['values']['update_interval'],
    'num_products_per_update' => $form_state['values']['num_products_per_update'],
    'num_products_per_api_request' => '',
    'delete_missing_products' => $form_state['values']['delete_missing_products'],
    'preprocess_maximum' => $form_state['values']['preprocess_maximum'],
    'postprocess_maximum' => $form_state['values']['postprocess_maximum'],
    'num_products_per_search' => $form_state['values']['num_products_per_search'],
    'default_filters' => $_REQUEST['dfrps_configuration'],
    'cron_interval' => $form_state['values']['cron_interval'],
    'updates_enabled' => $form_state['values']['updates_enabled'],
    'default_cpt' => $form_state['values']['default_cpt'],
  );

  variable_set('dfrps_configuration', $dfrps_configuration);
  drupal_set_message(t('Configuration successfully updated.'));
  drupal_goto('admin/config/datafeedr_product_sets/product/configuration');
}
