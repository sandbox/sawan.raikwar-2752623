<?php

/**
 * @file
 * This file is used to display Datafeedr Add OR Edit Product Sets form/page.
 */

/**
 * Datafeedr product sets form to filter the product set.
 */
function datafeedr_product_sets_form($form, &$form_state) {

  global $base_path;

  /* Added css & js inline. */
  drupal_add_js(drupal_get_path('module', 'datafeedr_product_sets') . '/js/cpt.js');
  drupal_add_js(drupal_get_path('module', 'datafeedr_product_sets') . '/js/cpt-ajax.js');

  /* Get default configurations. */
  $dfrps_configuration = variable_get('dfrps_configuration', array());

  $default_filters = $dfrps_configuration['default_filters'];
  if (isset($default_filters['dfrps_query'])) {
    $default_filters = $default_filters['dfrps_query'];
  }
  else {
    $default_filters = array();
  }

  /* Check for valid product set id for edit. */
  $ps_id = (NULL !== arg(4)) ? arg(4) : 0;

  if ($ps_id > 0) {
    drupal_add_css('.dfrps_loading {background-image: url("./../../../../' . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/loading.gif") !important;}', array('type' => 'inline'));
  }
  else {
    drupal_add_css('.dfrps_loading {background-image: url("./../../../' . drupal_get_path('module', 'datafeedr_product_sets') . '/images/icons/loading.gif") !important;}', array('type' => 'inline'));
  }

  $dfrps_categories = array();
  if ($ps_id > 0) {
    $dfrps_info = db_select('datafeedr_product_sets', 'dfrps')
      ->fields('dfrps')
      ->condition('dfrps.dfrps_id', $ps_id)
      ->execute()
      ->fetchAssoc();

    $dfrps_title = $dfrps_info['dfrps_title'];
    $default_filters = unserialize($dfrps_info['dfrps_temp_query']);

    if ($dfrps_info['dfrps_categories'] != '') {
      $dfrps_categories = unserialize($dfrps_info['dfrps_categories']);
    }
  }

  if ($ps_id > 0 && !isset($dfrps_info['dfrps_id'])) {
    // Drupal_set_message( t( 'Product set not found.' ), 'error' ).
    drupal_goto('admin/config/datafeedr_product_sets/product/dfrps_add');
  }

  /* Create form. */
  $sform = new Dfrapi_SearchForm();
  $form = array();

  /* Adding form wrapper: LEFT. */
  $form['left']['#prefix'] = '<div id="datafeedr-search-left">';
  $form['left']['#suffix'] = '</div>';

  $form['left']['description'] = array(
    '#type' => 'item',
    '#title' => ($ps_id > 0) ? t('Edit Product Set:') . ' ' . $dfrps_title : t('Add New Product Set'),
  );
  $form['left']['product_set_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Product set title'),
    '#default_value' => isset($dfrps_title) ? $dfrps_title : '',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['left']['div_dfrps_tabs'] = array(
    '#type' => 'horizontal_tabs',
    '#tree' => TRUE,
    '#prefix' => '<div id="unique-wrapper">',
    '#suffix' => '</div>',
  );
  $form['left']['div_dfrps_tabs']['tab_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container'] = array(
    '#type' => 'container',
    '#title' => '',
    '#prefix' => '<div id="div_dfrps_tab_search" class="psbox"><div class="inside">',
    '#suffix' => '</div></div>',
  );

  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['num_products_per_search1'] = array(
    '#type' => 'item',
    '#title' => '',
    '#markup' => $sform->render('_dfrps_cpt_query', $default_filters),
  );

  /* Adding action option */
  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['left-footer']['action']['#prefix'] = '<div id="left-action">';
  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['left-footer']['action']['#suffix'] = '</div>';

  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['left-footer']['action']['dfrps_raw_query'] = array(
    '#markup' => '<span class="dfrps_raw_query"><a href="#" id="dfrps_view_raw_query">view api request</a></span>&nbsp;',
  );

  $add_update_button_text = (isset($dfrps_info) && $dfrps_info['dfrps_query'] != '') ? t('Update Saved Search') : t('Add as Saved Search');
  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['left-footer']['action']['dfrps_cpt_save_search'] = array(
    '#markup' => '<div id="dfrps_save_update_search_actions">
			<input type="button" class="form-submit" id="dfrps_cpt_save_search" value="' . $add_update_button_text . '" />
		</div>',
  );
  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['left-footer']['action']['dfrps_cpt_search'] = array(
    '#markup' => '<input type="button" class="form-submit" value="Search" name="button_dfrps_cpt_search" id="dfrps_cpt_search">',
  );

  /* Load search result. */
  $form['left']['div_dfrps_tabs']['tab_search']['tab_search_container']['left-footer']['dfrps_tab_search_results'] = array(
    '#markup' => '<div id="div_dfrps_tab_search_results"></div>',
  );

  // TAB: Saved Search.
  $form['left']['div_dfrps_tabs']['tab_saved_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Saved Search (0)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['left']['div_dfrps_tabs']['tab_saved_search']['tab_saved_container'] = array(
    '#type' => 'container',
    '#title' => '',
    '#prefix' => '<div id="div_dfrps_tab_saved_search" class="psbox"><div class="inside">',
    '#suffix' => '</div></div>',
  );
  $form['left']['div_dfrps_tabs']['tab_saved_search']['tab_saved_container']['left-footer']['action'] = array(
    '#prefix' => '<div id="left-action" class="clearfix">',
    '#suffix' => '</div>',
  );
  $form['left']['div_dfrps_tabs']['tab_saved_search']['tab_saved_container']['left-footer']['action']['dfrps_cpt_saved_search'] = array(
    '#markup' => '<input type="button" class="form-submit" value="Load & Refresh Saved Search" name="button_dfrps_cpt_saved_search" id="dfrps_cpt_saved_search" title="Click on button will perform a API request.">',
  );

  /* Load saved search result. */
  $form['left']['div_dfrps_tabs']['tab_saved_search']['tab_saved_container']['left-footer']['dfrps_tab_saved_search_results'] = array(
    '#markup' => '<div id="div_dfrps_tab_saved_search_results"></div>',
  );

  // TAB: Included.
  $form['left']['div_dfrps_tabs']['tab_included'] = array(
    '#type' => 'fieldset',
    '#title' => t('Single Products <span title="Products individually added to this Product Set." class="count">(0)</span>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['left']['div_dfrps_tabs']['tab_included']['tab_included_container'] = array(
    '#type' => 'container',
    '#title' => '',
    '#prefix' => '<div id="div_dfrps_tab_included" class="psbox"><div class="inside">',
    '#suffix' => '</div></div>',
  );
  $form['left']['div_dfrps_tabs']['tab_included']['tab_included_container']['left-footer']['action'] = array(
    '#prefix' => '<div id="left-action" class="clearfix">',
    '#suffix' => '</div>',
  );
  $form['left']['div_dfrps_tabs']['tab_included']['tab_included_container']['left-footer']['action']['dfrps_cpt_included_search'] = array(
    '#markup' => '<input type="button" class="form-submit" value="Load & Refresh Included Search" name="button_dfrps_cpt_included_search" id="dfrps_cpt_included_search" title="Click on button will perform a API request.">',
  );

  /* Load included search result. */
  $form['left']['div_dfrps_tabs']['tab_included']['tab_included_container']['left-footer']['dfrps_tab_included_search_results'] = array(
    '#markup' => '<div id="div_dfrps_tab_included_search_results"></div>',
  );

  // TAB: Blocked.
  $form['left']['div_dfrps_tabs']['tab_blocked'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocked Products <span title="Products blocked from this Product Set." class="count">(0)</span>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['left']['div_dfrps_tabs']['tab_blocked']['tab_blocked_container'] = array(
    '#type' => 'container',
    '#title' => '',
    '#prefix' => '<div id="div_dfrps_tab_blocked" class="psbox"><div class="inside">',
    '#suffix' => '</div></div>',
  );
  $form['left']['div_dfrps_tabs']['tab_blocked']['tab_blocked_container']['left-footer']['action'] = array(
    '#prefix' => '<div id="left-action" class="clearfix">',
    '#suffix' => '</div>',
  );
  $form['left']['div_dfrps_tabs']['tab_blocked']['tab_blocked_container']['left-footer']['action']['dfrps_cpt_blocked_search'] = array(
    '#markup' => '<input type="button" class="form-submit" value="Load & Refresh Blocked Search" name="button_dfrps_cpt_blocked_search" id="dfrps_cpt_blocked_search" title="Click on button will perform a API request.">',
  );

  /* Load blocked search result. */
  $form['left']['div_dfrps_tabs']['tab_blocked']['tab_blocked_container']['left-footer']['dfrps_tab_blocked_search_results'] = array(
    '#markup' => '<div id="div_dfrps_tab_blocked_search_results"></div>',
  );

  /* Adding form wrapper: RIGHT. */
  $form['right']['#prefix'] = '<div id="datafeedr-search-right">';
  $form['right']['#suffix'] = '</div>';

  $form['right']['category_container'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="dfrps_product_product_cat_category_chooser">
	<h3 class="hndle"><span>Ubercart Categories</span></h3>
	<div class="dfrps_tax_instructions">Add this Product Set to a Product Category.</div>
	<div id="product_cat-all" class="tabs-panel dfrps_category_selection_panel">
		<div class="dfrps_saving_taxonomy">Saving...</div>
		<div id="product_catchecklist" class="categorychecklist" cpt="product">',
    '#suffix' => '</div></div></div>',
  );

  $vacobulary = taxonomy_vocabulary_machine_name_load('catalog');
  $tree = taxonomy_get_tree($vacobulary->vid);
  if ($tree && (count($tree) > 0)) {
    foreach ($tree as $term) {
      $chk = array(
        'name' => 'tax_input[product_cat][]',
        'id' => 'in-product_cat-' . $term->tid,
        'class' => array('selectit'),
      );
      if (isset($dfrps_categories['product']) && !empty($dfrps_categories['product'])) {
        if (in_array($term->tid, $dfrps_categories['product'])) {
          $chk = array(
            'name' => 'tax_input[product_cat][]',
            'id' => 'in-product_cat-' . $term->tid,
            'class' => array('selectit'),
            'checked' => 'checked',
          );
        }
      }

      $form['right']['category_container']['product_catchecklist'][$term->tid] = array(
        '#type' => 'checkbox',
        '#title' => $term->name,
        '#return_value' => $term->tid,
        '#field_prefix' => str_repeat('&nbsp;&nbsp;', $term->depth),
        '#attributes' => $chk,
      );
    }
  }

  /* Hidden Product Set ID. */
  $form['ps_ID'] = array(
    '#type' => 'hidden',
    '#name' => 'ps_ID',
    '#attributes' => array('id' => 'ps_ID'),
    '#default_value' => ($ps_id > 0) ? $ps_id : 0,
  );

  $form['searching_x_products'] = array(
    '#type' => 'hidden',
    '#name' => 'searching_x_products',
    '#attributes' => array('id' => 'searching_x_products'),
    '#default_value' => dfrapi_get_total_products_in_db(TRUE, ''),
  );

  $form['icon_path'] = array(
    '#type' => 'hidden',
    '#name' => 'icon_path',
    '#attributes' => array('id' => 'icon_path'),
    '#default_value' => $base_path . drupal_get_path('module', 'datafeedr_product_sets'),
  );

  $form['right']['search_button_right'] = array(
    '#type' => 'submit',
    '#value' => t('Save Product Set'),
  );

  return $form;
}

/**
 * Datafeedr product set filter form submit handler.
 */
function datafeedr_product_sets_form_submit($form, &$form_state) {
  // Check for the ps_ID.
  $ps_ID = $form_state['values']['ps_ID'];
  $cpt = 'product';

  try {
    if ($ps_ID == 0) {

      $cids = array();
      $dfrps_categories = array();
      $categories = '';
      if (isset($_REQUEST['tax_input'])) {
        $product_cat = $_REQUEST['tax_input'];
        $cids = $product_cat['product_cat'];
        $dfrps_categories[$cpt] = $cids;

        if (!empty($cids) && count($cids) > 0) {
          $categories = serialize($dfrps_categories);
        }
      }
      $dfrps_cpt_query = '';
      if (!empty($_REQUEST['_dfrps_cpt_query'])) {
        $dfrps_cpt_query = serialize($_REQUEST['_dfrps_cpt_query']);
      }

      $product_set_id = db_insert('datafeedr_product_sets')
        ->fields(
          array(
            'dfrps_title' => $form_state['values']['product_set_title'],
            // 'dfrps_query' => $dfrps_cpt_query,.
            'dfrps_temp_query' => $dfrps_cpt_query,
            'dfrps_categories' => $categories,
            'dfrps_categories_history' => $categories,
            'dfrps_created' => time(),
            'dfrps_modified' => time(),
            'dfrps_update_phase' => 0,
            'dfrps_next_update_time' => 0,
          )
        )
        ->execute();
      drupal_set_message(t('Product set successfully added.'));
    }
    else {
      /* Check the product set exist or not. */
      $product_set_id = $ps_ID;
      $result = db_select('datafeedr_product_sets', 'dfrps')
        ->fields('dfrps', array('dfrps_id', 'dfrps_categories_history'))
        ->condition('dfrps.dfrps_id', $ps_ID, '=')
        ->execute()
        ->fetchAssoc();

      if (!empty($result['dfrps_id'])) {

        $categories_history = unserialize($result['dfrps_categories_history']);

        $cids = array();
        if (isset($_REQUEST['tax_input'])) {
          $product_cat = $_REQUEST['tax_input'];
          $cids = $product_cat['product_cat'];
        }

        if (is_array($categories_history[$cpt]) && !empty($categories_history[$cpt])) {
          $categories_history[$cpt] = array_merge($categories_history[$cpt], $cids);
        }
        else {
          $categories_history[$cpt] = $cids;
        }

        $dfrps_categories_history[$cpt] = array_unique($categories_history[$cpt]);
        $dfrps_categories[$cpt] = $cids;

        $dfrps_cpt_query = '';
        if (!empty($_REQUEST['_dfrps_cpt_query'])) {
          $dfrps_cpt_query = serialize($_REQUEST['_dfrps_cpt_query']);
        }

        $product_set_update = db_update('datafeedr_product_sets')
          ->fields(
            array(
              'dfrps_title' => $_REQUEST['product_set_title'],
              // 'dfrps_query' => $dfrps_cpt_query,.
              'dfrps_temp_query' => $dfrps_cpt_query,
              'dfrps_categories' => (!empty($cids)) ? serialize($dfrps_categories) : '',
              'dfrps_categories_history' => (!empty($dfrps_categories_history[$cpt])) ? serialize($dfrps_categories_history) : '',
              'dfrps_modified' => time(),
            )
          )
          ->condition('dfrps_id', $product_set_id, '=');
        $product_set_update->execute();
        drupal_set_message(t('Product set successfully updated.'));
      }
      else {
        drupal_set_message(t('Invalid Product set.'), 'error');
      }
    }
    drupal_goto('admin/config/datafeedr_product_sets/dfrps_edit/' . $product_set_id);
  }
  catch (Exception $e) {

  }
}
