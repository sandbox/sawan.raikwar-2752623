<?php

/**
 * @file
 * This file is used to display Datafeedr Tools page.
 */

/**
 * Method should return a form for Datafeedr Product Sets Add New OR Edit page.
 */
function datafeedr_product_sets_tools_form($form, &$form_state) {

  /* added css & js inline */
  drupal_add_js(drupal_get_path('module', 'datafeedr_product_sets') . '/js/tools.js');

  /* Create form. */
  $form = array();

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Tools - Datafeedr Product Sets'),
    '#prefix' => '<h1>',
    '#suffix' => '</h1>',
  );

  /* Fix Missing Images. */
  $form['fix_missing_images'] = array(
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
    '#markup' => t('Fix Missing Images'),
  );
  $form['fix_missing_images_desc'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => t("If a product has an image but it wasn't imported into your site successfully, click the [Fix Missing Images] button to attempt to redownload the missing images the next time those products are displayed on your site."),
  );

  $form['fix_missing_images_button'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p><div id="dfrps_fix_missing_images_result" style="padding: 10px; border: 1px solid silver; display: none; background: #FFF;"></div>',
    '#markup' => l(t('Fix Missing Images'), '#', array(
      'attributes' => array(
        'class' => 'button',
        'id' => 'dfrps_fix_missing_images',
      ),
    )
    ),
  );

  /* Bulk Image Import. */
  $form['bulk_image_import'] = array(
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
    '#markup' => t('Bulk Import'),
  );
  $form['bulk_image_import_desc'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => t("When a product is first displayed on your website, its image is downloaded from the merchant's website. If you want to download product images before they are displayed on your website, click the !str01[Start Image Import]!str02 button to begin importing images for products which do not already have an image. !str03 This process is server intensive. Use with caution!!str04", array(
      '!str01' => '<strong>',
      '!str02' => '</strong>',
      '!str03' => '<br /><span class="dfrps_warning">',
      '!str04' => '</span>',
    )),
  );

  $desc_01 = l(t('Start Image Import'), '#', array(
    'attributes' => array(
      'class' => 'button',
      'id' => 'dfrps_start_batch_image_import',
    ),
  )
  );
  $desc_01 .= l(t('Stop'), '#', array(
    'attributes' => array(
      'class' => 'button',
      'id' => 'dfrps_stop_batch_image_import',
      'style' => 'display: none;',
    ),
  )
  );
  $form['bulk_image_import_button'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p><div id="dfrps_start_batch_image_import_result" style="padding: 10px; border: 1px solid silver; display: none; background: #FFF;"></div>',
    '#markup' => $desc_01,
  );

  return $form;
}
