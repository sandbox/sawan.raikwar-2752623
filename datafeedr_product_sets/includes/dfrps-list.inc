<?php

/**
 * @file
 * This file is used to display the list of all Datafeedr Product Sets.
 */

/**
 * Listing for product set.
 */
function datafeedr_product_sets_list() {

  // Initialising output.
  $output = '';
  $output .= '<h6>' . l(t('Add New Product Set'), 'admin/config/datafeedr_product_sets/product/dfrps_add') . '</h6> ';

  // Table header.
  $header = array(
    array('data' => 'Set Name', 'field' => 'dfrps_title'),
    array('data' => 'Created', 'field' => 'dfrps_created'),
    array('data' => 'Modified'),
    array('data' => 'Status'),
    array('data' => 'Next Update'),
    array('data' => 'Started'),
    array('data' => 'Completed'),
    array('data' => 'Added'),
    array('data' => 'Deleted'),
    array('data' => 'API Requests'),
    array('data' => 'Actions'),
  );

  // Setting the sort conditions.
  $sort = 'ASC';
  $order = 'dfrps_title';
  if (isset($_GET['sort']) && isset($_GET['order'])) {
    // Sort it Ascending or Descending?
    $sort = ($_GET['sort'] == 'asc') ? 'ASC' : 'DESC';

    // Which column will be sorted.
    switch ($_GET['order']) {
      case 'Title':
        $order = 'dfrps_title';
        break;

      case 'Date Created':
        $order = 'dfrps_created';
        break;

      default:
        $order = 'title';
        break;
    }
  }

  // Query.
  $query = db_select('datafeedr_product_sets', 'dfrps');
  // Adding fields.
  $query->fields('dfrps', array(
    'dfrps_id',
    'dfrps_title',
    'dfrps_created',
    'dfrps_modified',
    'dfrps_update_phase',
    'dfrps_status',
    'dfrps_next_update_time',
    'dfrps_last_update_time_started',
    'dfrps_last_update_time_completed',
    'dfrps_last_update_num_products_added',
    'dfrps_last_update_num_products_deleted',
    'dfrps_last_update_num_api_requests',
    'dfrps_errors',
  )
  );
  // Set order by.
  $query->orderBy($order, $sort);
  // Pagination.
  $query = $query->extend('TableSort')->extend('PagerDefault')->limit(10);
  // Executing query.
  $results = $query->execute();

  $rows = array();
  foreach ($results as $result) {

    /* Created. */
    $created = '<abbr title="This Product Set was created on ' . $result->dfrps_created . '">' . dfrps_date_in_two_rows($result->dfrps_created) . '</abbr>';

    /* Modified. */
    $modified = '<abbr title="This Product Set was modified on ' . $result->dfrps_modified . '">' . dfrps_date_in_two_rows($result->dfrps_modified) . '</abbr>';

    /* Status. */
    $status = ($result->dfrps_status == 1) ? 'Published' : 'Unpublished';

    /* Next update. */
    $dfrps_next_update_time = (isset($result->dfrps_next_update_time)) ? $result->dfrps_next_update_time : 0;

    $next_update = '&mdash;';
    if ($status == 'Published') {
      $next_update = ($dfrps_next_update_time == 0) ? '<abbr title="This Product Set will update as soon as possible.' . '">ASAP</abbr>' : '<abbr title="This Product Set will update on (or after) ' . format_date($result->dfrps_next_update_time, 'custom', 'Y-m-d G:i:s') . '">' . dfrps_date_in_two_rows($dfrps_next_update_time) . '</abbr>';
    }

    /* Started. */
    $dfrps_last_update_time_started = (isset($result->dfrps_last_update_time_started)) ? $result->dfrps_last_update_time_started : 0;

    $started = ($dfrps_last_update_time_started > 0) ? '<abbr title="This Product Set\'s last update started on ' . format_date($result->dfrps_last_update_time_started, 'custom', 'Y-m-d G:i:s') . '">' . dfrps_date_in_two_rows($dfrps_last_update_time_started) . '</abbr>' : 'Never';

    /* Completed. */
    $update_phase = intval($result->dfrps_update_phase);
    $dfrps_errors = (isset($result->dfrps_errors)) ? @unserialize($result->dfrps_errors) : '';
    $dfrps_last_update_time_completed = (isset($result->dfrps_last_update_time_completed)) ? $result->dfrps_last_update_time_completed : 0;

    if ($dfrps_errors != '') {
      $completed = dfrapi_output_api_error($dfrps_errors);
    }
    else {
      if ($update_phase == 0) {
        if ($dfrps_last_update_time_completed > 0) {
          $completed = '<abbr title="This Product Set\'s last update completed on ' . format_date($result->dfrps_last_update_time_completed, 'custom', 'Y-m-d G:i:s') . '">' . dfrps_date_in_two_rows($dfrps_last_update_time_completed) . '</abbr>';
        }
        else {
          $completed = 'Never';
        }
      }
      else {
        $percent_complete = '';
        $completed = '<span class="dfrps_currently_updating">Updating&hellip;</span>';
        if ($percent_complete) {
          $completed .= dfrps_progress_bar($percent_complete);
        }
      }
    }

    /* Added. */
    $products_added = (isset($result->dfrps_last_update_num_products_added)) ? number_format(intval($result->dfrps_last_update_num_products_added)) : 0;
    $added = '<div class="dfrps_label dfrps_label-success" title="' . $products_added . 'products were added during the last update of this Product Set.' . '">' . $products_added . '</div>';

    /* Deleted. */
    $products_deleted = (isset($result->dfrps_last_update_num_products_deleted)) ? number_format(intval($result->dfrps_last_update_num_products_deleted)) : 0;
    $deleted = '<div class="dfrps_label dfrps_label-danger" title="' . $products_deleted . ' products were moved to the Trash during the last update of this Product Set.' . '">' . $products_deleted . '</div>';

    /* Api request. */
    $api_requests = (isset($result->dfrps_last_update_num_api_requests)) ? number_format(intval($result->dfrps_last_update_num_api_requests)) : 0;
    $apirequests = '<div class="dfrps_label dfrps_label-warning" title="' . $api_requests . ' API requests were required during the last update of this Product Set.' . '">' . $api_requests . '</div>';

    $rows[] = array(l($result->dfrps_title, 'admin/config/datafeedr_product_sets/dfrps_edit/' . $result->dfrps_id),
      $created,
      $modified,
      $status,
      $next_update,
      $started,
      $completed,
      $added,
      $deleted,
      $apirequests,
      l(t('Edit'), 'admin/config/datafeedr_product_sets/dfrps_edit/' . $result->dfrps_id) . '&nbsp;' . l(t('Delete'), 'admin/config/datafeedr_product_sets/dfrps_delete/' . $result->dfrps_id),
    );
  }

  // Setting the output of the field.
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('class' => array('table table-striped table-bordered')),
    'sticky' => TRUE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => t('No product set available!'),
  )
      ) . theme('pager');
  return $output;
}
