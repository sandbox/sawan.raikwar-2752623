=== Datafeedr Product Sets ===

Contributors: datafeedr.com, cognizant.com
Tags: datafeedr, product sets, dfrapi, dfrps, import, products
Requires at least: 7.x

Build sets of products to import into your website.

== Description ==

**NOTE:** The *Datafeedr Product Sets* module requires the [Datafeedr API module].

**What is a Product Set?**

A Product Set is a collection of related products. Once you create a Product Set, the products in that set will be imported into your website (via an importer module). The Product Set is also responsible for updating those imported products with the newest information at an interval you choose.

The *Datafeedr Product Sets* module currently integrates with the following module:

* [Datafeedr API]
* [Datafeedr Ubercart Importer]

**How does it work?**

1. Create a new Product Set by performing a product search for specific keywords.  In this example lets use "rock climbing shoes" as our keywords.

1. The Datafeedr Product Sets module connects to the Datafeedr API and makes an API request querying 250 million affiliate products in the Datafeedr database for the keywords "rock climbing shoes".

1. The Datafeedr API returns the products in the database that match your search keywords.

1. At this point, you have 2 choices: You can "save" your search (so that all products returned are added to your Product Set) or you can pick and choose specific products to add to your Product Set.

1. After your Product Set has some products in it, you choose what Category to import the Product Set into. For example, you could import all of the rock climbing shoes into your Ubercart store in the "Climbing Shoes" product category.

1. Within a few seconds the Product Set will attempt to import those products into your Ubercart product category. It will do so by getting all of the products in the Product Set and passing them to an importer module (in this case the [Datafeedr Ubercart Importer module].

1. After a few minutes (depending on how many products are in your set and your update settings) your "Climbing Shoes" product category will be filled with products from your Product Set.

1. Lastly, at an interval you configure, the Product Set will trigger a product update. At this time, products no longer available via the Datafeedr API will be removed from your Ubercart store, all product information will be updated and any new products that match your "saved search" will be added to your store.

The *Datafeedr Product Sets* module requires at least one importer module to import products from a Product Set into your store.

We currently have one importer which imports products from your Product Sets into your Ubercart store: [Datafeedr Ubercart Importer module]. Additional importers will be developed over the coming months. Custom importers may also be written. Product Sets can be imported into one or more Drupal Content Types.

== Requirements ==

* Drupal Cron enabled.
* 64MB of memory.
* PHP's `allow_url_fopen` must be `On`.
* PHP's `CURL` support must be enabled.
* An [importer module] to handle importing products from your Product Sets into your website.

== Installation ==

This section describes how to install the module:

* Install the module as usual, for further information please visit http://drupal.org/node/895232

== Configuration ==

This section describes how to configure the module:

* Configure your Product Sets settings here: Drupal Admin Area > Datafeedr Configuration > Datafeedr Product Sets > Configuration
* Add a Product Set here: WordPress Admin Area > Datafeedr Configuration > Datafeedr Product Sets > Add Product Set

== Frequently Asked Questions ==

= Where can I get help?  =
