<?php

/**
 * @file
 * This file handles all the product import functions.
 */

if (!class_exists('DatafeedrProductSetsDfrpsUpdate')) {

  /**
   * Product Set Updater.
   */
  class DatafeedrProductSetsDfrpsUpdate {

    /**
     * Constructor for set Updater.
     */
    public function __construct($post) {
      $this->action = 'update';
      $this->set = $post;
      $this->config = $this->getConfiguration();
      $this->phase = 1;
      $this->update();
    }

    /**
     * Get user's configuration settings.
     */
    public function getConfiguration() {
      return variable_get('dfrps_configuration', array());
    }

    /**
     * Get the current phase of the update.
     */
    public function currentPhase() {
      $ps_interval = get_product_set_data($this->set['dfrps_id'], 'dfrps_update_phase');
      $phase = intval($ps_interval['dfrps_update_phase']);
      if ($phase == 0) {
        $phase = 1;
        update_product_set_data($this->set['dfrps_id'], array('dfrps_update_phase' => $phase));
      }
      return $phase;
    }

    /**
     * Create temporary product table.
     */
    public function createTempProductTable() {
      global $databases;
      $table = $databases['default']['default']['prefix'] . 'dfrps_product_data';
      $collation = '';
      if (isset($databases['default']['default']['collation'])) {
        $collation = $databases['default']['default']['collation'];
      }
      $sql = " CREATE TABLE IF NOT EXISTS $table 
					(
						product_id varchar(255) DEFAULT '' PRIMARY KEY,
						data LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_swedish_ci,
						updated TIMESTAMP
					) $collation ";
      db_query($sql);
    }

    /**
     * Insert into temporary product table.
     */
    public function insertTempProduct($product) {
      if (!isset($product['_id'])) {
        return FALSE;
      }
      global $databases;
      $table = $databases['default']['default']['prefix'] . 'dfrps_product_data';
      $data = array('product_id' => $product['_id'], 'data' => serialize($product));
      db_insert($table)
        ->fields($data)
        ->execute();
    }

    /**
     * Get products from temp table to update.
     */
    public function selectProductsForUpdate() {
      global $databases;
      $table = $databases['default']['default']['prefix'] . 'dfrps_product_data';
      $products = db_select($table, 'p')
        ->fields('p')
        ->orderBy('updated', 'DESC')
        ->range(0, $this->config['num_products_per_update'])
        ->execute()
        ->fetchAll();
      return $products;
    }

    /**
     * Delete a product record from the temp table.
     */
    public function deleteProductFromTable($id) {
      global $databases;
      $table = $databases['default']['default']['prefix'] . 'dfrps_product_data';
      db_delete($table)
        ->condition('product_id', $id)
        ->execute();
    }

    /**
     * Drop the temp product table.
     */
    public function dropTempProductTable() {
      global $databases;
      $table = $databases['default']['default']['prefix'] . 'dfrps_product_data';
      db_query("DROP TABLE IF EXISTS $table");
    }

    /**
     * Run update.
     */
    public function update() {

      // Getting API settings data.
      $dfrapi_configuration = variable_get('dfrapi_configuration', array());

      // Get all networks.
      if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {

        // Setting maximum execution time to complete import cycle for test.
        ini_set('max_execution_time', 500);
      }

      // Set transient to 5 minutes time + cron interval.
      $cron_interval = (int) ($this->config['cron_interval']);
      variable_set('dfrps_doing_update', ((60 * 5) + $cron_interval));

      // Begin endless loop.
      while (1) {

        // Get current phase.
        $current_phase = $this->currentPhase();

        // Create method name.
        $phase = 'phase' . $current_phase;

        // Check if method exists.
        if (!method_exists($this, $phase)) {
          break;
        }

        // Call method and get results of method.
        // Results: skip, ready, repeat, complete.
        $result = $this->$phase();
        echo $result;

        // Skip phase altogether.
        if ($result == 'skip') {
          $fields = array('dfrps_update_phase' => $current_phase + 1);
          update_product_set_data($this->set['dfrps_id'], $fields);
          // Repeat the loop.
          continue;
        }

        // Phase is complete.
        if ($result == 'ready') {
          $fields = array('dfrps_update_phase' => $current_phase + 1);
          update_product_set_data($this->set['dfrps_id'], $fields);
          // Update phase and stop the loop.
          break;
        }

        // Phase is not complete, repeat it.
        if ($result == 'repeat') {
          // Don't update phase, just stop the loop.
          break;
        }

        // Full update is complete, update phase and break.
        if ($result == 'complete') {
          $fields = array('dfrps_update_phase' => 0);
          update_product_set_data($this->set['dfrps_id'], $fields);
          // Update phase and stop the loop.
          break;
        }
      }
      // While(1) {.
      variable_del('dfrps_doing_update');
    }

    /**
     * Count each iteraction of the update process.
     */
    public function countIteration() {
      $ps_iteration = get_product_set_data($this->set['dfrps_id'], 'dfrps_update_iteration');
      $iteration = $ps_iteration['dfrps_update_iteration'];
      if (!empty($iteration)) {
        $iteration = intval($iteration);
        $iteration = ($iteration + 1);
        $fields = array('dfrps_update_iteration' => $iteration);
      }
      else {
        $iteration = 1;
        $fields = array('dfrps_update_iteration' => $iteration);
      }
      update_product_set_data($this->set['dfrps_id'], $fields);
    }

    /**
     * Check Preprocess complete.
     */
    public function preprocessCompleteCheck() {
      $complete = get_product_set_data($this->set['dfrps_id'], 'dfrps_preprocess_complete_product');
      return $complete['dfrps_preprocess_complete_product'];
    }

    /**
     * Check Postprocess complete.
     */
    public function postprocessCompleteCheck() {
      $complete = get_product_set_data($this->set['dfrps_id'], 'dfrps_postprocess_complete_product');
      return $complete['dfrps_postprocess_complete_product'];
    }

    /**
     * Set update phase for cron.
     */
    public function isFirstPass() {
      $first_pass = get_product_set_data($this->set['dfrps_id'], 'dfrps_update_phase' . $this->phase . '_first_pass');
      if ($first_pass['dfrps_update_phase' . $this->phase . '_first_pass'] == 0) {
        // This is the first pass for this phase.
        update_product_set_data($this->set['dfrps_id'], array('dfrps_update_phase' . $this->phase . '_first_pass' => 1));
        return TRUE;
      }
      return FALSE;
    }

    /**
     * Delete update phase for cron.
     */
    public function deleteFirstPasses() {
      for ($i = 1; $i <= 5; $i++) {
        $fields = array('dfrps_update_phase' . $i . '_first_pass' => FALSE);
        update_product_set_data($this->set['dfrps_id'], $fields);
      }
    }

    /**
     * Handle the error during cron.
     */
    public function handleError($data) {

      $class = $data['dfrapi_api_error']['class'];

      // These are the ERROR classes that trigger updates to be disabled.
      $error_classes = array(
        'DatafeedrBadRequestError',
        'DatafeedrAuthenticationError',
        'DatafeedrLimitExceededError',
        'DatafeedrQueryError',
      );

      $error_classes = apply_filters('dfrps_disable_updates_error_classes', $error_classes);

      if (in_array($class, $error_classes)) {
        $this->config['updates_enabled'] = 'disabled';
        variable_set('dfrps_configuration', $this->config);
        $this->updatesDisabledEmailUser($data);
      }
    }

    /**
     * Notify to user on error of cron.
     */
    public function updatesDisabledEmailUser($obj) {
      global $base_url;

      $params = array();
      $params['to'] = variable_get('site_mail', '');
      $params['subject'] = variable_get('site_name', '');
      $params['subject'] .= ': ';
      $params['subject'] .= t('Datafeedr API Message (Product Set Update Failed)');

      $params['message'] = "<p>";
      $params['message'] .= t('This is an automated message generated by');
      $params['message'] .= ": " . $base_url . "</p>";
      $params['message'] .= "<p>";
      $params['message'] .= t('An error occurred during the update of the');
      $params['message'] .= l($this->set['dfrps_title'], "/admin/config/datafeedr_product_sets/dfrps_edit/" . $this->set['dfrps_id']);
      $params['message'] .= "product set.</p>";

      if (isset($obj['dfrapi_api_error']['class'])) {

        // Have we exceeded the API request limit?
        if ($obj['dfrapi_api_error']['class'] == 'DatafeedrLimitExceededError') {

          $params['message'] .= "<p>";
          $params['message'] .= t('You have used !percent of your allocated Datafeedr API requests for this period.', array('!percent' => '<strong>100%</strong>'));
          $params['message'] .= "<u>";
          $params['message'] .= t('You are no longer able to query the Datafeedr API to get product information.');
          $params['message'] .= "</u></p>";
          $params['message'] .= "<p><strong>";
          $params['message'] .= t('What to do next?');
          $params['message'] .= "</strong></p>";
          $params['message'] .= "<p>";
          $params['message'] .= t('We strongly recommend that you upgrade to prevent your product information from becoming outdated.');
          $params['message'] .= "</p>";
          $params['message'] .= "<p>";
          $params['message'] .= l(t('UPGRADE NOW'), dfrapi_user_pages('change'), array(
            'attributes' => array(
              'target' => '_blank',
              'title' => t('UPGRADE NOW'),
            ),
            'external' => TRUE,
            'query' => array(
              'utm_source' => 'email',
              'utm_medium' => 'link',
              'utm_campaign' => 'updatesdisablednotice',
            ),
          )
          );
          $params['message'] .= "</p>";
          $params['message'] .= "<p>";
          $params['message'] .= t('Upgrading only takes a minute. You will have <strong>instant access</strong> to more API requests.');
          $params['message'] .= t('Any remaining credit for your current plan will be applied to your new plan.');
          $params['message'] .= "</p>";
          $params['message'] .= "<p>";
          $params['message'] .= t('You are under no obligation to upgrade. You may continue using your current plan for as long as you would like.');
          $params['message'] .= "</p>";
        }
        else {

          $params['message'] .= "<p>";
          $params['message'] .= t('The details of the error are below.');
          $params['message'] .= "</p>";
          $params['message'] .= "<tt>";
          $params['message'] .= "#################################################<br />";
          $params['message'] .= t('CLASS');
          $params['message'] .= ": " . $obj['dfrapi_api_error']['class'] . "<br />";
          $params['message'] .= t('CODE');
          $params['message'] .= ": " . $obj['dfrapi_api_error']['code'] . "<br />";
          $params['message'] .= t('MESSAGE');
          $params['message'] .= ": " . $obj['dfrapi_api_error']['msg'] . "<br />";
          if (!empty($obj['dfrapi_api_error']['params'])) {
            $query = dfrapi_display_api_request($obj['dfrapi_api_error']['params']);
            $params['message'] .= "<br />";
            $params['message'] .= t('QUERY');
            $params['message'] .= ":<br />" . $query . "<br />";
          }
          $params['message'] .= "#################################################";
          $params['message'] .= "</tt>";
        }
      }

      $params['message'] .= "<p>";
      $params['message'] .= t('In the meantime, all product updates have been disabled on your site. After you fix this problem you will need to');
      $params['message'] .= l(t('enable updates again'), '/admin/config/datafeedr_product_sets/product/configuration');
      $params['message'] .= "</p>";
      $params['message'] .= "<p>";
      $params['message'] .= t('If you have any questions about your account, please');
      $params['message'] .= l(t('contact us.'), DATAFEEDR_API_EMAIL_US_URL, array(
        'attributes' => array(
          'target' => '_blank',
          'title' => t('contact us'),
        ),
        'external' => TRUE,
        'query' => array(
          'utm_source' => 'email',
          'utm_medium' => 'link',
          'utm_campaign' => 'updatesdisablednotice',
        ),
      )
      );
      $params['message'] .= "</p>";
      $params['message'] .= "<p>";
      $params['message'] .= t('Thanks');
      $params['message'] .= ",<br />Eric &amp; Stefan<br />";
      $params['message'] .= t('The Datafeedr Team');
      $params['message'] .= "</p>";

      $params['message'] .= "<p>";
      $params['message'] .= l(t('Account Information'), '/admin/config/datafeedr/settings/account');
      $params['message'] .= ' | ';
      $params['message'] .= l(t('Upgrade Account'), dfrapi_user_pages('change'), array(
        'attributes' => array(
          'target' => '_blank',
          'title' => t('UPGRADE NOW'),
        ),
        'external' => TRUE,
        'query' => array(
          'utm_source' => 'email',
          'utm_medium' => 'link',
          'utm_campaign' => 'updatesdisablednotice',
        ),
      )
      );
      $params['message'] .= " | ";
      $params['message'] .= l(t('Enable Updates'), '/admin/config/datafeedr_product_sets/product/configuration');
      $params['message'] .= "</p>";

      // Send admin notification for product sets.
      $module = 'datafeedr_product_sets';
      $key = 'notice_product_set';
      $to = $params['to'];
      $from = $params['to'];
      $language = language_default();
      $params = array(
        'subject' => $params['subject'],
        'body' => $params['message'],
      );
      $send = TRUE;

      $mail = drupal_mail($module, $key, $to, $language, $params, $from, $send);
      if ($mail['result']) {
        return TRUE;
      }
      else {
        $error_msg = t('Failed to send the email in datafeedr product sets Module.');
        watchdog('datafeedr-email', $error_msg, array(), WATCHDOG_ALERT);
        return FALSE;
      }
    }

    /**
     * Phase 1, initialize update, set variables and update phase.
     */
    public function phase1() {

      $this->phase = 1;

      if ($this->isFirstPass()) {

        $fields = array(
          'dfrps_errors' => '',
          'dfrps_previous_update_info' => serialize($this->set),
          'dfrps_update_iteration' => 1,
          'dfrps_offset' => 1,
          'dfrps_last_update_time_started' => time(),
          'dfrps_last_update_time_completed' => 0,
          'dfrps_last_update_num_products_added' => 0,
          'dfrps_last_update_num_api_requests' => 0,
          'dfrps_last_update_num_products_deleted' => 0,
        );
        update_product_set_data($this->set['dfrps_id'], $fields);
      }
      else {
        $this->countIteration();
      }

      module_load_include('inc', 'datafeedr_ubercart_importer', '/includes/datafeedr_ubercart_importer');
      dfrpswc_unset_post_categories($this);

      // Check if preprocess is complete (detemined by importer scripts).
      $preprocess_complete = $this->preprocessCompleteCheck();

      // Move to phase 2 if all posts have been unset from their categories.
      if ($preprocess_complete) {
        // DROP TABLE to remove any remaining products from this table.
        // Products still in this table after a Product Set was moved to Trash.
        $this->dropTempProductTable();
        $this->createTempProductTable();
        return "ready";
      }

      return 'repeat';
    }

    /**
     * Phase 2, import saved search products into the options table.
     */
    public function phase2() {

      $this->phase = 2;

      $this->countIteration();

      $query = unserialize($this->set['dfrps_query']);
      // Remove this later $query = (isset($query[0])) ? $query[0] : array();.
      $query = (isset($query[0])) ? $query : array();
      // Check that a saved search exists and move on if it doesn't.
      if (empty($query)) {
        return 'skip';
      }

      // Get manually blocked product IDs.
      $blocked = get_product_set_data($this->set['dfrps_id'], 'dfrps_manually_blocked_ids');
      $manually_blocked = array_filter((array) unserialize($blocked['dfrps_manually_blocked_ids']));

      // Getting API settings data.
      $dfrapi_configuration = variable_get('dfrapi_configuration', array());

      // Check for sample data flag.
      if (isset($dfrapi_configuration['dfrapi_sample_data']) && $dfrapi_configuration['dfrapi_sample_data'] == 1) {

        $result = db_select('dfrapi_sample_products', 'data')
          ->fields('data')
          ->execute()
          ->fetchAssoc();
        $data = unserialize($result['data']);
        unset($data['products']);

        $dfrps_offset1 = get_product_set_data($this->set['dfrps_id'], 'dfrps_offset');
        if ($dfrps_offset1['dfrps_offset'] == 0) {
          $page = 0;
        }
        else {
          $page = $dfrps_offset1['dfrps_offset'] - 1;
        }

        $results = db_select('dfrapi_sample_products_data_01', 'sample_data')
          ->fields('sample_data', array('product_data'))
          ->range(($page * 100), 100)
          ->execute()
          ->fetchAll();
        foreach ($results as $result) {
          $data['products'][] = @unserialize($result->product_data);
        }
      }
      else {
        // Run query.
        $data = dfrapi_api_get_products_by_query($query, $this->config['num_products_per_update'], $this->set['dfrps_offset'], $manually_blocked);
      }

      // Update number of API requests.
      $last_api_request_num = get_product_set_data($this->set['dfrps_id'], 'dfrps_last_update_num_api_requests');
      $num_request_fields = array('dfrps_last_update_num_api_requests' => ($last_api_request_num['dfrps_last_update_num_api_requests'] + 1));
      update_product_set_data($this->set['dfrps_id'], $num_request_fields);

      // Handle errors & return.
      if (is_array($data) && array_key_exists('dfrapi_api_error', $data)) {
        $error_fields = array('dfrps_errors' => $data);
        update_product_set_data($this->set['dfrps_id'], $error_fields);
        $this->handleError($data);
        return 'repeat';
      }

      // Delete any errors that are currently being stored.
      $error_fields = array('dfrps_errors' => '');
      update_product_set_data($this->set['dfrps_id'], $error_fields);

      // If there are products, store product data in options table.
      if (isset($data['products']) && !empty($data['products'])) {

        foreach ($data['products'] as $product) {
          $this->insertTempProduct($product);
        }
      }
      else {

        // No products, update "Phase".
        $offset_fields = array('dfrps_offset' => 1);
        update_product_set_data($this->set['dfrps_id'], $offset_fields);
        return 'ready';
      }

      // All products in this batch have been imported into the options table.
      // Now update some meta stuff.
      $dfrps_offset = get_product_set_data($this->set['dfrps_id'], 'dfrps_offset');
      $dfrps_last_update_num_products_added = get_product_set_data($this->set['dfrps_id'], 'dfrps_last_update_num_products_added');
      $update_fields = array(
        'dfrps_offset' => ($dfrps_offset['dfrps_offset'] + 1),
        'dfrps_last_update_num_products_added' => ($dfrps_last_update_num_products_added['dfrps_last_update_num_products_added'] + count($data['products'])),
      );

      update_product_set_data($this->set['dfrps_id'], $update_fields);

      // If the number of products is less than the number of products per
      // update (that means subsequent queries won't return any more products).
      // Move to next phase so as not to incur 1 additional API request.
      if (count($data['products']) < ($this->config['num_products_per_api_request'])) {
        $offset_fields = array('dfrps_offset' => 1);
        update_product_set_data($this->set['dfrps_id'], $offset_fields);
        return 'ready';
      }

      return 'repeat';
    }

    /**
     * Phase 3, import single products into the options table.
     */
    public function phase3() {

      $this->phase = 3;

      $this->countIteration();

      // Get included IDs and remove any duplicates or empty values.
      $ids = get_product_set_data($this->set['dfrps_id'], 'dfrps_manually_added_ids');
      $ids = array_filter((array) unserialize($ids['dfrps_manually_added_ids']));

      $ids = array();
      // If no IDs, update phase and go to Phase 3.
      if (empty($ids)) {
        return 'skip';
      }

      // Query API.
      $offset = get_product_set_data($this->set['dfrps_id'], 'dfrps_offset');
      $data = dfrapi_api_get_products_by_id($ids, $this->config['num_products_per_update'], $offset['dfrps_offset']);

      // Update number of API requests.
      $dfrps_last_update_num_api_requests = get_product_set_data($this->set['dfrps_id'], 'dfrps_last_update_num_api_requests');
      $dfrps_last_update_num_api_requests_field = array('dfrps_last_update_num_api_requests' => ($dfrps_last_update_num_api_requests['dfrps_last_update_num_api_requests'] + 1));
      update_product_set_data($this->set['dfrps_id'], $dfrps_last_update_num_api_requests_field);

      // Handle errors & return.
      if (is_array($data) && array_key_exists('dfrapi_api_error', $data)) {
        $error_fields = array('dfrps_errors' => $data);
        update_product_set_data($this->set['dfrps_id'], $error_fields);
        $this->handleError($data);
        return 'repeat';
      }

      // Delete any errors that are currently being stored.
      $error_fields = array('dfrps_errors' => '');
      update_product_set_data($this->set['dfrps_id'], $error_fields);

      // If there are products, store product data in options table.
      if (isset($data['products']) && !empty($data['products'])) {
        foreach ($data['products'] as $product) {
          $this->insertTempProduct($product);
        }
      }

      // All products in this batch have been imported into the options table.
      // Now update some meta stuff.
      $dfrps_offset = get_product_set_data($this->set['dfrps_id'], 'dfrps_offset');
      $dfrps_last_update_num_products_added = get_product_set_data($this->set['dfrps_id'], 'dfrps_last_update_num_products_added');
      $update_fields = array(
        'dfrps_offset' => ($dfrps_offset['dfrps_offset'] + 1),
        'dfrps_last_update_num_products_added' => ($dfrps_last_update_num_products_added['dfrps_last_update_num_products_added'] + count($data['products'])),
      );
      update_product_set_data($this->set['dfrps_id'], $update_fields);

      /* If the number of product IDs is less than the number of products per
       * update (that means we've queried ALL product IDs).
       */
      // Move to next phase.
      $offset = ($this->config['num_products_per_api_request'] * ($dfrps_offset['dfrps_offset'] - 1));
      $length = $this->config['num_products_per_api_request'];
      $current_range_of_ids = array_slice($ids, $offset, $length);

      if ((count($current_range_of_ids) < $this->config['num_products_per_api_request'])) {
        $offset_fields = array('dfrps_offset' => 1);
        update_product_set_data($this->set['dfrps_id'], $offset_fields);
        return 'ready';
      }

      return 'repeat';
    }

    /**
     * Phase 4.
     *
     * Now we loop through our saved product data in the options table
     * and begin importing the products into the posts table.
     */
    public function phase4() {

      $this->phase = 4;

      $this->countIteration();

      // Get products to update.
      $products = $this->selectProductsForUpdate();

      // There are no products, move to phase 5.
      if (!is_array($products) || empty($products)) {
        return 'skip';
      }

      // Loop through products, importing and then removing from options array.
      foreach ($products as $product_data) {

        $product = unserialize($product_data->data);

        // The integration plugin(s) handle the group of products for this set.
        module_load_include('inc', 'datafeedr_ubercart_importer', '/includes/datafeedr_ubercart_importer');
        dfrpswc_do_products(array('products' => array($product)), $this->set);
        $this->deleteProductFromTable($product['_id']);
      }

      return 'repeat';
    }

    /**
     * Phase 5, clean up and finalize.
     */
    public function phase5() {
      $this->phase = 5;

      $this->countIteration();

      if ($this->isFirstPass()) {

      }

      dfrpswc_delete_stranded_products($this);

      // Check if preprocess is complete (detemined by importer scripts).
      $postprocess_complete = $this->postprocessCompleteCheck();

      if ($postprocess_complete) {
        $this->deleteFirstPasses();
        $next_update_time = dfrps_get_next_update_time();
        $update_fields = array(
          'dfrps_next_update_time' => $next_update_time,
          'dfrps_last_update_time_completed' => time(),
        );
        update_product_set_data($this->set['dfrps_id'], $update_fields);

        return 'complete';
      }

      return 'repeat';
    }

  }

  // Class DatafeedrProductSetsDfrpsUpdate.
}
