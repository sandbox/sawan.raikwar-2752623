<?php

/**
 * @file
 * This file handles all the product import functions.
 */

if (!class_exists('DatafeedrProductSetsDfrpsDelete')) {

  /**
   * Product Set Delete.
   */
  class DatafeedrProductSetsDfrpsDelete {

    /**
     * Constructor for the class.
     */
    public function __construct($post) {
      $this->action = 'delete';
      $this->set = $post;
      $this->config = $this->getConfiguration();
      $this->phase = $this->getPhase();
      $this->update();
    }

    /**
     * Get user's configuration settings.
     */
    public function getConfiguration() {
      return variable_get('dfrps_configuration', array());
    }

    /**
     * Get the current phase of the update.
     */
    public function getPhase() {
      $ps_interval = get_product_set_data($this->set['dfrps_id'], 'dfrps_update_phase');
      $phase = intval($ps_interval['dfrps_update_phase']);
      if (isset($phase)) {
        return intval($phase);
      }
      return 0;
    }

    /**
     * Run update.
     */
    public function update() {

      if ($this->phase == 0 || $this->phase == 1) {
        $this->phase1();
      }
      elseif ($this->phase == 2) {
        $this->phase2();
      }
    }

    /**
     * Check the preprocess complete or not.
     */
    public function preprocessCompleteCheck() {
      $complete = get_product_set_data($this->set['dfrps_id'], 'dfrps_preprocess_complete_product');
      return ($complete['dfrps_preprocess_complete_product'] == '') ? FALSE : TRUE;
    }

    /**
     * Check the postprocess complete or not.
     */
    public function postprocessCompleteCheck() {
      $complete = get_product_set_data($this->set['dfrps_id'], 'dfrps_postprocess_complete_product');
      return ($complete['dfrps_postprocess_complete_product'] == '') ? FALSE : TRUE;
    }

    /**
     * Check the first pass of the cron.
     */
    public function isFirstPass() {
      $first_pass = get_product_set_data($this->set['dfrps_id'], 'dfrps_update_phase' . $this->phase . '_first_pass');
      if (empty($first_pass)) {
        // This is the first pass for this phase.
        $fields = array('dfrps_update_phase' . $this->phase . '_first_pass' => TRUE);
        update_product_set_data($this->set['dfrps_id'], $fields);
        return TRUE;
      }
      return FALSE;
    }

    /**
     * Delete the phase passes.
     */
    public function deleteFirstPasses() {
      for ($i = 1; $i <= 2; $i++) {
        $fields = array('dfrps_update_phase' . $i . '_first_pass' => FALSE);
        update_product_set_data($this->set['dfrps_id'], $fields);
      }
    }

    /**
     * Phase 1, initialize update, set variables and update phase.
     */
    public function phase1() {

      $this->phase = 1;

      if ($this->isFirstPass()) {

        $fields = array(
          'dfrps_errors' => '',
          'dfrps_update_phase' => 1,
          'dfrps_previous_update_info' => serialize($this->set),
          'dfrps_offset' => 1,
          'dfrps_last_update_time_started' => time(),
          'dfrps_last_update_time_completed' => 0,
          'dfrps_last_update_num_products_added' => 0,
          'dfrps_last_update_num_api_requests' => 0,
          'dfrps_last_update_num_products_deleted' => 0,
        );
        update_product_set_data($this->set['dfrps_id'], $fields);
      }

      // Check if preprocess is complete (detemined by importer scripts).
      $preprocess_complete = $this->preprocessCompleteCheck();

      // Move to phase 2 if all posts have been unset from their categories.
      if ($preprocess_complete) {
        $this->phase = 2;
        update_product_set_data($this->set['dfrps_id'], array('dfrps_update_phase' => 2));
      }
    }

    /**
     * Phase 2, clean up and finalize.
     */
    public function phase2() {

      $this->phase = 2;

      if ($this->isFirstPass()) {
        update_product_set_data($this->set['dfrps_id'], array('dfrps_postprocess_complete_product' => 0));
      }

      // Check if preprocess is complete (detemined by importer scripts)
      $postprocess_complete = $this->postprocessCompleteCheck();

      if ($postprocess_complete) {
        $this->deleteFirstPasses();
        $this->phase = 0;
        $update_fields = array(
          'dfrps_next_update_time' => 3314430671,
          'dfrps_last_update_time_completed' => time(),
          'dfrps_update_phase' => 0,
        );
        update_product_set_data($this->set['dfrps_id'], $update_fields);
      }
    }

  }

  // Class DatafeedrProductSetsDfrpsDelete.
}
