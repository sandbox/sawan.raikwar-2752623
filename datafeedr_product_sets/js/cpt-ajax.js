/**
 * @file
 * Implement a ajax functionality.
 *
 * @see cpt-ajax.js.
 *
 * This file is used to implement ajax functionality.
 */

(function ($) {

  'use strict';

  /**
   * Get the path for Ajax request.
   */
  var pathArray = window.location.pathname.split('/');
  var newPathname = '';
  for (var i = 0; i < pathArray.length; i++) {
    newPathname += pathArray[i] + '/';
    if (pathArray[i] === 'datafeedr_product_sets') {
      break;
    }
  }
  var ajaxurl_ps = window.location.protocol + '//' + window.location.host + newPathname;

  /**
   * Save category selection (upon checking checkbox).
   */
  function save_category_selection(checklistElement) {

    // product.
    var cpt = checklistElement.attr('cpt');
    // product_catchecklist.
    var checklist = checklistElement.attr('id');
    var div = 'div_dfrps_tab_search';

    // http://stackoverflow.com/a/14474805/2489248
    var categoryIds = $('#' + checklist + ' input:checked').map(function () {
      return $(this).val();
    }).get();

    $('#' + div + ' .inside .messages').remove();
    $('.dfrps_category_selection_panel input').attr('disabled', true);
    $('.dfrps_saving_taxonomy').css('display', 'block');

    $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/update_taxonomy',
      dataType: 'json',
      data: {
        action: 'dfrps_ajax_update_taxonomy',
        dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
        psid: $('#ps_ID').val(),
        title: $('#edit-product-set-title').val(),
        query: $('form .filter:visible :input').serialize(),
        cids: categoryIds,
        cpt: cpt
      }
    }).done(function (response) {

      if (response.status === 'error') {

        // Display error message.
        $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');

        $('.dfrps_saving_taxonomy').css('display', 'none');
        $('.dfrps_category_selection_panel input').removeAttr('disabled');

      }
      else {
        $('#ps_ID').val(response.data.id);

        $('.dfrps_saving_taxonomy').css('display', 'none');
        $('.dfrps_category_selection_panel input').removeAttr('disabled');

        // Remove message about not having any search.
        $('#' + div + ' .dfrps_msg').remove();
      }
    });
  }

  /**
   * This is a helper function which gets the number for a given element.
   */
  function dfrpsGetElementsIntValue(element) {
    var el = $(element).first().text();
    var intval = parseInt(el.replace(',', ''), 10);
    if (isNaN(intval)) {
      return 0;
    }
    if (intval < 0) {
      return 0;
    }
    return intval;
  }

  /**
   * This is a helper function which flashes a tab when something happens in another tab.
   */
  function dfrpsHighlightFadeTab(element) {
    var current_bg_color = $(element).css('background-color');
    $(element).stop().animate({backgroundColor: 'yellow'}, 50).delay(50).animate({backgroundColor: current_bg_color}, 300);
  }

  /**
   * This is a helper function which adds 1 to an element.
   */
  function dfrpsAddOne(element, str) {
    var new_value = 0;
    if (str !== '' && typeof str !== 'undefined') {
      var el = $(element).first().text();
      var res = el.replace(str, '').replace(')', '').replace(',', '');
      var intval = parseInt(res, 10);
      if (isNaN(intval) || intval < 0) {
        intval = 0;
      }
      new_value = (intval + 1);
      if (new_value >= 0) {
        $(element).text(str + new_value + ')');
      }
      else {
        $(element).text(str + 0 + ')');
      }
    }
    else {
      var current_value = dfrpsGetElementsIntValue(element);
      new_value = (current_value + 1);
      if (new_value >= 0) {
        $(element).text(new_value);
      }
      else {
        $(element).text(0);
      }
    }
  }

  /**
   * This is a helper function which subtracts 1 from an element.
   */
  function dfrpsRemoveOne(element, str) {
    var new_value = 0;
    if (str !== '' && typeof str !== 'undefined') {
      var el = $(element).first().text();
      var res = el.replace(str, '').replace(')', '').replace(',', '');
      var intval = parseInt(res, 10);
      if (isNaN(intval) || intval < 0) {
        intval = 0;
      }
      new_value = (intval - 1);
      if (new_value >= 0) {
        $(element).text(str + new_value + ')');
      }
      else {
        $(element).text(str + 0 + ')');
      }
    }
    else {
      var current_value = dfrpsGetElementsIntValue(element);
      new_value = (current_value - 1);
      if (new_value >= 0) {
        $(element).text(new_value);
      }
      else {
        $(element).text(0);
      }
    }
  }

  /**
   * Get content for Saved Search tab.
   */
  function dfrpsGetSavedSearch() {
    var div = 'div_dfrps_tab_saved_search';

    // Remove message about not having any search.
    $('#' + div + ' .inside .messages').remove();
    $('#' + div + ' .inside .dfrapi_api_error').remove();

    $('#div_dfrps_tab_saved_search_results').html('<div class="dfrps_loading"></div>');

    return $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/get_productset',
      dataType: 'json',
      async: false,
      data: {
        action: 'dfrps_ajax_get_products',
        dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
        psid: $('#ps_ID').val(),
        context: div
      }
    }).done(function (response) {

      if (response.status === 'error') {

        // Display error message.
        if (response.flag === 1) {
          $('#div_dfrps_tab_saved_search_results').html(response.data.html);
        }
        else if (response.flag === 0) {
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
          $('#div_dfrps_tab_saved_search_results').html('');
        }

      }
      else {
        // Display error message.
        $('#' + div + ' .inside').prepend(response.data.html_msg);

        // Display saved search results.
        $('#div_dfrps_tab_saved_search_results').fadeOut(100).hide().fadeIn(100).show().html(response.data.html);

        $('#datafeedr-search-left .horizontal-tab-button-1 a strong').text('Saved Search (' + dfrpsGetElementsIntValue('#' + div + ' .dfrps_relevant_results') + ')');

      }
    });
  }

  /**
   * Get content for Included Products tab.
   */
  function dfrpsGetIncludedProducts() {

    var div = 'div_dfrps_tab_included';

    // Remove message about not having any search.
    $('#' + div + ' .inside .messages').remove();
    $('#' + div + ' .inside .dfrapi_api_error').remove();

    $('#div_dfrps_tab_included_search_results').html('<div class="dfrps_loading"></div>');

    return $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/get_productset',
      dataType: 'json',
      data: {
        action: 'dfrps_ajax_get_products',
        dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
        psid: $('#ps_ID').val(),
        context: div
      }
    }).done(function (response) {
      if (response.status === 'error') {

        // Display error message.
        if (response.flag === 1) {
          $('#div_dfrps_tab_included_search_results').html(response.data.html);
        }
        else if (response.flag === 0) {
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
          $('#div_dfrps_tab_included_search_results').html('');
        }
      }
      else {
        // Display error message.
        $('#' + div + ' .inside').prepend(response.data.html_msg);

        // Display saved search results.
        $('#div_dfrps_tab_included_search_results').fadeOut(100).hide().fadeIn(100).show().html(response.data.html);

        $('#datafeedr-search-left .horizontal-tab-button-2 a strong').text('Single Products (' + dfrpsGetElementsIntValue('#' + div + ' .dfrps_relevant_results') + ')');

        // Remove message about not having any search.
        $('#' + div + ' .dfrps_alert').remove();
      }
    });
  }

  /**
   * Get content for Blocked Products tab.
   */
  function dfrpsGetBlockedProducts() {

    var div = 'div_dfrps_tab_blocked';

    // Remove message about not having any search.
    $('#' + div + ' .inside .messages').remove();
    $('#' + div + ' .inside .dfrapi_api_error').remove();

    $('#div_dfrps_tab_blocked_search_results').html('<div class="dfrps_loading"></div>');

    return $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/get_productset',
      dataType: 'json',
      data: {
        action: 'dfrps_ajax_get_products',
        dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
        psid: $('#ps_ID').val(),
        context: div
      }
    }).done(function (response) {

      if (response.status === 'error') {

        // Display error message.
        if (response.flag === 1) {
          $('#div_dfrps_tab_blocked_search_results').html(response.data.html);
        }
        else if (response.flag === 0) {
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
          $('#div_dfrps_tab_blocked_search_results').html('');
        }
      }
      else {
        // Display error message.
        $('#' + div + ' .inside').prepend(response.data.html_msg);

        // Display saved search results.
        $('#div_dfrps_tab_blocked_search_results').fadeOut(100).hide().fadeIn(100).show().html(response.data.html);

        $('#datafeedr-search-left .horizontal-tab-button-3 a strong').text('Blocked Products (' + dfrpsGetElementsIntValue('#' + div + ' .dfrps_relevant_results') + ')');

        // Remove message about not having any search.
        $('#' + div + ' .dfrps_alert').remove();
      }
    });
  }

  function dfrps_init() {

    var icon_path = $('#icon_path').val();

    /**
     * Load blocked search results.
     */
    $('#datafeedr-product-sets-form').on('click', '#dfrps_cpt_blocked_search', function (e) {
      dfrpsGetBlockedProducts();
    });

    /**
     * Load included search results.
     */
    $('#datafeedr-product-sets-form').on('click', '#dfrps_cpt_included_search', function (e) {
      dfrpsGetIncludedProducts();
    });

    /**
     * Load saved search results.
     */
    $('#datafeedr-product-sets-form').on('click', '#dfrps_cpt_saved_search', function (e) {
      dfrpsGetSavedSearch();
    });

    /**
     * Load search results.
     */
    $('#datafeedr-product-sets-form').on('click', '#dfrps_cpt_search', function (e) {

      // Set variables.
      var div = $(this).closest('.psbox').attr('id');
      var page = $(this).attr('page');
      var searching_x_products = $('#searching_x_products').val();

      // Remove message about not having any search.
      $('#' + div + ' .inside .messages').remove();
      $('#' + div + ' .inside .dfrapi_api_error').remove();

      // Disable search and save search buttons.
      // Add as Saved Search.
      $('#dfrps_cpt_save_search').addClass('button-primary-disabled');
      $('#dfrps_cpt_search').addClass('button-disabled').val('Searching...');

      // Display 'loading...' image.
      $('#div_dfrps_tab_search_results').html('<div class="dfrps_loading"></div><div class="dfrps_searching_x_products">Searching ' + searching_x_products + ' products...</div>');

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/get_productset',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_get_products',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          query: $('form .filter:visible :input').serialize(),
          page: page,
          context: div
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          if (response.flag === 1) {
            $('#div_dfrps_tab_search_results').html(response.data.html);
          }
          else if (response.flag === 0) {
            $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
            $('#div_dfrps_tab_search_results').html('');
          }

          // Undisable search and save search buttons.
          $('#dfrps_cpt_search').removeClass('button-disabled').val('Search');
        }
        else {
          // Display error message.
          $('#' + div + ' .inside').prepend(response.data.html_msg);

          // Undisable search and save search buttons.
          $('#dfrps_cpt_save_search').removeClass('button-primary-disabled');
          $('#dfrps_cpt_search').removeClass('button-disabled').val('Search');

          // Display 'Save Search' button & 'view api request' link.
          $('#dfrps_save_update_search_actions').show();
          $('.dfrps_raw_query').show();

          // Display search results.
          $('#div_dfrps_tab_search_results').fadeIn(100).show().html(response.data.html);
        }
      });
      e.preventDefault();
    });

    /**
     * Pagination functionality.
     */
    $('#datafeedr-product-sets-form').on('click', '.dfrps_pager', function (e) {

      // Set variables.
      var page = $(this).attr('page');
      var div = $(this).closest('.psbox').attr('id');
      var element = '';
      if (div === 'div_dfrps_tab_search') {
        element = '#' + div + ' .inside #div_dfrps_tab_search_results';
      }
      else if (div === 'div_dfrps_tab_saved_search') {
        element = '#' + div + ' .inside #div_dfrps_tab_saved_search_results';
      }
      else if (div === 'div_dfrps_tab_included') {
        element = '#' + div + ' .inside #div_dfrps_tab_included_search_results';
      }
      else if (div === 'div_dfrps_tab_blocked') {
        element = '#' + div + ' .inside #div_dfrps_tab_blocked_search_results';
      }

      // Remove message about not having any search.
      $('#' + div + ' .inside .messages').remove();
      $('#' + div + ' .inside .dfrapi_api_error').remove();

      // Display 'loading...' image.
      $(element).html('<div class="dfrps_loading"></div>');

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/get_productset',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_get_products',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          page: page,
          context: div
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          if (response.flag === 1) {
            $(element).html(response.data.html);
          }
          else if (response.flag === 0) {
            $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
            $(element).html('');
          }
        }
        else {
          // Display error message.
          $('#' + div + ' .inside').prepend(response.data.html_msg);

          // Display product list.
          $(element).fadeIn(100).html(response.data.html);
        }
      });
      e.preventDefault();
    });

    /**
     * Add a Single Product.
     */
    $('#datafeedr-product-sets-form').on('click', '.dfrps_add_individual_product a', function (e) {

      // Set variables.
      var pid = $(this).attr('product-id');
      var div = $(this).closest('.psbox').attr('id');
      var product_block = '#product_' + pid + '_' + div;
      var cloned_product_block = $(product_block).clone();
      var ele_html = $(product_block + ' .action_links .dfrps_add_individual_product').html();

      // Display 'loading...' image.
      $(product_block + ' .action_links .dfrps_add_individual_product').html('<img src="' + icon_path + '/images/icons/loading.gif" />');

      $('#' + div + ' .inside .messages').remove();

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/action_links',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_add_individual_product',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          pid: pid
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');

          $(product_block + ' .action_links .dfrps_add_individual_product').html(ele_html);
        }
        else {
          // Flash 'Single Products' tab.
          dfrpsHighlightFadeTab('#datafeedr-search-left .horizontal-tab-button-2');

          // Update counters.
          dfrpsAddOne('#datafeedr-search-left .horizontal-tab-button-2 a strong', 'Single Products (');
          dfrpsAddOne('#div_dfrps_tab_included .dfrps_pager_end');
          dfrpsAddOne('#div_dfrps_tab_included .dfrps_relevant_results');

          // Remove 'add' icon and change to 'checkmark'.
          $(product_block + ' .action_links .dfrps_add_individual_product').remove();
          $(product_block + ' .action_links').prepend(response.data.html);

          // Push the cloned div to the Included page product list.
          $(cloned_product_block).attr('id', 'product_' + pid + '_div_dfrps_tab_included');
          $('#div_dfrps_tab_included .product_list').prepend(cloned_product_block);

          // Remove message about not having added any individual products.
          $('#' + div + ' .inside .messages').remove();
        }
      });
      e.preventDefault();
    });

    /**
     * Block a Product.
     */
    $('#datafeedr-product-sets-form').on('click', '.dfrps_block_individual_product a', function (e) {

      // Set variables.
      var pid = $(this).attr('product-id');
      var div = $(this).closest('.psbox').attr('id');
      var product_block = '#product_' + pid + '_' + div;
      var cloned_product_block = $(product_block).clone();

      var ele_html = $(product_block + ' .action_links .dfrps_block_individual_product').html();

      // Display 'loading...' image.
      $(product_block + ' .action_links .dfrps_block_individual_product').html('<img src="' + icon_path + '/images/icons/loading.gif" />');

      // Change color of the border of the table.
      $(product_block + ' table').css('border-color', '#d9534f');

      $('#' + div + ' .inside .messages').remove();

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/action_links',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_block_individual_product',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          pid: pid
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');

          $(product_block + ' .action_links .dfrps_block_individual_product').html(ele_html);

          // Change color of the border of the table.
          $(product_block + ' table').css('border-color', '#d0e3f0');
        }
        else {
          // Flash 'Blocked Products' tab.
          dfrpsHighlightFadeTab('#datafeedr-search-left .horizontal-tab-button-3');

          // Update counters.
          dfrpsAddOne('#datafeedr-search-left .horizontal-tab-button-3 a strong', 'Blocked Products (');
          dfrpsAddOne('#div_dfrps_tab_blocked .dfrps_pager_end');
          dfrpsAddOne('#div_dfrps_tab_blocked .dfrps_relevant_results');

          dfrpsRemoveOne('#datafeedr-search-left .horizontal-tab-button-1 a strong', 'Saved Search (');
          dfrpsRemoveOne('#div_dfrps_tab_search .dfrps_pager_end');
          dfrpsRemoveOne('#div_dfrps_tab_search .dfrps_relevant_results');
          dfrpsRemoveOne('#div_dfrps_tab_saved_search .dfrps_pager_end');
          dfrpsRemoveOne('#div_dfrps_tab_saved_search .dfrps_relevant_results');

          // Change style of the border of the table slide up this product block.
          $(product_block + ' table').css('border-style', 'dotted');
          $(product_block).slideUp();

          // Remove 'add' icon and change to 'checkmark'.
          $(product_block + ' .action_links .dfrps_add_individual_product').remove();
          $(product_block + ' .action_links').prepend(response.data.html);

          // Slide up on Search Results & Saved Search page, too.
          $('#product_' + pid + '_div_dfrps_tab_search').slideUp();
          $('#product_' + pid + '_div_dfrps_tab_saved_search').slideUp();

          // Push the cloned div to the Blocked Products list.
          $(cloned_product_block).attr('id', 'product_' + pid + '_div_dfrps_tab_blocked');
          $('#div_dfrps_tab_blocked .product_list').prepend(cloned_product_block);

          // Remove message about not having added any blocked products.
          $('#' + div + ' .inside .messages').remove();
        }
      });
      e.preventDefault();
    });

    /**
     * Save search.
     */
    $('#datafeedr-product-sets-form').on('click', '#dfrps_cpt_save_search', function (e) {

      // Set variables.
      var div = $(this).closest('.psbox').attr('id');

      // Remove message about not having any search.
      $('#' + div + ' .inside .messages').remove();

      // Check product set title.
      if ($('input[name=product_set_title]').val() === '') {
        $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>Please enter the product set title.</div>');
        return false;
      }

      var num_products = dfrpsGetElementsIntValue('#div_dfrps_tab_search .inside .dfrps_relevant_results');
      var cloned_pagination_results_first = $('#div_dfrps_tab_search_results .dfrps_pagination').first().clone();
      var cloned_pagination_results_last = $('#div_dfrps_tab_search_results .dfrps_pagination').last().clone();

      // Disable 'save' button.
      $('#dfrps_cpt_save_search').addClass('button-primary-disabled').val('Saving...');

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/save_query',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_save_query',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          title: $('#edit-product-set-title').val(),
          query: $('form .filter:visible :input').serialize(),
          num_products: num_products
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');

          $('#dfrps_cpt_save_search').addClass('button-primary-disabled').val('Add as Saved Search');
        }
        else {
          $('#ps_ID').val(response.data.id);

          // Flash 'Saved Search' tab & update count.
          dfrpsHighlightFadeTab('#datafeedr-search-left .horizontal-tab-button-1');
          $('#datafeedr-search-left .horizontal-tab-button-1 a strong').text('Saved Search (' + num_products + ')');

          // Remove any pagination and product list that already exists under Saved Search tab.
          $('#div_dfrps_tab_saved_search .dfrps_pagination').remove();
          $('#div_dfrps_tab_saved_search .product_list').empty();

          // Loop through each product block and at it to Saved Search tab area.
          $('#div_dfrps_tab_search_results > .product_list > div').each(function () {
            var cloned_product_block = $(this).clone();
            var pid_class = $(this).attr('class').split(' ')[1];
            $(cloned_product_block).attr('id', pid_class + '_div_dfrps_tab_saved_search');
            $('#div_dfrps_tab_saved_search .product_list').append(cloned_product_block);
          });

          // show() product_list in case it was hidden.
          $('#div_dfrps_tab_saved_search .product_list').show();

          // Add pagination.
          $('#div_dfrps_tab_saved_search .product_list').before(cloned_pagination_results_first);
          $('#div_dfrps_tab_saved_search .product_list').after(cloned_pagination_results_last);

          // Update [Save Search] button with 'Update Saved Search' text.
          $('#dfrps_cpt_save_search').val(response.data.html);
        }
      });

      e.preventDefault();
    });

    /**
     * Delete Saved Search.
     */
    $('#datafeedr-product-sets-form').on('click', '.dfrps_delete_saved_search', function (e) {
      var div = $(this).closest('.psbox').attr('id');

      // Remove message about not having any saved search.
      $('#' + div + ' .messages').remove();

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/delete_saved_search',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_delete_saved_search',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val()
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          $('#' + div + ' .inside').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
        }
        else {

          $('#div_dfrps_tab_saved_search .dfrps_pagination').fadeOut();
          $('#div_dfrps_tab_saved_search .product_list').fadeOut();

          // Update Saved Search Tab count with 0.
          $('#datafeedr-search-left .horizontal-tab-button-1 a strong').text('Saved Search (0)');

          // Remove message about not having any saved search.
          $('#' + div + ' .messages').remove();

          // Display 'success' message.
          $('#' + div + ' .inside').prepend('<div class="messages status"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
        }
      });

      e.preventDefault();
    });

    /**
     * Remove product which was added individually.
     */
    $('#datafeedr-product-sets-form').on('click', '.dfrps_remove_individual_product a', function (e) {

      // Set variables.
      var pid = $(this).attr('product-id');
      var div = $(this).closest('.psbox').attr('id');
      var product_block = '#product_' + pid + '_' + div;

      // Display 'loading...' image.
      $(product_block + ' .action_links .dfrps_remove_individual_product').html('<img src="' + icon_path + '/images/icons/loading.gif" />');

      // Change color of the border of the table.
      $(product_block + ' table').css('border-color', '#d9534f');

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/action_links',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_remove_individual_product',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          pid: pid
        }
      }).done(function (response) {

        if (response.status === 'error') {

          // Display error message.
          $('#' + div + ' .inside').prepend('<div class="messages status"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');

        }
        else {
          // Flash 'Single Products' tab.
          dfrpsHighlightFadeTab('#datafeedr-search-left .horizontal-tab-button-2');

          // Update counters.
          dfrpsRemoveOne('#div_dfrps_tab_included .dfrps_pager_end');
          dfrpsRemoveOne('#div_dfrps_tab_included .dfrps_relevant_results');
          dfrpsRemoveOne('#datafeedr-search-left .horizontal-tab-button-2 a strong', 'Single Products (');

          // Change style of the border of the table slide up this product block.
          $(product_block + ' table').css('border-style', 'dotted');
          $(product_block).slideUp();

          // Make it 'addable' again on the Search tab page.
          $('#product_' + pid + '_div_dfrps_tab_search .action_links .dfrps_product_already_included').remove();
          $('#product_' + pid + '_div_dfrps_tab_search .action_links').prepend('<div class="dfrps_add_individual_product"><a href="#" product-id="' + pid + '" title="Add this product to this Product Set."><img src="' + icon_path + '/images/icons/plus.png" /></a></div>');

          // Remove message about not having any saved search.
          $('#' + div + ' .inside .messages').remove();
        }
      });

      e.preventDefault();
    });

    /**
     * Unblock blocked product.
     */
    $('#datafeedr-product-sets-form').on('click', '.dfrps_unblock_individual_product a', function (e) {

      // Set variables.
      var pid = $(this).attr('product-id');
      var div = $(this).closest('.psbox').attr('id');
      var product_block = '#product_' + pid + '_' + div;

      // Display 'loading...' image.
      $(product_block + ' .action_links .dfrps_unblock_individual_product').html('<img src="' + icon_path + '/images/icons/loading.gif" />');

      // Change color of the border of the table.
      $(product_block + ' table').css('border-color', '#d9534f');

      $.ajax({
        type: 'POST',
        url: ajaxurl_ps + 'ajax/action_links',
        dataType: 'json',
        data: {
          action: 'dfrps_ajax_unblock_individual_product',
          dfrps_security: $("#datafeedr-product-sets-form input[name*='form_token']").val(),
          psid: $('#ps_ID').val(),
          pid: pid
        }
      }).done(function (response) {
        if (response.status === 'error') {

          // Display error message.
          $('#' + div + ' .inside').prepend('<div class="messages status"><h2 class="element-invisible">Error message</h2>' + response.data.html + '</div>');
        }
        else {
          // Flash 'Single Products' tab.
          dfrpsHighlightFadeTab('#datafeedr-search-left .horizontal-tab-button-3');

          // Update counters.
          dfrpsRemoveOne('#div_dfrps_tab_blocked .dfrps_pager_end');
          dfrpsRemoveOne('#div_dfrps_tab_blocked .dfrps_relevant_results');
          dfrpsRemoveOne('#datafeedr-search-left .horizontal-tab-button-3 a strong', 'Blocked Products (');

          // Change style of the border of the table slide up this product block.
          $(product_block + ' table').css('border-style', 'dotted');
          $(product_block).slideUp();

          // Remove message about not having any saved search.
          $('#' + div + ' .inside .messages').remove();
        }
      });

      e.preventDefault();
    });

    $('#datafeedr-product-sets-form').on('click', '.selectit', function (e) {
      save_category_selection($(this).closest('.categorychecklist'));
    });
  }
  if (window.addEventListener) {
    window.addEventListener('load', dfrps_init, false);
  }
  else if (window.attachEvent) {
    window.attachEvent('onload', dfrps_init);
  }
})(jQuery);
