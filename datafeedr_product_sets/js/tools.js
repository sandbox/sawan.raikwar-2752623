/**
 * @file
 * Implement a tools form.
 *
 * @see tools.inc.
 *
 * This file is used to implement the tools option.
 */

jQuery(function ($) {

  'use strict';

  var pathArray = window.location.pathname.split('/');
  var newPathname = '';
  for (var i = 0; i < pathArray.length; i++) {
    newPathname += pathArray[i] + '/';
    if (pathArray[i] === 'datafeedr_product_sets') {
      break;
    }
  }
  var ajaxurl_ps = window.location.protocol + '//' + window.location.host + newPathname;

  /**
   * Fix missing images.
   */
  $('#dfrps_fix_missing_images').on('click', function (e) {
    $('#dfrps_fix_missing_images_result').hide();
    $('#dfrps_fix_missing_images').text('Processing...').addClass('button-disabled');
    $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/fix_missing_images',
      data: {
        action: 'dfrps_ajax_fix_missing_images',
        dfrps_security: $("#datafeedr-product-sets-tools-form input[name*='form_token']").val()
      }
    }).done(function (html) {
      $('#dfrps_fix_missing_images').text('Fix Missing Images').removeClass('button-disabled');
      $('#dfrps_fix_missing_images_result').show().html(html);
    });
    e.preventDefault();
  });

  /**
   * Bulk image import start.
   */
  $('#dfrps_start_batch_image_import').on('click', function (e) {
    $('#dfrps_start_batch_image_import_result').show().html('<div>Starting...</div>');
    $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/start_batch_image_import',
      data: {
        action: 'dfrps_ajax_start_batch_image_import',
        dfrps_security: $("#datafeedr-product-sets-tools-form input[name*='form_token']").val()
      }
    }).done(function (html) {
      $('#dfrps_stop_batch_image_import').show().html('Stop');
      reload_batch_import();
    });
    e.preventDefault();
  });

  /**
   * Bulk image import stop.
   */
  $('#dfrps_stop_batch_image_import').on('click', function (e) {
    $('#dfrps_stop_batch_image_import').text('Stopping...').addClass('button-disabled');
    $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/stop_batch_image_import',
      data: {
        action: 'dfrps_ajax_stop_batch_image_import',
        dfrps_security: $("#datafeedr-product-sets-tools-form input[name*='form_token']").val()
      }
    }).done(function (html) {
      $('#dfrps_stop_batch_image_import').text('Stop').removeClass('button-disabled').hide();
    });
    e.preventDefault();
  });

  // Bulk image import.
  var reload_batch_import = function () {
    return $.ajax({
      type: 'POST',
      url: ajaxurl_ps + 'ajax/batch_import_images',
      cache: false,
      data: {
        action: 'dfrps_ajax_batch_import_images',
        dfrps_security: $("#datafeedr-product-sets-tools-form input[name*='form_token']").val()
      },
      success: function (data) {
        $('#dfrps_start_batch_image_import_result').prepend(data);
        if (data !== 'Stopped' && data !== 'Complete') {
          reload_batch_import();
        }
      }
    });
  };
});
