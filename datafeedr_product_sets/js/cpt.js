/**
 * @file
 * Implement a ajax functionality.
 *
 * @see cpt.js.
 *
 * This file is used to implement ajax functionality.
 */

jQuery(function ($) {

  'use strict';

  // Tabs on add/edit Product Set page (Search, Included Products & Excluded Products).
  $('#dfrps_cpt_tabs a').click(function (e) {
    var id = $(this).attr('id');
    // Remove 'active' class from all tabs. & Make background the right color.
    $('.nav-tab').removeClass('nav-tab-active').css('background-color', '#E4E4E4');
    // Hide all meta boxes. We'll show 1 later.
    $('.dfrps_meta_box').hide();
    // Make the clicked tab active. & Make background the right color.
    $(this).addClass('nav-tab-active').css('background-color', '#F1F1F1');
    // Don't hide the tabs div.
    $('#div_dfrps_tabs').show();
    // Un-hide the selected tab's div.
    $('#div_dfrps_' + id).fadeIn(200);
    e.preventDefault();
  });

  $('#dfrps_search_instructions_toggle').on('click', function (e) {
    $('#dfrps_search_instructions').slideToggle();
    e.preventDefault();
  });

  // More info for a product in search results.
  $('.datafeedr-productset_admin').on('click', '.more_info', function (e) {
    var table = $(this).closest('.product_block').attr('id');
    var table_id = '#' + table + ' .more_info_row';
    $(table_id).slideToggle();
    e.preventDefault();
  });

  $('#datafeedr-product-sets-form').on('click', '#dfrps_view_raw_query', function (e) {
    $('#dfrps_raw_api_query').slideToggle();
    e.preventDefault();
  });

  function dfrpsStyleUpdatingRow() {
    var row = $('.wp-list-table').find('.dfrps_currently_updating').closest('tr').attr('id');
    if (row !== 'undefined') {
      $('tr#' + row).addClass('dfrps_updating_row');
    }
  }
  dfrpsStyleUpdatingRow();
});
