<?php

/**
 * @file
 * This file is used to display configuration page for this module.
 */

/**
 * Implements ubercart importer config.
 */
function datafeedr_ubercart_importer_config_form($form, &$form_state) {

  $dfrpsuc_options = variable_get('dfrpsuc_options', array());

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Options - Datafeedr Ubercart Importer'),
  );

  // Section: General Settings.
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['general_settings']['button_text'] = array(
    '#type' => 'textfield',
    '#title' => 'Button Text',
    '#default_value' => isset($dfrpsuc_options['button_text']) ? $dfrpsuc_options['button_text'] : '',
    '#description' => t("The text on the button which links to the merchant's website."),
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );
  return $form;
}

/**
 * Save config changes of ubercart importer config.
 */
function datafeedr_ubercart_importer_config_form_submit($form, &$form_state) {

  // Storing the values.
  $dfrpsuc_options = array('button_text' => $form_state['values']['button_text']);

  variable_set('dfrpsuc_options', $dfrpsuc_options);
  drupal_set_message(t('Configuration successfully updated.'));
  drupal_goto('admin/config/datafeedr_ubercart_importer');
}
