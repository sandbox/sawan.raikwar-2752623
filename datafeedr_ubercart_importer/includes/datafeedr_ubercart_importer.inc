<?php

/**
 * @file
 * This file implements all necessary functions to import product.
 */

// Define constants.
define('DFRPSWC_DOMAIN', 'dfrpswc_integration');
define('DFRPSWC_POST_TYPE', 'product');
define('DFRPSWC_TAXONOMY', 'product_cat');

/**
 * Return TRUE if finished unsetting categories.
 *
 * Return FALSE if not finished.
 *
 * This unsets products from their categories before updating products.
 * Why?
 *
 * We need to remove all products which were imported via a product set
 * from the categories they were added to when they were imported
 * so that at the end of the update,
 * If these products weren't re-imported
 * during the update, the post/product's category information (for this
 * set) will no longer be available so that if this post/product was
 * added via another Product Set, only that Product Set's category IDs
 * will be attributed to this post/product
 * This processes batches at a time as this is a server/time intensive process.
 */
function dfrpswc_unset_post_categories($obj) {
  // Get posts to unset categories for.
  $nodes = variable_get('unset_post_categories_' . DFRPSWC_POST_TYPE . '_for_set_' . $obj->set['dfrps_id']);

  /* If $nodes does not exist (ie. FALSE) and
   * If the the status of preprocess is FALSE then
   * Get all post IDs (as an array) set by this Product Set
   * and set them to 'unset_post_categories_{product}_for_set_XXX'
   * in the options table. That way we don't have to run the
   * dfrps_get_all_post_ids_by_set_id() query again and we can
   * incrementally remove post IDs from this array of IDs as we
   * get ready for updating.
   */
  if (!$nodes && !dfrpswc_process_complete('preprocess', $obj->set['dfrps_id'])) {
    $nodes = dfrps_get_all_post_ids_by_set_id($obj->set['dfrps_id']);
    variable_set('unset_post_categories_' . DFRPSWC_POST_TYPE . '_for_set_' . $obj->set['dfrps_id'], $nodes);
  }
  /*
   * If $nodes contains post IDs, we will grab the first X number of.
   * IDs from the array (where X is "preprocess_maximum") and remove.
   * The category IDs from these posts where the category IDs match.
   * the IDs which were selected when creating the Product Set.
   * After the category ID is removed, we also remove "dfrps_product_set_id".
   * so that we can find this product later and delete it if it is no.
   * Longer associated with a Product Set.
   * Lastly, we remove each processed post ID from the $nodes array.
   */
  if (is_array($nodes) && !empty($nodes)) {
    $config = variable_get('dfrps_configuration', array());
    $ids = array_slice($nodes, 0, $config['preprocess_maximum']);
    foreach ($ids as $id) {
      dfrps_remove_category_ids_from_post($id, $obj->set, DFRPSWC_POST_TYPE, DFRPSWC_TAXONOMY);

      // Update "product_set_id" field value to 0.
      db_update('field_data_dfrps_product_set_id')
        ->field(array('dfrps_product_set_id_value' => 0))
        ->condition('entity_id', $id, '=')
        ->execute();

      // Update product set field table value to null.
      if (($key = array_search($id, $nodes)) !== FALSE) {
        unset($nodes[$key]);
      }
    }
  }
  /*
   * Now we need to check if there are more post IDs to process.
   * If $nodes is empty, then we are done with the "preprocess" stage.
   * - Set "_dfrps_preprocess_complete_" to TRUE.
   *  - Delete the 'unset_post_categories_{product}_for_set_XXX' so.
   * no longer attempt to process it.
   * If $nodes is NOT empty,
   * We update 'unset_post_categories_product_for_set_XXX'.
   * With our reduced $nodes array and let the "preprocess" run again.
   */
  if (empty($nodes)) {

    update_product_set_data($obj->set['dfrps_id'], array('dfrps_preprocess_complete_' . DFRPSWC_POST_TYPE => 1));
    variable_del('unset_post_categories_product_for_set_' . $obj->set['dfrps_id']);
  }
  else {
    variable_set('unset_post_categories_product_for_set_' . $obj->set['dfrps_id'], $nodes);
  }
}

/**
 * Adds product in the content_type you are inserting products into.
 */
function dfrpswc_do_products($data, $set) {

  // Check if there are products available.
  if (!isset($data['products']) || empty($data['products'])) {
    return;
  }

  // Loop through products.
  foreach ($data['products'] as $product) {

    // Get post if it already exists.
    $existing_post = dfrps_get_existing_post($product, $set);

    // Determine what to do based on if post exists or not.
    if ($existing_post && $existing_post['type'] == DFRPSWC_POST_TYPE) {
      $action = 'update';
      dfrpswc_insert_update_post($existing_post, $product, $set, $action);
    }
    else {
      $existing_post['nid'] = 0;
      $action = 'insert';
      dfrpswc_insert_update_post($existing_post, $product, $set, $action);
    }
  }
}

/**
 * This insert OR updates a post.
 *
 * This should return a FULL $post object in ARRAY_A format.
 */
function dfrpswc_insert_update_post($existing_post, $product, $set, $action) {

  if ($existing_post['nid'] == 0) {
    $node = new stdClass();
    $node->type = "product";
    // Sets some defaults. Invokes hook_prepare() and hook_node_prepare().
    node_object_prepare($node);
    $node->is_new = TRUE;
    $node->uid = 1;
    // (1 or 0): promoted to front page.
    $node->promote = 1;
    // 0 = comments disabled, 1 = read only, 2 = read/write.
    $node->comment = 0;
  }
  elseif ($existing_post['nid'] != 0) {
    $node = node_load($existing_post['nid']);
    $node->is_new = FALSE;
  }
  $node->title = @$product['name'];
  // Or e.g. 'en' if locale is enabled.
  $node->language = LANGUAGE_NONE;
  // (1 or 0): published or not.
  $node->status = 1;
  // SKU.
  $node->model = @$product['_id'];
  $node->list_price = (isset($product['price'])) ? dfrps_int_to_price($product['price']) : 0.00000;
  $node->cost = (isset($product['price'])) ? dfrps_int_to_price($product['price']) : 0.00000;
  $node->sell_price = (isset($product['saleprice'])) ? dfrps_int_to_price($product['saleprice']) : 0.00000;
  $node->weight = 0;
  $node->weight_units = 'lb';
  $node->length = 0;
  $node->width = 0;
  $node->height = 0;
  $node->length_units = 'in';
  $node->pkg_qty = 1;
  $node->default_qty = 1;
  $node->price = (isset($product['price'])) ? dfrps_int_to_price($product['price']) : 0.00000;
  if ($existing_post['nid'] == 0) {
    $node->created = time();
  }
  else {
    $node->created = $node->created;
  }
  $node->body[LANGUAGE_NONE][0]['value'] = @$product['description'];
  $node->body[LANGUAGE_NONE][0]['summary'] = @$product['shortdescription'];
  $node->body[LANGUAGE_NONE][0]['format'] = 'full_html';
  $node->dfrps_product_url[LANGUAGE_NONE][0]['value'] = @$product['url'];
  $node->dfrps_is_dfrps_product[LANGUAGE_NONE][0]['value'] = 1;
  $node->dfrps_is_dfrpsuc_product[LANGUAGE_NONE][0]['value'] = 1;
  $node->dfrps_product_id[LANGUAGE_NONE][0]['value'] = $product['_id'];
  // This stores all info about the product in 1 array.
  $node->dfrps_product[LANGUAGE_NONE][0]['value'] = serialize($product);
  $node->dfrps_product_set_id[LANGUAGE_NONE][0]['value'] = $set['dfrps_id'];
  $node->dfrps_product_check_image[LANGUAGE_NONE][0]['value'] = 0;
  // Set featured image url (if there's an image).
  if (@$product['image'] != '') {
    $node->dfrps_featured_image_url[LANGUAGE_NONE][0]['value'] = $product['image'];
    // I have used public folder to store images using image field settings.
    $file_info = system_retrieve_file($product['image'], 'public://', TRUE, FILE_EXISTS_REPLACE);
    if (isset($file_info->fid)) {
      // Assign fid.
      $node->uc_product_image[LANGUAGE_NONE]['0']['fid'] = $file_info->fid;
    }
  }
  elseif (@$product['thumbnail'] != '') {
    $node->dfrps_featured_image_url = $product['thumbnail'];
    // I have used public folder to store images using image field settings.
    $file_info = system_retrieve_file($product['thumbnail'], 'public://', TRUE, FILE_EXISTS_REPLACE);
    if ($file_info->fid) {
      // Assign fid.
      $node->uc_product_image[LANGUAGE_NONE]['0']['fid'] = $file_info->fid;
    }
  }

  // Get the IDs of the categories this product is associated with.
  $ids = array();
  $cat = get_product_set_data($set['dfrps_id'], 'dfrps_categories');
  $cat_product = @unserialize($cat['dfrps_categories']);
  if (isset($cat_product['product']) && !empty($cat_product['product'])) {
    foreach ($cat_product['product'] as $id) {
      $ids[]['tid'] = $id;
    }
  }
  $node->taxonomy_catalog[$node->language] = $ids;

  if (dfrpswc_is_dfrpsuc_product($existing_post['nid'])) {
    $post_id = $existing_post['nid'];
    $query = db_select('field_data_dfrps_product', 'uc')
      ->fields('uc', array('dfrps_product_value'))
      ->condition('uc.entity_id', $post_id, '=');
    $result = $query->execute();
    $node_product = $result->fetchAssoc();
    $external_link = dfrapi_url($node_product);
  }
  elseif ($existing_post['nid'] == 0) {
    $external_link = dfrapi_url($product);
  }
  $node->dfrps_product_url[$node->language][0]['value'] = @$external_link;
  if ($existing_post['nid'] == 0) {
    // Prepare node for saving.
    $node = node_submit($node);
  }

  node_save($node);
  return $node;
}

/**
 * This is clean up after the update is finished.
 *
 * Here we will:Delete (move to Trash) all products which
 * were "stranded" after the update.
 *
 * Strandad means they no longer have a Product Set ID associated with them.
 */
function dfrpswc_delete_stranded_products($obj) {

  $dfrps_configuration = variable_get('dfrps_configuration', array());

  // Should we even delete missing products?
  if (isset($dfrps_configuration['delete_missing_products']) && ($dfrps_configuration['delete_missing_products'] == 0)) {
    update_product_set_data($obj->set['dfrps_id'], array('dfrps_postprocess_complete_product' => 1));
    return;
  }

  // If trashable posts are already set for this Set['ID'], then just return.
  $trashable_posts = variable_get('trashable_posts_for_set_' . $obj->set['dfrps_id']);

  // If we've not run the SQL to get trashable posts, do so now.
  if (!$trashable_posts && !dfrpswc_process_complete('postprocess', $obj->set['dfrps_id'])) {

    $nodeq = db_select('node', 'n');
    $nodeq->leftJoin('field_data_dfrps_is_dfrps_product', 'ips', 'ips.entity_id = n.nid');
    $nodeq->leftJoin('field_data_dfrps_product_set_id', 'ps', 'ps.entity_id = n.nid');
    $nodeq->fields('n', array('nid'));
    $nodeq->condition('ips.dfrps_is_dfrps_product_value', '', '!=');
    $nodeq->condition('ps.dfrps_product_set_id_value', 0, '=');
    $nodeq->condition('n.status', 1, '=');
    $results = $nodeq->execute();
    $results->fetchAll();

    $post_ids = array();
    foreach ($results as $post) {
      $post_ids[] = $post['nid'];
    }

    if (empty($post_ids)) {
      update_product_set_data($obj->set['dfrps_id'], array('dfrps_postprocess_complete_' . DFRPSWC_POST_TYPE => 1));
      update_product_set_data($obj->set['dfrps_id'], array('dfrps_last_update_num_products_deleted' => count($post_ids)));
      return;
    }

    $trashable_posts = $post_ids;
    variable_set('trashable_posts_for_set_' . $obj->set['dfrps_id'], $post_ids);
    update_product_set_data($obj->set['dfrps_id'], array('dfrps_last_update_num_products_deleted' => count($post_ids)));
  }

  if (is_array($trashable_posts) && !empty($trashable_posts)) {
    $ids = array_slice($trashable_posts, 0, $dfrps_configuration['postprocess_maximum']);
    foreach ($ids as $id) {

      node_delete($id);
      if (($key = array_search($id, $trashable_posts)) !== FALSE) {
        unset($trashable_posts[$key]);
      }
    }
  }

  if (empty($trashable_posts)) {
    variable_del('trashable_posts_for_set_' . $obj->set['dfrps_id']);
    update_product_set_data($obj->set['dfrps_id'], array('dfrps_postprocess_complete_product' => 1));
    return;
  }
  else {
    variable_set('trashable_posts_for_set_' . $obj->set['dfrps_id'], $trashable_posts);
    return;
  }
}

/**
 * Returns true if product was imported by this module.
 */
function dfrpswc_is_dfrpsuc_product($product_id) {

  $query = db_select('field_data_dfrps_is_dfrpsuc_product', 'uc')
    ->fields('uc', array('dfrps_is_dfrpsuc_product_value'))
    ->condition('uc.entity_id', $product_id, '=');
  $result = $query->execute();
  $num = $result->rowCount();
  return ($num == 0) ? FALSE : TRUE;
}

/**
 * A function to determine either the preprocess or postprocess are complete.
 *
 * Returns true if complete, FALSE if not complete.
 */
function dfrpswc_process_complete($process, $set_id) {
  $status = get_product_set_data($set_id, 'dfrps_' . $process . '_complete_product');
  return $status['dfrps_' . $process . '_complete_product'];
}
