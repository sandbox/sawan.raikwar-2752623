=== Datafeedr Ubercart Importer ===

Contributors: datafeedr.com, cognizant.com
Tags: adrecord, adtraction, affiliate marketing, affiliate window, affiliate4you, affilinet, avangate, avantlink, amazon local, belboon, cj, clickbank, commission factory, commission junction, csv, daisycon, data feed, datafeed, datafeeder, datafeedr, dfrapi, dfrps, dfrpsimporter, dfrpswc, dgm, flipkart, impact radius, import, linkconnector, linkshare, onenetworkdirect, paid on results, partner-ads, pepperjam, popshops, premiumpress, mycommerce, revresponse, sellfire, shareasale, shopperpress, superclix, tradedoubler, tradetracker, webgains, ubercart, wpallimport, xml, zanox
Requires at least: 7.x

Import products from the Datafeedr API into your Ubercart store.

== Description ==

Search, select & import affiliate products into your Ubercart website and display those products using the most popular eCommerce platform, Ubercart.

No programming required. 

Simply use our powerful search form to find the products you want to promote, then click to import those products directly into your Ubercart store.

= What is Ubercart? =
Ubercart is a free eCommerce module that helps you sell anything on your Drupal website, even affiliate products.

= Features =
35 reasons why you'll LOVE our module!

* **270+ Million Products** - Access our massive database of affiliate products.

* **12,000+ Merchants** - Select from thousands of internationally recognizable brands and boutique retailers.

* **30+ Affiliate Networks** - Choose from the largest affiliate networks. (See full list below.)

* **19 Countries** - Import products from Australia, Belgium, Brazil, Canada, Denmark, Finland, France, Germany, Ireland, India, Italy, Netherlands, New Zealand, Norway, Spain, Sweden, Switzerland, Turkey, the United Kingdom & the United States.

* **Unlimited Websites** - Import products into as many websites as you want.

* **Keep 100% of Your Commissions** - You keep every penny you earn!

* **Easy-to-use Interface** - Create an affiliate store in just a few minutes!

* **NO Datafeeds** - No need to download, import and parse large, unorganized CSV or XML files. Datafeedr does all of that for you!

* **Powerful Product Search** - Search across multiple affiliate networks and merchants simultaneously. Filter products by name, description, network, merchant, brand, price, sale price, discount percentage, currency, sale status and more.

* **Saved Searches** - Add products to your store in bulk by saving a search. No need to add products one by one, unless you want to.

* **Store-Specific Merchant Selection** - Select exactly which merchants to promote on a store-by-store basis. No need to search unrelated merchants when you�re creating a niche store. 

* **Automatically Update Products** - Updating doesn't require any action on your part. Product information updates automatically.

* **Automatically Add New Products** - When using "Saved Searches", any new products that match your search will automatically be added to your store during updates. There's no need to continually monitor merchant data feeds for new products.

* **Automatically Remove Old Products** - When a merchant removes a product from their inventory, it will automatically be removed from your store.

* **Updates Run in Background** - Most product importers require you to sit and watch the update until it finishes. Datafeedr performs product updates in the background whether you're watching or not. 

* **Character Encoding** - Properly handles special characters and accents when importing products.

* **Customizable Product Information** - Customize product name, description, and other details before or after loading products into Drupal.

* **Custom Products** - Add products from CSV importers or other sources to your store without conflicting with products imported by the Datafeedr Ubercart Importer module.

* **Custom Categories = Unique Website** - Most product importers use the merchant categories as the product categories on your site. The problem is, your store will be a duplicate of every other site using that merchant�s feed. Our module ensures your store will be unique by letting you create the categories you want and add unique groups of products to those categories.

* **SEO** - Works seamlessly with popular SEO module such as pathauto, metatag.

* **Tableless Layout** - Your pages will load faster and be more search engine friendly because no HTML tables are used to display product data.

* **Real Product Pages** - Every product in your store has its own crawlable, indexable URL.

* **Locally Stored Product Images** - All product images are stored on your server allowing them to be resized, optimized, displayed lightning fast, pinned on Pinterest and indexed by Google.

* **Locally Stored Product Data** - All product data is stored in your website's database to ensure it loads fast and is always accessible.

* **Display SKU** - Display product codes to attract highly motivated shoppers to your store.

* **NO Javascript Generated Content** - To ensure your product pages are found and indexed by Google, no product data is displayed using JavaScript. 

* **rel="nofollow"** - All affiliate links have the rel="nofollow" attribute added automatically.

* **Raw Affiliate Links** - Increase speed and protect your privacy by using the affiliate links provided by the affiliate network. No intermediary redirects or tracking!

= Supported Affiliate Networks =
The Datafeedr Ubercart Importer module currently supports over 12,000 merchants from the following affiliate networks:

1. **Adrecord** (SE)
1. **Adtraction** (DK, FI, NO, SE)
1. **Affiliate Window** (UK, US)
1. **Affiliate4You** (NL)
1. **Affiliator** (SE)
1. **Affilinet** (DE, ES, FR, NL, UK)
1. **Avangate** (US)
1. **AvantLink** (CA, US)
1. **Amazon Local** (US)
1. **belboon** (DE, FR, NL, UK)
1. **Betty Mills** (US)
1. **ClickBank** (US)
1. **clixGalore** (AU, NZ, UK, US)
1. **Commission Factory** (AU)
1. **Commission Junction** (DE, SE, UK, US)
1. **Daisycon** (BE, DE, FR, NL)
1. **DGM** (AU)
1. **Double.net** (SE)
1. **FlipKart** (IN)
1. **Impact Radius** (US)
1. **LinkConnector** (US)
1. **LinkShare** (AU, UK, US)
1. **MyCommerce** (US)
1. **oneNetworkDirect** (DE, UK, US)
1. **Paid On Results** (UK)
1. **Partner-ads** (DK)
1. **PepperJam** (US)
1. **RevResponse** (US)
1. **ShareASale** (US)
1. **SuperClix** (DE)
1. **Tradedoubler** (BE, CH, DE, DK, ES, FI, FR, IR, IT, NL, NO, SE, UK)
1. **TradeTracker** (BE, DE, DK, ES, FI, FR, IT, NL, NO, SE, UK)
1. **Webgains** (DE, ES, FR, IR, NL, NO, SE, UK, US)
1. **Zanox** (AU, BE, BR, CH, DE, DK, ES, FI, FR, IT, NL, NO, SE, TR)

== Installation ==

This section describes how to install and configure the module:

* Install the module as usual, for further information please visit http://drupal.org/node/895232

= Requirements =
In order to use the Datafeedr Ubercart Importer module you'll need a few things:

* [Ubercart](http://www.drupal.org/projects/ubercart) (free)
* [Datafeedr](http://www.drupal.org/projects/datafeedr) (free)
* [Datafeedr API account](https://v4.datafeedr.com/pricing?p=1&utm_campaign=dfrpswcplugin&utm_medium=referral&utm_source=wporg) (paid)
* An account with 1 or more of the supported affiliate networks.
* PHP's `allow_url_fopen` must be `On`.
* PHP's `CURL` support must be enabled.
* Drupal Cron enabled.
* 64MB of memory

== Frequently Asked Questions ==

= Where can I get help?  =
