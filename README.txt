=== Datafeedr ===

Contributors: datafeedr.com, cognizant.com
Tags: ubercart, datafeedr, affiliate products, dfrapi, adrecord, adtraction, affiliate window, affiliate4you, affilinet, amazon local, avangate, avantlink, belboon, betty mills, cj, clickbank, clixgalore, commission factory, commission junction, daisycon, dgm, flipkart, impact radius, linkconnector, linkshare, onenetworkdirect, paid on results, partner-ads, pepperjam, mycommerce, revresponse, shareasale, superclix, tradedoubler, tradetracker, webgains, zanox
Requires at least: 7.x

Connect to the Datafeedr API.

== Installation & Configuration ==

Steps to install the module and test with sample data:

1: Install Drupal 7 and go to administrator panel.
2: Install & enable all the dependencies modules.
  2.1: Ubercart (Store, Products, Catalog, Product attributes & Stock)
  2.2: Views
  2.3: Ctools
  2.4: field_group
  2.5: jquery_update
3: Install & enable Datafeedr modules.
  3.1: datafeedr_api
  3.2: datafeedr_product_sets
  3.3: datafeedr_ubercart_importer
4: Note: To work with the sample data please check the "Use sample data" chcekbox.
5: Module: datafeedr_api
  5.1: Please enter you "API Access ID" and "API Secret Key" 
  For testing purpose you can enter sample key (e.g. API Access ID = "demo_dfr" and API Secret Key = "demo_dfr").
  5.2: Please select the network(s) from the networks list. User has to select at least one network.
  NOTE: Please enter the Affiliate ID as "123" and Tracking ID as "456" for testing only.
  5.3: Please select the merchant(s) from the merchants list. User has to select at least one merchant.
6: Module: Ubercart
  6.1: Add category terms for the products.
7: Module: datafeedr_product_sets
  7.1: Click on "Add New Product Set" OR "Add Product Set" link/tab to create new Product Sets and map it to related product categories.
  7.2: NOTE: Please save the the search query by clicking on "Add as Save Search" & "Update Saved Search" button to import the products in the product set. Without save any search importer will not import the product in any product set.
8: Module: datafeedr_ubercart_importer
  8.1: Use this URL to test the importer functionality.
  URL: <SiteURL>/admin/config/datafeedr_ubercart_importer/test
  
  NOTE: This will import all the products stored in database sample table to your Ubercart store.

== Test Module with Sample Data ==

There are two ways to test the module products import functionality:

1) Enable the Drupal default CRON and update the configurations of Datafeedr Product Sets module.

2) Please run the test script URL in the browser till it displayed the message 'complete'.

URL: <Site URL>/admin/config/datafeedr_ubercart_importer/test
